import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./shared/components/home/home.component";
import { PageComponent } from "./components/page-top-level/page-top-level.component";
import { MsalGuard } from "@azure/msal-angular";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    pathMatch: "full",
    canActivate: [MsalGuard]
  },
  { path: "page/:pageId", component: PageComponent, canActivate: [MsalGuard] },
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
