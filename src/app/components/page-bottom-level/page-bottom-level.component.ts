import { EffectService } from "./../../services/effect.service";
import { Component, OnInit, Input, OnChanges, OnDestroy } from "@angular/core";
import { Observable, of, Subject, Subscription } from "rxjs";
import { Page, PageId, PageFK } from "src/app/models/Page";
import { map, repeatWhen, finalize, tap, catchError } from "rxjs/operators";
import { PageService } from "src/app/services/page.service";
import { IEffect } from "../page-table/page-table.component";
import { PageRequest } from "src/app/models/PageRequests";
import { PageAccessors, PageAccessor } from "src/app/models/PageAccessors";
import { PageEvent, MatTabChangeEvent } from "@angular/material";
import { fadeInOutTrigger, fadeInTrigger } from "src/app/models/Animations";
import {
  modifyItems,
  compareItemColumns,
  constructSearchString,
  concatenateSearchStrings
} from "src/app/shared/functions/page-functions";
import { Search } from "src/app/models/SearchChange";
import { ExportToExcel } from "../export";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "page-bottom-level",
  templateUrl: "./page-bottom-level.component.html",
  styleUrls: ["./page-bottom-level.component.scss"],
  animations: [fadeInOutTrigger, fadeInTrigger]
})
export class PageBottomLevelComponent implements OnInit, OnChanges, OnDestroy {
  @Input() childPages: PageId[] = [];

  requests$: Observable<{ title: string; page: Observable<Page> }[]>;
  requestParamsChange$ = new Subject<void>();
  totalItems$: Observable<number>;

  pageAccessor: PageAccessor;

  currentPageSize: number = 100;
  currentPageIndex: number = 0;
  currentTabTitle: string;
  currentTabIndex: number = 0;
  currentSearchOptions: Search[] = [];
  isLoading: boolean;

  searchString: string;
  latestPage: Page;

  constructor(
    private service: PageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnChanges() {
    this.queryParamsSub = this.route.queryParams.subscribe(params => {
      if (params && params.row) {
        this.currentSearchOptions = Search.FromQueryParams(
          this.route.snapshot.queryParamMap,
          "L2"
        );
        this.requestParamsChange$.next();
      }
    });
  }

  ngOnInit() {
    if (!this.childPages.length) return;

    this.requests$ = of(null).pipe(
      repeatWhen(() => this.requestParamsChange$),
      tap(() => (this.isLoading = true)),
      map(() => {
        this.currentTabTitle = this.childPages[this.currentTabIndex];

        let requests: {
          title: string;
          page: Observable<Page>;
        }[] = [];
        this.childPages.forEach(page => {
          const pageRequest = PageRequest(page);
          if (!pageRequest)
            return requests.push({
              title: "",
              page: of(null)
            });

          const pageAccessors = PageAccessors(page);
          if (!pageAccessors)
            return requests.push({
              title: "",
              page: of(null)
            });

          if (!this.pageAccessor) this.pageAccessor = pageAccessors;

          let search = constructSearchString(
            this.currentSearchOptions,
            pageAccessors,
            "L2"
          );

          const queryCurrentRow = this.route.snapshot.queryParamMap.get("row");
          if (!queryCurrentRow) throw new Error("row is not found");

          const currentPage = this.route.snapshot.paramMap.get(
            "pageId"
          ) as PageId;

          const pageFK = PageFK(pageAccessors.model, currentPage);
          if (!pageFK)
            console.error(
              `PageFK for ${currentPage} with ${pageAccessors.model} is not found`
            );

          const fkSearchString = `&$filter=${pageFK} eq ${queryCurrentRow}`;

          const request = this.entriesRequest(
            pageRequest,
            pageAccessors,
            concatenateSearchStrings([search, fkSearchString])
          ).pipe(
            catchError(() =>
              this.entriesRequest(
                pageRequest,
                pageAccessors,
                fkSearchString
              ).pipe(tap(() => console.log("Entries error fallback")))
            )
          );

          requests.push({
            page: request,
            title: pageAccessors.page
          });
        });

        return requests;
      })
    );
  }

  queryParamsSub: Subscription;
  ngOnDestroy(): void {
    if (this.queryParamsSub) this.queryParamsSub.unsubscribe();
  }

  private countRequest(
    request: string,
    pageAccessors: PageAccessor,
    search?: string
  ): Observable<number> {
    const { model } = pageAccessors;
    return this.service.Count(`${model}?${request}${search || ""}`);
  }

  private entriesRequest(
    request: string,
    pageAccessors: PageAccessor,
    search?: string
  ): Observable<Page> {
    const { model } = pageAccessors;
    return this.service
      .Entries(
        `${model}?$skip=${this.currentPageIndex * this.currentPageSize}&$top=${
          this.currentPageSize
        }&${request}${search || ""}`
      )
      .pipe(
        map(items => {
          if (items.length == 0) {
            this.totalItems$ = of(1);
            if (!this.latestPage) return null;

            const emptyPage = { ...this.latestPage };
            emptyPage.dataSource = [];
            return emptyPage;
          }

          this.totalItems$ = this.countRequest(
            request,
            pageAccessors,
            search
          ).pipe(
            catchError(() =>
              this.countRequest(request, pageAccessors).pipe(
                tap(() => console.log("Total items error fallback"))
              )
            )
          );

          const modifiedItems = modifyItems(items, pageAccessors);

          let columns = Object.keys(modifiedItems[0]);
          columns.sort((x, y) => compareItemColumns(x, y, modifiedItems[0]));

          const page: Page = {
            displayedColumns: columns,
            accessors: pageAccessors.accessors,
            dataSource: modifiedItems
          };

          page.dataSource.forEach(item => {
            Object.keys(item)
              .filter(
                key =>
                  key.indexOf("_effect") > -1 &&
                  item[key]["type"] == "primaryKey"
              )
              .forEach(i => {
                item[i]["routerLink"] = `/page/${pageAccessors.page}`;
                item[i]["type"] = "switchLevel";
              });
          });

          this.latestPage = { ...page };
          return page;
        }),
        finalize(() => (this.isLoading = false))
      );
  }

  searchChanged(event: Search[]) {
    this.router.navigate([], {
      queryParams: event.length
        ? Search.ToQueryParams(event, "L2")
        : Search.ResetQueryParams(this.route.snapshot.queryParamMap, ["L2"]),
      queryParamsHandling: "merge"
    });
  }

  tabChange(event: MatTabChangeEvent) {
    this.currentPageSize = 100;
    this.currentPageIndex = 0;
    this.requestParamsChange$.next();
    this.currentTabTitle = event.tab.textLabel;
  }

  pageChange(event: PageEvent) {
    this.currentPageIndex = event.pageIndex;
    this.currentPageSize = event.pageSize;
    this.requestParamsChange$.next();
  }

  exportToExcel() {
    return ExportToExcel(
      document.getElementById("bottom-level-table") as HTMLTableElement,
      "export",
      false
    );
  }
}
