import { element } from "protractor";
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ViewChild,
  OnDestroy,
  Output,
  EventEmitter,
  SimpleChanges
} from "@angular/core";
import { MatTableDataSource, MatSort } from "@angular/material";
import { DataType, ModelId, PageId, PagePK } from "src/app/models/Page";
import { Router, ActivatedRoute } from "@angular/router";
import { fadeInTrigger } from "src/app/models/Animations";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { debounceTime, map } from "rxjs/operators";
import { Subscription } from "rxjs";
import { Search } from "src/app/models/SearchChange";
import { SelectionModel } from "@angular/cdk/collections";
import { filterItemColumns } from "src/app/shared/functions/page-functions";

@Component({
  selector: "page-table",
  templateUrl: "./page-table.component.html",
  styleUrls: ["./page-table.component.scss"],
  animations: [fadeInTrigger]
})
export class PageTableComponent implements OnInit, OnChanges, OnDestroy {
  @Input() items: any[];
  @Input() displayedColumns: string[] = [];
  @Input() identifier: string;
  @Input() existingSearchOptions: Search[];
  @Input() childPages: boolean = true;
  @Output() searchChanged: EventEmitter<Search[]> = new EventEmitter();

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  selection: SelectionModel<any>;
  dataSource: MatTableDataSource<any>;

  searchTitles: string[] = [];
  searchForm: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    
  }

  ngOnInit() {
    this.renderTable();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.displayedColumns = filterItemColumns(this.displayedColumns);

    const { items } = changes;
    if (items && !items.firstChange) {
      const { previousValue, currentValue } = items;
      if (JSON.stringify(previousValue) !== JSON.stringify(currentValue))
        this.renderTable();
    }
  }

  changeView(effect?: IEffect, element?: any, view: viewId = "detail") {
    const queryParams = Search.ResetQueryParams(
      this.route.snapshot.queryParamMap,
      ["L2"]
    );

    queryParams["view"] = view;

    if (effect && element) {
      const hiddenEffect = element["hidden_effect"];

      queryParams["row"] = hiddenEffect
        ? element[hiddenEffect["modifiedKey"]]
        : element[effect.modifiedKey];

      if (effect.type == "primaryKey") {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: queryParams,
          queryParamsHandling: "merge"
        });
      } else {
        queryParams[`L1${effect.modifiedKey}`] = element[effect.modifiedKey];
        this.router.navigate([effect.routerLink], {
          queryParams: queryParams
        });
      }
    } else {
      queryParams["row"] = null;
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: queryParams,
        queryParamsHandling: "merge"
      });
    }
  }

  rowClicked(event: MouseEvent, element: IEffect) {
    console.log("rowCLicked-HasChild: " + this.childPages);
    if(this.childPages){
      this.selection.toggle(element);
      if (event.altKey) return console.log(element);
      this.displayedColumns.forEach(i => {
        const effect = element[i + "_effect"];
        if (effect && effect.type == "primaryKey") {
          if (this.selection.isSelected(element)) {
            this.changeView(effect, element, "default");
          } else {
            this.changeView(null, null, "default");
          }
          return;
        }
        if (effect && effect.type == "switchLevel") {
          this.changeView(effect, element, "default");
          return;
        }
      });
    }
  }
  private renderTable() {
    console.log("HasChildPageTable: " + this.childPages);
    console.log(
      "#CALL renderTable on page-table with identifier: " + this.identifier
    );
    this.searchForm = this.fb.group({
      searchItems: this.fb.array([])
    });

    this.displayedColumns.forEach(col => {
      const existingSearchOption = this.existingSearchOptions
        ? this.existingSearchOptions.find(opt => opt.name == col)
        : null;

      if (col.toLowerCase().indexOf("effect") === -1)
        this.searchItems.push(
          this.fb.group({
            name: col,
            value: existingSearchOption ? existingSearchOption.value : ""
          })
        );
    });

    this.searchItemsSub = this.searchItems.valueChanges
      .pipe(
        debounceTime(700),
        map((change: Search[]) =>
          this.searchChanged.emit(change.filter(c => c.value))
        )
      )
      .subscribe();

    if (this.items.length === 0) {
      const placeholder = {};
      this.displayedColumns.forEach(
        col => (placeholder[col] = "No data found.")
      );

      this.items.push(placeholder);
    }

    this.dataSource = new MatTableDataSource(this.items);
    this.dataSource.sort = this.sort;

    this.displayedColumns = this.displayedColumns.filter(
      col => col.indexOf("_effect") == -1
    );

    this.searchTitles = this.displayedColumns.map(col => `f_${col}`);

    this.selection = new SelectionModel<any>(false, []);

    //highlight selected item
    this.selectItemSub = this.route.queryParamMap
      .pipe(
        map(params => {
          if (this.identifier == "top-level-table") {
            const pagePK = PagePK(
              this.route.snapshot.paramMap.get("pageId") as PageId
            );
            if (params && params.get("row")) {
              const preSelectedItem = this.items.find(
                i => i[pagePK.modifiedKey] == params.get("row")
              );
              if (preSelectedItem) {
                this.selection.select(preSelectedItem);
              }
            } else {
              this.selection.clear();
            }
          }
        })
      )
      .subscribe();
  }

  private get searchItems(): FormArray {
    return this.searchForm.get("searchItems") as FormArray;
  }

  selectItemSub: Subscription;
  searchItemsSub: Subscription;
  ngOnDestroy() {
    if (this.searchItemsSub) this.searchItemsSub.unsubscribe();
    if (this.selectItemSub) this.selectItemSub.unsubscribe();
  }
}

export type viewId = "default" | "detail";

export interface IEffect {
  type: DataType;
  modifiedKey: string;
  unmodifiedKey: string;
  value: string;
  routerLink?: string;
  model: ModelId;
}
