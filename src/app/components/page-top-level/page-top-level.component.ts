import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { PageService } from "src/app/services/page.service";
import { Page, PageChildren, PageId } from "src/app/models/Page";
import { Observable, of, Subject, Subscription } from "rxjs";
import {
  map,
  switchMap,
  tap,
  repeatWhen,
  catchError,
  finalize
} from "rxjs/operators";
import {
  fadeInTrigger,
  fadeInOutTrigger,
  slideXInOutTrigger
} from "src/app/models/Animations";
import { ActivatedRoute, ParamMap, Router,Params } from "@angular/router";
import { IEffect, viewId } from "../page-table/page-table.component";
import { PageRequest } from "src/app/models/PageRequests";
import { PageAccessors, PageAccessor } from "src/app/models/PageAccessors";
import { PageEvent } from "@angular/material";
import {
  modifyItems,
  compareItemColumns,
  constructSearchString
} from "src/app/shared/functions/page-functions";
import { ExportToExcel } from "../export";
import { Search } from "src/app/models/SearchChange";
import { SideNavbarPage } from "src/app/shared/components/side-navbar/side-navbar.component";

@Component({
  selector: "page-top-level",
  templateUrl: "./page-top-level.component.html",
  styleUrls: ["./page-top-level.component.scss"],
  animations: [fadeInTrigger, fadeInOutTrigger, slideXInOutTrigger]
})
export class PageComponent implements OnInit, OnDestroy {
  page$: Observable<Page>;
  effect$: Observable<IEffect>;
  childPages$: Observable<string[]>;

  containerSize: number = 5;
  maxContainerSize: number = 5;
  isLoading: boolean;

  searchString: string = "";
  currentPageSize: number = 100;
  currentPageIndex: number = 0;
  currentPage: PageId;
  currentPageName: string;
  currentSearchOptions: Search[] = [];
  currentView: viewId = "default";

  totalItems$: Observable<number>;
  pageChange$ = new Subject<void>();
  queryChange$ = new Subject<Search[]>();

  latestPage: Page;
  selectedRow: string;
  hasChild : boolean = false;
  param: PageId;

  constructor(
    private cdr: ChangeDetectorRef,
    private service: PageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    console.log("ng-on-init page-top-level");
    this.page$ = of(null).pipe(
      repeatWhen(() => this.pageChange$),
      switchMap(() => {
        console.log("1");
        return this.route.paramMap.pipe(
          tap(() => (this.isLoading = true)),
          switchMap(paramMap => {
            console.log("3");
            this.resetPage(paramMap);
            console.log("afteresetpage");
            const pageAccessors = PageAccessors(this.currentPage);
            console.log("afterpageaccessors");
            if (!pageAccessors) return this.notFound();
            console.log("afterpageaccessors1");
            const request = PageRequest(this.currentPage);
            console.log(request);
            if (!request) return this.notFound();
            console.log("afterpageaccessors2");
            if(PageChildren(this.currentPage) !== null){
              this.hasChild = true;
            }else{
              this.hasChild = false;
            }
            console.log("afterpageaccessors3");
            console.log("Has Child :" + this.hasChild);

            return of(null).pipe(
              repeatWhen(() => this.queryChange$),
              switchMap(() => {
                console.log("2");
                if (!this.currentSearchOptions.length)
                  this.currentSearchOptions = Search.FromQueryParams(
                    this.route.snapshot.queryParamMap,
                    "L1"
                  );

                const search = constructSearchString(
                  this.currentSearchOptions,
                  pageAccessors,
                  "L1"
                );
                console.log(`${request} - Request. ${pageAccessors} - pageAccessors. ${search} - search`);
                return this.entriesRequest(request, pageAccessors, search).pipe(
                  catchError(() => {
                    console.log("TOP LEVEL Entries Request fallback");
                    return this.entriesRequest(request, pageAccessors);
                  })
                );
              })
            );
          })
        );
      })
    );
    this.childPages$ = this.route.paramMap.pipe(
      map(paramMap => {
        const page = paramMap.get("pageId") as PageId;
        console.log(page);
        this.param = page;
        return PageChildren(page);
      })
    );
    this.queryChangeSub = this.route.queryParamMap
      .pipe(
        map(queryParamMap => {
          console.log("5");
          this.viewChange(queryParamMap);
          const search = Search.FromQueryParams(queryParamMap, "L1");
          const currentPage = this.route.snapshot.paramMap.get(
            "pageId"
          ) as PageId;
          if (
            JSON.stringify(this.currentSearchOptions) !==
              JSON.stringify(search) &&
            currentPage == this.currentPage
          ) {
            this.currentSearchOptions = search;
            this.queryChange$.next(search);
          }
        })
      )
      .subscribe();
        
        console.log("dara");
    this.cdr.detectChanges();
  }

  viewChange(params: ParamMap) {
    const rowCondition = params && params.has("row");
    const viewCondition = params && params.has("view");
    if (rowCondition) {
      this.maxContainerSize = 4;
      if (this.containerSize == 5) this.containerSize = 3;
      this.selectedRow = params.get("row");
    } else {
      this.maxContainerSize = 5;
      this.containerSize = 5;
      this.selectedRow = null;
    }

    this.currentView =
      viewCondition && params.get("view") == "detail" ? "detail" : "default";
  }

  exportToExcel() {
    return ExportToExcel(
      document.getElementById("top-level-table") as HTMLTableElement,
      "export",
      false
    );
  }

  defaultView() {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { view: "default" },
      queryParamsHandling: "merge"
    });
  }

  resetQuery() {
    const queryParams = Search.ResetQueryParams(
      this.route.snapshot.queryParamMap,
      ["L1", "L2"]
    );

    this.router.navigate([], {
      queryParams: queryParams,
      queryParamsHandling: "merge"
    });
  }

  searchChanged(event: Search[]) {
    this.router.navigate([], {
      queryParams: event.length
        ? Search.ToQueryParams(event, "L1")
        : Search.ResetQueryParams(this.route.snapshot.queryParamMap, ["L1"]),
      queryParamsHandling: "merge"
    });
  }

  pageChange(event: PageEvent) {
    this.currentPageIndex = event.pageIndex;
    this.currentPageSize = event.pageSize;
    this.pageChange$.next();
  }

  get topLevelHeight() {
    return this.containerSize * 20;
  }

  get bottomLevelHeight() {
    return (5 - this.containerSize) * 20;
  }

  private entriesRequest(
    request: string,
    pageAccessors: PageAccessor,
    search?: string
  ): Observable<Page> {
    const { model, accessors } = pageAccessors;

    return this.service
      .Entries(
        `${model}?$top=${this.currentPageSize}&$skip=${this.currentPageSize *
          this.currentPageIndex}&${request}${search || ""}`
      )
      .pipe(
        map(items => {
          console.log(items);
          if (items.length == 0) {
            if (!this.latestPage) return null;

            const emptyPage = { ...this.latestPage };
            emptyPage.dataSource = [];
            return emptyPage;
          }

          const modifiedItems = modifyItems(items, pageAccessors);

          let columns = Object.keys(modifiedItems[0]);

          columns.sort((x, y) => compareItemColumns(x, y, modifiedItems[0]));

          const page: Page = {
            displayedColumns: columns,
            dataSource: modifiedItems,
            accessors
          };

          this.totalItems$ = this.totalItemsRequest(
            request,
            pageAccessors,
            search
          );

          this.latestPage = { ...page };
          return page;
        }),
        finalize(() => (this.isLoading = false))
      );
  }

  private totalItemsRequest(
    request: string,
    pageAccessors: PageAccessor,
    search?: string
  ): Observable<number> {
    const { model } = pageAccessors;
    return this.service.Count(`${model}?${request}${search || ""}`);
  }

  private notFound = (): Observable<any> => {
    return of(null).pipe(this.stopLoader());
  };

  private stopLoader = () => {
    return tap(() => (this.isLoading = false));
  };

  private resetPage(paramMap: ParamMap) {
    this.containerSize = this.maxContainerSize = this.selectedRow ? 3 : 5;

    const currentPage = paramMap.get("pageId") as PageId;
    if (this.currentPage != currentPage) {
      console.log("#NAVIGATION Page change occurred.");
      this.currentSearchOptions = [];
      this.searchString = "";
      this.currentPage = currentPage;
      this.currentPageName = SideNavbarPage(this.currentPage);
      this.currentPageSize = 100;
      this.currentPageIndex = 0;
    }
  }

  queryChangeSub: Subscription;
  ngOnDestroy() {
    if (this.queryChangeSub) this.queryChangeSub.unsubscribe();
  }
}
