import * as XLSX from "xlsx";
import { ElementRef } from "@angular/core";

export function ExportToExcel(
  table: HTMLTableElement,
  filename_prefix: string,
  filtersHided: boolean
) {
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(table);

  delete_col(ws, 1);

  if (!filtersHided) {
    delete_row(ws, 1);
  }

  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

  /* save to file */
  let today = new Date().toLocaleDateString();
  let filename = `${filename_prefix}__${today}.xlsx`;
  XLSX.writeFile(wb, filename);
}

function ec(r: number, c: number) {
  return XLSX.utils.encode_cell({ r: r, c: c });
}

function delete_row(ws: XLSX.WorkSheet, row_index: number) {
  let range = XLSX.utils.decode_range(ws["!ref"]);
  for (var R = row_index; R <= range.e.r; ++R) {
    for (var C = range.s.c; C <= range.e.c; ++C) {
      ws[ec(R, C)] = ws[ec(R + 1, C)];
    }
  }
  range.e.r--;
  ws["!ref"] = XLSX.utils.encode_range(range.s, range.e);
}

function delete_col(ws: XLSX.WorkSheet, col_index: number) {
  let range = XLSX.utils.decode_range(ws["!ref"]);
  for (var R = range.s.r; R <= range.e.r; ++R) {
    for (var C = col_index; C <= range.e.c; ++C) {
      ws[ec(R, C)] = ws[ec(R, C + 1)];
    }
  }
  range.e.c--;
  ws["!ref"] = XLSX.utils.encode_range(range.s, range.e);
}
