import { Observable, of, Subject, Subscription } from "rxjs";
import { EffectService } from "./../../services/effect.service";
import { PageId, PagePK } from "./../../models/Page";
import { PageService } from "src/app/services/page.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { map, switchMap, catchError, repeatWhen } from "rxjs/operators";
import { ActivatedRoute } from "@angular/router";
import { PageAccessors } from "src/app/models/PageAccessors";
import { PageRequest } from "src/app/models/PageRequests";
import { fadeInTrigger } from "src/app/models/Animations";
import {
  modifyItems,
  compareItemColumns,
  filterItemColumns
} from "src/app/shared/functions/page-functions";

@Component({
  selector: "page-detail",
  templateUrl: "./page-detail.component.html",
  styleUrls: ["./page-detail.component.scss"],
  animations: [fadeInTrigger]
})
export class PageDetailComponent implements OnInit, OnDestroy {
  pageDetail$: Observable<{ detail: any; columns: any[] }>;
  catchedError: boolean = false;
  currentPage: PageId;
  queryChange$ = new Subject<void>();
  queryCurrentRow: string = "";
  constructor(private service: PageService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.pageDetail$ = of(null).pipe(
      switchMap(() => {
        return this.route.paramMap.pipe(
          switchMap(paramMap => {
            const page = paramMap.get("pageId") as PageId;
            return of(null).pipe(
              repeatWhen(() => this.queryChange$),
              switchMap(() => {
                if (this.currentPage != page) return of(null);

                const pageAccessor = PageAccessors(page);
                if (!pageAccessor) return of(null);

                const pageRequest = PageRequest(page);
                if (!pageRequest) return of(null);

                const pagePK = PagePK(page);
                if (!pagePK) throw new Error(`PK for ${page} not found`);

                const filter = `&$filter=${pagePK.unmodifiedKey} eq ${this.queryCurrentRow}`;

                return this.service
                  .Entries(`${pageAccessor.model}?${pageRequest}${filter}`)
                  .pipe(
                    map(items => {
                      const modifiedItems = modifyItems(items, pageAccessor);
                      const detail = modifiedItems[0];
                      const columns = filterItemColumns(Object.keys(detail));

                      columns.sort((x, y) =>
                        compareItemColumns(x, y, modifiedItems[0])
                      );

                      return {
                        detail,
                        generalColumns: columns.filter(
                          c => !this.isLink(detail[c])
                        ),
                        linkColumns: columns.filter(c => this.isLink(detail[c]))
                      };
                    }),
                    catchError((error: any) => {
                      console.error(error);
                      this.catchedError = true;
                      return of([]);
                    })
                  );
              })
            );
          })
        );
      })
    );

    this.queryChangeSub = this.route.queryParamMap
      .pipe(
        map(queryParamMap => {
          const detailView = queryParamMap.get("view");
          const queryRow = queryParamMap.get("row");
          if (detailView == "detail" && queryRow) {
            const page = this.route.snapshot.paramMap.get("pageId") as PageId;
            this.currentPage = page;
            this.queryCurrentRow = queryRow;
            this.queryChange$.next();
          }
        })
      )
      .subscribe();
  }

  isLink(value: string): boolean {
    return value && typeof value === "string"
      ? value.includes("http://") || value.includes("https://")
      : false;
  }

  queryChangeSub: Subscription;
  ngOnDestroy() {
    if (this.queryChangeSub) this.queryChangeSub.unsubscribe();
  }
}
