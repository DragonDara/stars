import { tryFormatBoolean, tryFormatDate } from "src/app/pipes/clean-data.pipe";
import { PageAccessor } from "src/app/models/PageAccessors";
import { Search } from "src/app/models/SearchChange";

export function filterItemColumns(arr: string[]): string[] {
  return arr.filter(
    key => key.indexOf("_effect") == -1 && key.toLowerCase().indexOf("id") == -1
  );
}
export function compareItemColumns(i: string, j: string, item: any): number {
  if (item[i + "_effect"]) {
    if (item[i + "_effect"].type == "primaryKey") {
      return -1;
    }
    if (item[j + "_effect"]) {
      if (item[j + "_effect"].type == "primaryKey") {
        return 1;
      }
    }
    if (item[i + "_effect"].type == "detail") {
      return -1;
    }
  }

  if (item[j + "_effect"]) {
    if (
      item[j + "_effect"].type == "primaryKey" ||
      item[j + "_effect"].type == "detail"
    ) {
      return 1;
    }
  }

  return i > j ? 1 : -1;
}

export function modifyItems(items: any[], pageAccessors: PageAccessor): any[] {
  return items.slice().map(item => {
    let modifiedItem = {};
    Object.keys(item).forEach(key => {
      const accessor = pageAccessors.accessors.filter(
        a => a.unmodifiedKey == key
      );
      if (!accessor) return;

      accessor.forEach(a => {
        try {
          modifiedItem[a.modifiedKey] = eval(`item.${a.pathToValue}`);

          if (a.dataType)
            switch (a.dataType) {
              case "boolean":
                modifiedItem[a.modifiedKey] = tryFormatBoolean(
                  modifiedItem[a.modifiedKey]
                );

                break;

              case "date":
                modifiedItem[a.modifiedKey] = tryFormatDate(
                  modifiedItem[a.modifiedKey]
                );
                break;

              case "link":
                modifiedItem[a.modifiedKey + "_effect"] = {
                  href: modifiedItem[a.modifiedKey],
                  type: "link"
                };
                break;

              case "primaryKey":
                modifiedItem[a.modifiedKey + "_effect"] = {
                  type: "primaryKey",
                  unmodifiedKey: a.unmodifiedKey,
                  modifiedKey: a.modifiedKey,
                  model: pageAccessors.model,
                  routerLink: `/page/${pageAccessors.page}`
                };
                break;
              case "hidden":
                modifiedItem["hidden_effect"] = {
                  unmodifiedKey: a.unmodifiedKey,
                  modifiedKey: a.modifiedKey
                };
                break;
            }
        } catch {
          modifiedItem[a.modifiedKey] = null;
        }
      });
    });

    return modifiedItem;
  });
}

export function constructSearchString(
  changes: Search[],
  pageAccessor: PageAccessor,
  prefix?: string
) {
  const { accessors } = pageAccessor;
  const expression = changes
    .map(c => {
      const index = prefix ? c.name.indexOf(prefix) : -1;
      const accessor = accessors.find(
        pa =>
          pa.modifiedKey ==
          (index > -1 ? c.name.substr(index + prefix.length) : c.name)
      );
      if (!accessor) return;

      const { pathToValue } = accessor;
      const paths = pathToValue.split(".");
      let field = paths[0];
      for (let i = paths.length - 1; i > 0; i--) field += `/${paths[i]}`;

      return isNaN(c.value)
        ? `contains(${field}, '${c.value}')`
        : `${field} eq ${c.value}`;
    })
    .join(" and ");

  return expression ? `&$filter=${expression}` : "";
}

export function concatenateSearchStrings(searchStrings: string[]) {
  return (
    "&$filter=" +
    searchStrings
      .filter(search => search)
      .map(search => {
        const index = search.indexOf("&$filter=");
        return index > -1 ? search.substr(index + "&$filter=".length) : search;
      })
      .join(" and ")
  );
}
