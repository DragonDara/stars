import { Component, OnInit, Input } from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import {
  slideXRightInTrigger,
  slideXLeftInTrigger
} from "src/app/models/Animations";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  animations: [slideXRightInTrigger, slideXLeftInTrigger]
})
export class HeaderComponent implements OnInit {
  @Input() sidenav: MatSidenav;
  currentTheme: ThemeType;
  scrollingEnabled: boolean = true;

  constructor() {}

  ngOnInit() {
    this.currentTheme =
      (localStorage.getItem("stars_theme") as ThemeType) || "light";
    this.theme(this.currentTheme);
  }

  theme(type: ThemeType) {
    const element = document.getElementById("theme") as HTMLLinkElement;
    if (!element) return;

    element.href = `${type}.css`;

    localStorage.setItem("stars_theme", type);
    this.currentTheme = type;
  }
}

declare type ThemeType = "light" | "dark";
