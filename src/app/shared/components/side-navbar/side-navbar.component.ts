import { Component, OnInit } from "@angular/core";
import { onSideNavChange } from "../../animations/sidenav-animation";
import { PageId } from "src/app/models/Page";

interface Page {
  pageId: PageId;
  link: string;
  name: string;
  icon?: string;
}

@Component({
  selector: "app-side-navbar",
  templateUrl: "./side-navbar.component.html",
  styleUrls: ["./side-navbar.component.scss"],
  animations: [onSideNavChange]
})
export class SideNavbarComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  pages: Page[] = sideNavbarPages;
}

export function SideNavbarPage(page: PageId): string {
  const sideNavbarPage = sideNavbarPages.find(pa => pa.pageId == page);
  return sideNavbarPage ? sideNavbarPage.name : "Page name is not found";
}

const sideNavbarPages: Page[] = [
  {
    link: "/page/areas",
    name: "Areas",
    pageId: "areas"
  },
  {
    name: "Area Report Names and Description vs Priorities and Systems",
    link: "/page/areareportlink",
    pageId: "areareportlink"
  },
  {
    name: "Commissioning Dossiers vs System and Comments",
    link: "/page/commdossiers",
    pageId: "commdossiers"
  },
  {
    name: "Commissioning Docs",
    link: "/page/commdossierdoc",
    pageId: "commdossierdoc"
  },
  {
    name: "Completion Statuses by System",
    link: "/page/completionstatus",
    pageId: "completionstatus"
  },
  {
    name: "Tag Component Heirarchy and System Reference",
    link: "/page/components",
    pageId: "components"
  },
  {
    name: "Document Numbers vs All Related File Links / References",
    link: "/page/docFileLinks",
    pageId: "docFileLinks"
  },
  {
    name: "Document Numbers",
    link: "/page/documentNumbers",
    pageId: "documentNumbers"
  },
  {
    name: "System Dossier Review Meta Data",
    link: "/page/dossierreview",
    pageId: "dossierreview"
  },
  {
    name: "Engineering Docs by System",
    link: "/page/engdocs",
    pageId: "engdocs"
  },
  {
    name: "FCR QVD Dossiers by System",
    link: "/page/fcrqvdarchive",
    pageId: "fcrqvdarchive"
  },
  {
    name: "FCR TPP Dossier by System",
    link: "/page/fcrtpparchive",
    pageId: "fcrtpparchive"
  },
  {
    name: "Industrial Base Archives",
    link: "/page/ibarchive",
    pageId: "ibarchive"
  },
  //  -- INVALID
  {
    name: "Passports (error, incomplete response)",
    link: "/page/passports",
    pageId: "passports"
  },
  {
    name: "Peer Review Details",
    link: "/page/peerreview",
    pageId: "peerreview"
  },
  {
    name: "Power Management System Document Details",
    link: "/page/pmsdocdetail",
    pageId: "pmsdocdetail"
  },
  {
    name: "Commissioning Procedures",
    link: "/page/commprocedures",
    pageId: "commprocedures"
  },
  {
    name: "Master Punchlist",
    link: "/page/punchlist",
    pageId: "punchlist"
  },
  {
    name: "Punchlist Post",
    link: "/page/punchlistspost",
    pageId: "punchlistspost"
  },
  {
    name: "Quality Verification Document Archive",
    link: "/page/qvdarchive",
    pageId: "qvdarchive"
  },
  {
    name: "Regulatory Affairs Certifications",
    link: "/page/racert",
    pageId: "racert"
  },
  {
    name: "Regulatory Certificates by System",
    link: "/page/racertsystem",
    pageId: "racertsystem"
  },
  {
    name: "Regulatory Affairs Citations",
    link: "/page/racitation",
    pageId: "racitation"
  },
  {
    name: "ReportDoc",
    link: "/page/reportdoc",
    pageId: "reportdoc"
  },
  {
    name: "Scope Drawing Master List",
    link: "/page/scopedrwgrev",
    pageId: "scopedrwgrev"
  },
  {
    name: "Scope Drawings by System",
    link: "/page/scopedrwgrevsyslink",
    pageId: "scopedrwgrevsyslink"
  },
  {
    name: "System Readiness Checklists by System",
    link: "/page/srcdoc",
    pageId: "srcdoc"
  },
  {
    name:
      "Commissioning, Start-Up, Operating and Troubleshooting Procedures by System",
    link: "/page/procdoc",
    pageId: "procdoc"
  },
  {
    name: "Commissioning Leak Test Procedures By System",
    link: "/page/leakdoc",
    pageId: "leakdoc"
  },
  {
    name: "Electrical Certificates vs Tag",
    link: "/page/commelpsp",
    pageId: "commelpsp"
  },
  {
    name: "Electrical Switch Cards by Tag",
    link: "/page/commswdoc",
    pageId: "commswdoc"
  },
  {
    name: "Well Service Test Packages by System",
    link: "/page/shelldoc",
    pageId: "shelldoc"
  },
  {
    name: "SOS Items vs Systems",
    link: "/page/sossystem",
    pageId: "sossystem"
  },
  {
    name: "System Master List",
    link: "/page/systems",
    pageId: "systems"
  },

  {
    name: "TPP Archive",
    link: "/page/tpparchive",
    pageId: "tpparchive"
  },
  {
    name: "Units",
    link: "/page/units",
    pageId: "units"
  },
  {
    name: "Variations",
    link: "/page/variations",
    pageId: "variations"
  },
  {
    name: "Vendor Documentation by System",
    link: "/page/vendordocs",
    pageId: "vendordocs"
  }
];
