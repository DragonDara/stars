import { animate, animation, style } from "@angular/animations";

export const FadeOutAnimation = animation([
  animate(
    "500ms",
    style({
      opacity: 0
    })
  )
]);

export const FadeInAnimation = animation([
  style({
    opacity: 0
  }),
  animate(
    "500ms",
    style({
      opacity: 1
    })
  )
]);
