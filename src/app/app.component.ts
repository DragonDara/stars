import { Component, OnInit } from "@angular/core";
import { onMainContentChange } from "./shared/animations/sidenav-animation";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [onMainContentChange]
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
