import { CleanDataPipe } from "./pipes/clean-data.pipe";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { HeaderComponent } from "./shared/components/header/header.component";
import { LayoutModule } from "@angular/cdk/layout";
import { MatButtonModule } from "@angular/material/button";
import { MatListModule } from "@angular/material/list";
import { SideNavbarComponent } from "./shared/components/side-navbar/side-navbar.component";
import { HomeComponent } from "./shared/components/home/home.component";

import { PageComponent } from "./components/page-top-level/page-top-level.component";
import {
  MatTableModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatTabsModule,
  MatFormFieldModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatTooltipModule,
  MatSortModule,
  MatDialogModule
} from "@angular/material";
import { GetValuePipe } from "./pipes/get-value.pipe";
import { PageBottomLevelComponent } from "./components/page-bottom-level/page-bottom-level.component";
import { PageDetailComponent } from "./components/page-detail/page-detail.component";
import { PageTableComponent } from "./components/page-table/page-table.component";
import { ReactiveFormsModule } from "@angular/forms";

import { MsalModule } from "@azure/msal-angular";
import { MsalInterceptor } from "@azure/msal-angular";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

const msalConfig = {
  //see in azure portal: AAD
  clientID: "20729944-6f27-4c60-9f99-8918606a9f01", // dev
  authority: "https://login.microsoftonline.com/chevron.onmicrosoft.com",
  validateAuthority: true,

  cacheLocation: "sessionStorage",
  storeAuthStateInCookie: true, // dynamically set to true when IE11

  popUp: false,

  navigateToLoginRequestUri: false,

  consentScopes: ["openid profile offline_access user.read"]
};

// To test on localhost
// ng serve --ssl true --port 44300
// uncomment MsalGuard in dynamic-table.module.ts

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideNavbarComponent,
    HomeComponent,
    PageComponent,
    GetValuePipe,
    CleanDataPipe,
    PageBottomLevelComponent,
    PageDetailComponent,
    PageTableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatSortModule,
    LayoutModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatTabsModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MsalModule.forRoot(msalConfig),
    MatTooltipModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: MsalInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
