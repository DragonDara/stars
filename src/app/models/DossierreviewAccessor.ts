import { PageAccessor } from "./PageAccessors";

export let dossierreviewRequest: string =
  "$select=Id,Status,Receiveddate,Reviewdate,Accepteddate,Transmittaldate,Location,Comments,Systemid" +
  "&$expand=System($select=Systemno,Description,Descrus,Systemid;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus))),Contractoruser($select=Usercai,Fullname),Reviewuser($select=Fullname,Usercai),Pfduser($select=Usercai,Fullname)";

export const dossierreviewAccessor: PageAccessor = {
  page: "dossierreview",
  model: "Dossierreview",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "System.Unit.Area.Name",
      modifiedKey: "Area Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Namerus",
      modifiedKey: "Area Code Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Description",
      modifiedKey: "Area Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitno",
      modifiedKey: "Unit Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnorus",
      modifiedKey: "Unit Code Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitname",
      modifiedKey: "Unit Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnamerus",
      modifiedKey: "Unit Name Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Dossier Review Status Code",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "Receiveddate",
      modifiedKey: "Received Date",
      unmodifiedKey: "Receiveddate",
      dataType: "date"
    },
    {
      pathToValue: "Reviewdate",
      modifiedKey: "Reviewd Date",
      unmodifiedKey: "Reviewdate"
    },
    {
      pathToValue: "Reviewdate",
      modifiedKey: "Reviewd Date",
      unmodifiedKey: "Reviewdate",
      dataType: "date"
    },
    {
      pathToValue: "Accepteddate",
      modifiedKey: "Accepted Date",
      unmodifiedKey: "Accepteddate",
      dataType: "date"
    },
    {
      pathToValue: "Transmittaldate",
      modifiedKey: "Transmittal Date",
      unmodifiedKey: "Transmittaldate",
      dataType: "date"
    },
    {
      pathToValue: "Contractoruser.Usercai",
      modifiedKey: "Contractor User CAI",
      unmodifiedKey: "Contractoruser"
    },
    {
      pathToValue: "Contractoruser.Fullname",
      modifiedKey: "Contractor User Fullname",
      unmodifiedKey: "Contractoruser"
    },
    {
      pathToValue: "Reviewuser.Usercai",
      modifiedKey: "Review User CAI",
      unmodifiedKey: "Reviewuser"
    },
    {
      pathToValue: "Reviewuser.Fullname",
      modifiedKey: "Review User Fullname",
      unmodifiedKey: "Reviewuser"
    },
    {
      pathToValue: "Pfduser.Usercai",
      modifiedKey: "PFD User CAI",
      unmodifiedKey: "Pfduser"
    },
    {
      pathToValue: "Pfduser.Fullname",
      modifiedKey: "PFD User Fullname",
      unmodifiedKey: "Pfduser"
    },
    {
      pathToValue: "Location",
      modifiedKey: "Location",
      unmodifiedKey: "Location"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Systemid",
      modifiedKey: "Systemid",
      unmodifiedKey: "Systemid"
    }
  ]
};
