import { PageAccessor } from "./PageAccessors";

export let docfilelinkRequest: string =
  "" +
  "$select=Id,Docnoid,Filename,Rev,Description,BackdraftStatus,Filetype,Updateuserid,Updatedate,Transferredtotidedate" +
  "&$expand=Transferredtotideuser($select=Usercai,Fullname)" +
  "&$expand=Docno($select=Docno)";

export const docfilelinkAccessor: PageAccessor = {
  page: "docFileLinks",
  model: "Docfilelink",
  accessors: [
    {
      unmodifiedKey: "Id",
      modifiedKey: "Id",
      pathToValue: "Id",
      dataType: "hidden"
    },
    {
      unmodifiedKey: "Docno",
      modifiedKey: "Document Number",
      pathToValue: "Docno.Docno",
      dataType: "primaryKey"
    },
    {
      unmodifiedKey: "Docnoid",
      modifiedKey: "Docnoid",
      pathToValue: "Docnoid"
    },
    {
      unmodifiedKey: "Filename",
      modifiedKey: "Filename",
      pathToValue: "Filename",
      dataType: "link"
    },
    {
      unmodifiedKey: "Rev",
      modifiedKey: "Rev",
      pathToValue: "Rev"
    },
    {
      unmodifiedKey: "Description",
      modifiedKey: "Description",
      pathToValue: "Description"
    },
    {
      unmodifiedKey: "BackdraftStatus",
      modifiedKey: "BackdraftStatus",
      pathToValue: "BackdraftStatus"
    },
    {
      unmodifiedKey: "Filetype",
      modifiedKey: "Filetype",
      pathToValue: "Filetype"
    },
    {
      unmodifiedKey: "Updateuserid",
      modifiedKey: "Updateuserid",
      pathToValue: "Updateuserid"
    },
    {
      unmodifiedKey: "Updatedate",
      modifiedKey: "Updatedate",
      pathToValue: "Updatedate",
      dataType: "date"
    },
    {
      unmodifiedKey: "Transferredtotidedate",
      modifiedKey: "Transferredtotidedate",
      pathToValue: "Transferredtotidedate",
      dataType: "date"
    },
    {
      unmodifiedKey: "Transferredtotideuser",
      modifiedKey: "TransferredToTideFullName",
      pathToValue: "Transferredtotideuser.Fullname"
    },
    {
      unmodifiedKey: "Transferredtotideuser",
      modifiedKey: "TransferredToTideCAI",
      pathToValue: "Transferredtotideuser.Usercai"
    }
  ]
};
