import { PageAccessor } from "src/app/models/PageAccessors";

export let reportdocRequest: string =
  "$select=Id" +
  "&$expand=System($select=Systemno,Description,Descrus),Docno($select=Id,Docno,Doctype,Title,Titlerus)";

export const reportdocAccessor: PageAccessor = {
  page: "reportdoc",
  model: "Reportdoc",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Docno.Title",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Docno"
    }
  ]
};
