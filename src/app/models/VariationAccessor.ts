import { PageAccessor } from "./PageAccessors";

export let variationRequest: string =
  "$select=Id,No,Comments,Status" +
  "&$expand=Docno($select=Docno,Title,TitleRus;$expand=Docfilelinks($select=Filename))";

export const variationAccessor: PageAccessor = {
  page: "variations",
  model: "Variation",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "No",
      modifiedKey: "Variation No",
      unmodifiedKey: "No",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Variation Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Variation Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Title",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Variation Status",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "Docno.Docfilelinks.Filename",
      modifiedKey: "Document Links",
      unmodifiedKey: "Docno",
      dataType: "link"
    }
  ]
};
