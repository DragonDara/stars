import { PageAccessor } from "./PageAccessors";

export let qvdarchiveRequest: string =
  "$select=Id,Tagno,Checksheet,Sheetdesc,Docno,Filename,Tabno,Notreq,Comments,Systemid" +
  "&$expand=System($select=Systemno,Description,Descrus),Discipline($select=Discipline1,Description,Descriptionrus),VerifiedUser($select=Usercai,Fullname)";

export const qvdarchiveAccessor: PageAccessor = {
  page: "qvdarchive",
  model: "Qvdarchive",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Tagno",
      modifiedKey: "Tag Number",
      unmodifiedKey: "Tagno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Discipline.Discipline1",
      modifiedKey: "Discipline",
      unmodifiedKey: "Discipline"
    },
    {
      pathToValue: "Discipline.Description",
      modifiedKey: "Discipline Description Eng",
      unmodifiedKey: "Discipline"
    },
    {
      pathToValue: "Discipline.Descriptionrus",
      modifiedKey: "Discipline Description Rus",
      unmodifiedKey: "Discipline"
    },
    {
      pathToValue: "Checksheetno",
      modifiedKey: "Checksheet No",
      unmodifiedKey: "Checksheetno"
    },
    {
      pathToValue: "Sheetdesc",
      modifiedKey: "Sheetdesc",
      unmodifiedKey: "Sheetdesc"
    },
    {
      pathToValue: "Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Filename",
      modifiedKey: "File Link",
      unmodifiedKey: "Filename",
      dataType: "link"
    },
    {
      pathToValue: "Tabno",
      modifiedKey: "Tab No",
      unmodifiedKey: "Tabno"
    },
    {
      pathToValue: "Notreq",
      modifiedKey: "Not Required Code",
      unmodifiedKey: "Notreq"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "VerifiedUser.Usercai",
      modifiedKey: "Usercai",
      unmodifiedKey: "VerifiedUser"
    },
    {
      pathToValue: "VerifiedUser.Fullname",
      modifiedKey: "Fullname",
      unmodifiedKey: "VerifiedUser"
    }
  ]
};
