import { PageAccessor } from "./PageAccessors";

export let punchlistRequest: string =
  "" +
  "$select=Id,Plnumber,Pldescription,Pldescrus,Actiongroup,Category,Raisedby,Clearedby,Cleareddate,Commentseng,Commentsrus,Eng,Mat,Reopencomments,Status,Verifiedby,Verifieddate" +
  "&$expand=Discipline($select=Description),System($select=Systemno)";

export const punchlistAccessor: PageAccessor = {
  page: "punchlist",
  model: "Punchlist",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Plnumber",
      modifiedKey: "Punchlist Item Number",
      unmodifiedKey: "Plnumber",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Pldescription",
      modifiedKey: "Description Eng",
      unmodifiedKey: "Pldescription"
    },
    {
      pathToValue: "Pldescrus",
      modifiedKey: "Description Rus",
      unmodifiedKey: "Pldescrus"
    },
    {
      pathToValue: "Pldescrus",
      modifiedKey: "Description Rus",
      unmodifiedKey: "Pldescrus"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Discipline.Description",
      modifiedKey: "Discipline",
      unmodifiedKey: "Discipline"
    },
    {
      pathToValue: "Actiongroup",
      modifiedKey: "Action Group",
      unmodifiedKey: "Actiongroup"
    },
    {
      pathToValue: "Category",
      modifiedKey: "Category Code",
      unmodifiedKey: "Category"
    },
    {
      pathToValue: "Raisedby",
      modifiedKey: "Raised By",
      unmodifiedKey: "Raisedby"
    },
    {
      pathToValue: "Clearedby",
      modifiedKey: "Cleared By",
      unmodifiedKey: "Clearedby"
    },
    {
      pathToValue: "Cleareddate",
      modifiedKey: "Cleared Date",
      unmodifiedKey: "Cleareddate",
      dataType: "date"
    },
    {
      pathToValue: "Commentseng",
      modifiedKey: "Comments Eng",
      unmodifiedKey: "Commentseng"
    },

    {
      pathToValue: "Commentsrus",
      modifiedKey: "Commets Rus",
      unmodifiedKey: "Commentsrus"
    },
    {
      pathToValue: "Eng",
      modifiedKey: "Engineering Required",
      unmodifiedKey: "Eng"
    },
    {
      pathToValue: "Mat",
      modifiedKey: "Material Required",
      unmodifiedKey: "Mat"
    },
    {
      pathToValue: "Reopencomments",
      modifiedKey: "Material Required",
      unmodifiedKey: "Reopencomments"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Status",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "Verifiedby",
      modifiedKey: "Verified By",
      unmodifiedKey: "Verifiedby"
    },
    {
      pathToValue: "Verifieddate",
      modifiedKey: "Verified Date",
      unmodifiedKey: "Verifieddate",
      dataType: "date"
    }
  ]
};
