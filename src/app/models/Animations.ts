import {
  animation,
  animate,
  style,
  useAnimation,
  transition,
  trigger,
  state
} from "@angular/animations";

export const fadeOutAnimation = animation([
  animate(
    ".5s 0s ease-out",
    style({
      opacity: 0
    })
  )
]);

export const fadeInAnimation = animation([
  style({ opacity: 0 }),
  animate(
    ".5s 0s ease-in",
    style({
      opacity: 1
    })
  )
]);

export const slideXLeftInAnimation = animation([
  style({
    transform: "translateX(-100%)",
    opacity: 0
  }),
  animate(
    ".5s",
    style({
      transform: "translateX(0)",
      opacity: 1
    })
  )
]);

export const slideXLeftOutAnimation = animation([
  style({
    zIndex: 0
  }),
  animate(
    ".5s",
    style({
      transform: "translateX(-100%)",
      opacity: 0
    })
  )
]);

export const slideXRightInAnimation = animation(
  [
    style({ transform: "translateX(100%)", opacity: 0 }),
    animate(
      "{{ delay }}",
      style({
        transform: "translateX(0)",
        opacity: 1
      })
    )
  ],
  {
    params: {
      delay: ".5s"
    }
  }
);

export const slideXRightOutAnimation = animation([
  animate(
    ".5s",
    style({
      transform: "translateX(100%)",
      opacity: 0
    })
  )
]);

export const slideYBottomInAnimation = animation([
  style({
    transform: "translateY(100%)",
    opacity: 0
  }),
  animate(
    ".5s 0s ease-in",
    style({
      transform: "translateY(0)",
      opacity: 1
    })
  )
]);

export const slideYBottomOutAnimation = animation([
  animate(
    ".5s 0s ease-out",
    style({
      transform: "translateY(100%)",
      opacity: 1
    })
  )
]);

export const fadeInTrigger = trigger("fadeIn", [
  transition(":enter", useAnimation(fadeInAnimation))
]);

export const fadeInOutTrigger = trigger("fadeInOut", [
  transition(":enter", useAnimation(fadeInAnimation)),
  transition(":leave", useAnimation(fadeOutAnimation))
]);

export const slideXRightInTrigger = trigger("slideXRightIn", [
  transition(":enter", useAnimation(slideXRightInAnimation))
]);

export const slideXLeftInTrigger = trigger("slideXLeftIn", [
  transition(":enter", useAnimation(slideXLeftInAnimation))
]);

export const slideYBottomInTrigger = trigger("slideYBottomIn", [
  transition(":enter", useAnimation(slideYBottomInAnimation))
]);

export const slideXInOutTrigger = trigger("slideXInOut", [
  state("right-in", style({})),
  state("left-in", style({})),
  state("right-out", style({})),
  state("left-out", style({})),
  transition("* => left-out", useAnimation(slideXLeftOutAnimation)),
  transition("* => left-in", useAnimation(slideXLeftInAnimation)),
  transition("* => right-out", useAnimation(slideXRightOutAnimation)),
  transition("* => right-in", useAnimation(slideXRightInAnimation))
]);
