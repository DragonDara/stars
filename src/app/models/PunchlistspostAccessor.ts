import { PageAccessor } from "./PageAccessors";

export let punchlistspostRequest: string =
  "$select=Plnumber,Category,Pldescription,Pldescrus,Contractor,Status,Actiongroup,Createddate,Verifieddate,Cleareddate" +
  "&$expand=Unit($select=Unitno,Unitname;$expand=Area($select=Name,Description,Descrus)),Raisedbyuser($select=Usercai,Fullname),Verifiedbyuser($select=Usercai,Fullname)";

export const punchlistspostAccessor: PageAccessor = {
  page: "punchlistspost",
  model: "Punchlistspost",
  accessors: [
    {
      pathToValue: "Plnumber",
      modifiedKey: "Punchlist Number",
      unmodifiedKey: "Plnumber",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Unit.Area.Name",
      modifiedKey: "Area Name",
      unmodifiedKey: "Unit"
    },
    {
      pathToValue: "Unit.Area.Description",
      modifiedKey: "Area Description Eng",
      unmodifiedKey: "Unit"
    },
    {
      pathToValue: "Unit.Area.Descrus",
      modifiedKey: "Area Description Rus",
      unmodifiedKey: "Unit"
    },

    {
      pathToValue: "Unit.Unitno",
      modifiedKey: "Unit No",
      unmodifiedKey: "Unit"
    },
    {
      pathToValue: "Unit.Unitname",
      modifiedKey: "Unit Name",
      unmodifiedKey: "Unit"
    },

    {
      pathToValue: "Category",
      modifiedKey: "Category",
      unmodifiedKey: "Category"
    },
    {
      pathToValue: "Pldescription",
      modifiedKey: "Punchlist Description Eng",
      unmodifiedKey: "Pldescription"
    },
    {
      pathToValue: "Pldescrus",
      modifiedKey: "Punchlist Description Rus",
      unmodifiedKey: "Pldescrus"
    },
    {
      pathToValue: "Contractor",
      modifiedKey: "Contractor",
      unmodifiedKey: "Contractor"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Status",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "Actiongroup",
      modifiedKey: "Action Group",
      unmodifiedKey: "Actiongroup"
    },
    {
      pathToValue: "Createddate",
      modifiedKey: "Created Date",
      unmodifiedKey: "Createddate",
      dataType: "date"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Status",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "Raisedbyuser.Usercai",
      modifiedKey: "Raised by CAI",
      unmodifiedKey: "Raisedbyuser"
    },
    {
      pathToValue: "Raisedbyuser.Fullname",
      modifiedKey: "Raised by",
      unmodifiedKey: "Raisedbyuser"
    },
    {
      pathToValue: "Verifieddate",
      modifiedKey: "Verified Date",
      unmodifiedKey: "Verifieddate",
      dataType: "date"
    },
    {
      pathToValue: "Verifiedbyuser.Usercai",
      modifiedKey: "Verified by CAI",
      unmodifiedKey: "Verifiedbyuser"
    },
    {
      pathToValue: "Verifiedbyuser.Fullname",
      modifiedKey: "Verified by",
      unmodifiedKey: "Verifiedbyuser"
    },
    {
      pathToValue: "Cleareddate",
      modifiedKey: "Cleared Date",
      unmodifiedKey: "Cleareddate",
      dataType: "link"
    }
  ]
};
