import { PageAccessor } from "./PageAccessors";

export let asbuiltlinkRequest: string =
"$select=SheetNo,Redlcommfn,Redlcomm,Redlconst,Recievedconst,Recievedcomm,Comments" +
"&$expand=Drawing($select=Id),System($select=Systemid),Updateuser($select=Userid)";

export const asbuiltlinkAccessor: PageAccessor = {
  page: "asbuiltlink",
  model: "Asbuiltlink",
  accessors: [
    {
        pathToValue: "System.Systemid",
        modifiedKey: "System Id",
        unmodifiedKey: "System"
    },
    {
        pathToValue: "Drawing.Id",
        modifiedKey: "Drawingid",
        unmodifiedKey: "Drawing",
    },
    {
        pathToValue: "Updateuser.Userid",
        modifiedKey: "UpdateUserId",
        unmodifiedKey: "Updateuser" 
    },
    {
        pathToValue: "Sheetno",
        modifiedKey: "AsBuilt Link Sheet Number",
        unmodifiedKey: "Sheetno"
    },
    {
        pathToValue: "Redlconst",
        modifiedKey: "Construction Red line Status Code",
        unmodifiedKey: "Redlconst"
    },
    {
        pathToValue: "Redlcommfn",
        modifiedKey: "Commissioning Red line File Name",
        unmodifiedKey: "Redlcommfn"
    },
    {
        pathToValue: "Redlcomm",
        modifiedKey: "Commissioning Red line Status Code",
        unmodifiedKey: "Redlcomm"
    },
    {
        pathToValue: "Recievedconst",
        modifiedKey: "AsBuilt Recieved From Construction",
        unmodifiedKey: "Recievedconst"
    },
    {
        pathToValue: "Recievedcomm",
        modifiedKey: "AsBuilt Recieved From Commissioning",
        unmodifiedKey: "Recievedcomm"
    },
    {
        pathToValue: "Comments",
        modifiedKey: "AsBuiltLink Comments",
        unmodifiedKey: "Comments"
    }
  ]
};
