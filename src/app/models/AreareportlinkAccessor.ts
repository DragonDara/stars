import { PageAccessor } from "./PageAccessors";

export let areareportlinkRequest: string =
  "$select=Id,Priorityno,Subpriorityno" +
  "&$expand=Areareport($select=Name,Namerus,Description,Descrus),System($select=Systemno,Description,Descrus,Systemid)";

export const areareportlinkAccessor: PageAccessor = {
  page: "areareportlink",
  model: "Areareportlink",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Priorityno",
      modifiedKey: "System Priority",
      unmodifiedKey: "Priorityno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Subpriorityno",
      modifiedKey: "System Sub Priority",
      unmodifiedKey: "Subpriorityno"
    },
    {
      pathToValue: "Areareport.Name",
      modifiedKey: "Area Report Name Eng",
      unmodifiedKey: "Areareport"
    },
    {
      pathToValue: "Areareport.Namerus",
      modifiedKey: "Area Report Name Rus",
      unmodifiedKey: "Areareport"
    },
    {
      pathToValue: "Areareport.Description",
      modifiedKey: "Area Report Description Eng",
      unmodifiedKey: "Areareport"
    },
    {
      pathToValue: "Areareport.Descrus",
      modifiedKey: "Area Report Dscription Rud",
      unmodifiedKey: "Areareport"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Systemid",
      modifiedKey: "System Id",
      unmodifiedKey: "System"
    }
  ]
};
