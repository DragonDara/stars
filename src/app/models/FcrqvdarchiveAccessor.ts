import { PageAccessor } from "./PageAccessors";
export let fcrqvdarchiveRequest: string =
  "$select=id,Tagno,Checksheet,Sheetdesc,Docno,Filename,Tabno,Notreq,Systemid" +
  "&$expand=System($select=Systemno,Description,Descrus),Disc($select=Discipline1,Description,DescriptionRus)";

export const fcrqvdarchiveAccessor: PageAccessor = {
  page: "fcrqvdarchive",
  model: "Fcrqvdarchive",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },

    {
      pathToValue: "Disc.Discipline1",
      modifiedKey: "Discipline",
      unmodifiedKey: "Disc"
    },
    {
      pathToValue: "Disc.Description",
      modifiedKey: "Discipline Description Eng",
      unmodifiedKey: "Disc"
    },
    {
      pathToValue: "Disc.Descriptionrus",
      modifiedKey: "Discipline Description Rus",
      unmodifiedKey: "Disc"
    },
    {
      pathToValue: "Tagno",
      modifiedKey: "Tag No",
      unmodifiedKey: "Tagno"
    },
    {
      pathToValue: "Checksheet",
      modifiedKey: "Checksheet",
      unmodifiedKey: "Checksheet"
    },
    {
      pathToValue: "Sheetdesc",
      modifiedKey: "Sheetdesc",
      unmodifiedKey: "Sheetdesc"
    },
    {
      pathToValue: "Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Filename",
      modifiedKey: "File Link",
      unmodifiedKey: "Filename",
      dataType:"link"
    },
    {
      pathToValue: "Tabno",
      modifiedKey: "Tab No",
      unmodifiedKey: "Tabno"
    },
    {
      pathToValue: "Notreq",
      modifiedKey: "Not Required",
      unmodifiedKey: "Notreq"
    }
  ]
};
