import { PageAccessor } from "./PageAccessors";
export let fcrtpparchiveRequest: string =
  "$select=Id,Tagno,Linenumber,Tcopipeowner,Receiveddate,Signoffdate,Comments,Accepteddate,Systemid" +
  "&$expand=System($select=Systemno,Description,Descrus)";

export const fcrtpparchiveAccessor: PageAccessor = {
  page: "fcrtpparchive",
  model: "Fcrtpparchive",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Tagno",
      modifiedKey: "Tag No",
      unmodifiedKey: "Tagno"
    },
    {
      pathToValue: "Linenumber",
      modifiedKey: "Line Number",
      unmodifiedKey: "Linenumber"
    },
    {
      pathToValue: "Tcopipeowner",
      modifiedKey: "TCOPIPEOWNER",
      unmodifiedKey: "Tcopipeowner"
    },
    {
      pathToValue: "Receiveddate",
      modifiedKey: "Received Date",
      unmodifiedKey: "Receiveddate",
      dataType: "date"
    },
    {
      pathToValue: "Signoffdate",
      modifiedKey: "Sign off Date",
      unmodifiedKey: "Signoffdate",
      dataType: "date"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Accepteddate",
      modifiedKey: "Accepted date",
      unmodifiedKey: "Accepteddate",
      dataType: "date"
    }
  ]
};
