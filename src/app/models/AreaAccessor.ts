import { PageAccessor } from "./PageAccessors";

export let areaRequest: string =
  "$select=Id,Description,Descrus,Name,NameRus,Responsible";

export const areaAccessor: PageAccessor = {
  page: "areas",
  model: "Area",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Name",
      modifiedKey: "Name Eng",
      unmodifiedKey: "Name",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Namerus",
      modifiedKey: "Name Rus",
      unmodifiedKey: "Namerus"
    },
    {
      pathToValue: "Description",
      modifiedKey: "Description Eng",
      unmodifiedKey: "Description"
    },
    {
      pathToValue: "Descrus",
      modifiedKey: "Description Rus",
      unmodifiedKey: "Descrus"
    },
    {
      pathToValue: "Responsible",
      modifiedKey: "Responsible",
      unmodifiedKey: "Responsible"
    }
  ]
};
