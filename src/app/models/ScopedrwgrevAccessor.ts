import { PageAccessor } from "./PageAccessors";

export let scopedrwgrevRequest: string =
  "$select=Apddate,Filename,Id" +
  "&$expand=Scopedrwgsheet($expand=Docno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename))),Apduser($select=Usercai,Fullname)";

export const scopedrwgrevAccessor: PageAccessor = {
  page: "scopedrwgrev",
  model: "Scopedrwgrev",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Scopedrwgsheet.Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Scopedrwgsheet",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Scopedrwgsheet.Docno.Title",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Scopedrwgsheet"
    },
    {
      pathToValue: "Scopedrwgsheet.Docno.Titlerus",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Scopedrwgsheet"
    },
    {
      pathToValue: "Apddate",
      modifiedKey: "Approved Date",
      unmodifiedKey: "Apddate",
      dataType: "date"
    },
    {
      pathToValue: "Apduser.Usercai",
      modifiedKey: "Approved User CAI",
      unmodifiedKey: "Apddate"
    },
    {
      pathToValue: "Apduser.Fullname",
      modifiedKey: "Approved User CAI",
      unmodifiedKey: "Apddate"
    },
    {
      pathToValue: "Filename",
      modifiedKey: "Scope Drawing Link",
      unmodifiedKey: "Filename",
      dataType: "link"
    },
    {
      pathToValue: "Scopedrwgsheet.Docno.Docfilelinks.Filename",
      modifiedKey: "Drawing link",
      unmodifiedKey: "Scopedrwgsheet",
      dataType: "link"
    }
  ]
};
