import { ParamMap } from "@angular/router";

export class Search {
  name: string;
  nameWithPrefix: string;
  value: any;

  public static ToQueryParams(search: Search[], prefix?: string) {
    const queryParams = {};

    search.forEach(s => (queryParams[`${prefix}${s.name}`] = s.value));

    return queryParams;
  }

  public static ResetQueryParams(paramMap: ParamMap, prefixes: string[]) {
    let result = {};
    prefixes.forEach(prefix => {
      const tempSearch = this.FromQueryParams(paramMap, prefix);
      tempSearch.forEach(i => {
        result[`${prefix}${i.name}`] = null;
      });
    });
    return result;
  }

  public static FromQueryParams(paramMap: ParamMap, prefix?: string) {
    const search = [] as Search[];
    paramMap.keys
      .filter(key => (prefix ? key.indexOf(prefix) > -1 : true))
      .forEach(nameWithPrefix => {
        const index = nameWithPrefix.indexOf(prefix);
        const name =
          index > -1
            ? nameWithPrefix.substr(index + prefix.length)
            : nameWithPrefix;
        search.push({
          name,
          nameWithPrefix,
          value: paramMap.get(nameWithPrefix)
        });
      });

    return search;
  }
}
