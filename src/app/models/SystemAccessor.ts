import { PageAccessor } from "./PageAccessors";

export let systemRequest: string =
  "" +
  "$select=Systemid,Systemno,Description,Closed,Fwdate,Virtual,Ct,Mc,Descrus,Tcdate,Rct,Rmc,Compstatus,Compvercomments,Vkaapplicable," +
  "Veredapplicable,Commdosstatus,Ctconfsent,Mcconfsent,Unitno,Areano,Unitdesc" +
  "&$expand=Unit($select=Unitno),Ctdocno($select=Docno),Mcdocno($select=Docno),Aoccomdocno($select=Docno),Aocmcdocno($select=Docno)";

export const systemAccessor: PageAccessor = {
  page: "systems",
  model: "Systema",
  accessors: [
    {
      pathToValue: "Systemid",
      modifiedKey: "System id",
      unmodifiedKey: "Systemid",
      dataType: "hidden"
    },
    {
      pathToValue: "Aoccomdocno.Docno",
      modifiedKey: "Aoccomdocnoid",
      unmodifiedKey: "Aoccomdocno"
    },
    {
      pathToValue: "Aocmcdocno.Docno",
      modifiedKey: "Aocmcdocnoid",
      unmodifiedKey: "Aocmcdocno"
    },
    {
      pathToValue: "Areano",
      modifiedKey: "Area Code Number",
      unmodifiedKey: "Areano"
    },
    {
      pathToValue: "Closed",
      modifiedKey: "System Closed",
      unmodifiedKey: "Closed",
      dataType: "boolean"
    },
    {
      pathToValue: "Commdosstatus",
      modifiedKey: "Commissioning Dossier Status",
      unmodifiedKey: "Commdosstatus"
    },
    {
      pathToValue: "Compstatus",
      modifiedKey: "System Completion Status Code",
      unmodifiedKey: "Compstatus"
    },
    {
      pathToValue: "Compvercomments",
      modifiedKey: "Completion Version Comments",
      unmodifiedKey: "Compvercomments"
    },
    {
      pathToValue: "Ct",
      modifiedKey: "CT Acceptance Date",
      unmodifiedKey: "Ct",
      dataType: "date"
    },
    {
      pathToValue: "Ctconfsent",
      modifiedKey: "Custody Transfer Confirmation Sent",
      unmodifiedKey: "Ctconfsent",
      dataType: "date"
    },
    {
      pathToValue: "Ctdocno.Docno",
      modifiedKey: "Custody Transfer Doc Number",
      unmodifiedKey: "Ctdocno"
    },
    {
      pathToValue: "Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "Description"
    },
    {
      pathToValue: "Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "Descrus"
    },
    {
      pathToValue: "Fwdate",
      modifiedKey: "Final Walkdown Date",
      unmodifiedKey: "Fwdate",
      dataType: "date"
    },
    {
      pathToValue: "Mc",
      modifiedKey: "Mechanical Completion Acceptance Date",
      unmodifiedKey: "Mc",
      dataType: "date"
    },
    {
      pathToValue: "Mcconfsent",
      modifiedKey: "Mechanical Completion Confirmation Sent",
      unmodifiedKey: "Mcconfsent",
      dataType: "date"
    },
    {
      pathToValue: "Mcdocno.Docno",
      modifiedKey: "Mechanical Completion Doc Number",
      unmodifiedKey: "Mcdocno"
    },
    {
      pathToValue: "Rct",
      modifiedKey: "Ready For CT Date",
      unmodifiedKey: "Rct",
      dataType: "date"
    },
    {
      pathToValue: "Rmc",
      modifiedKey: "Post Date of Ready Mechanical Completion Letter",
      unmodifiedKey: "Rmc",
      dataType: "date"
    },
    {
      pathToValue: "Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "Systemno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Tcdate",
      modifiedKey: "Technical Commission Date",
      unmodifiedKey: "Tcdate",
      dataType: "date"
    },
    {
      pathToValue: "Unitno",
      modifiedKey: "Unit Code Number",
      unmodifiedKey: "Unitno"
    },
    {
      pathToValue: "Unit.Unitno",
      modifiedKey: "UnitCodeEng",
      unmodifiedKey: "Unit"
    },
    {
      pathToValue: "Unitdesc",
      modifiedKey: "Unit Description",
      unmodifiedKey: "Unitdesc"
    },
    {
      pathToValue: "Veredapplicable",
      modifiedKey: "Venndor Engineering Redline Applicable",
      unmodifiedKey: "Veredapplicable",
      dataType: "boolean"
    },
    {
      pathToValue: "Virtual",
      modifiedKey: "Virtual",
      unmodifiedKey: "Virtual",
      dataType: "boolean"
    },
    {
      pathToValue: "Vkaapplicable",
      modifiedKey: "Vendor Key Action Applicable",
      unmodifiedKey: "Vkaapplicable",
      dataType: "boolean"
    }
  ]
};
