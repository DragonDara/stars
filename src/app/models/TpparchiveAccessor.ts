import { PageAccessor } from "./PageAccessors";

export let tpparchiveRequest: string =
  "$select=Id,Tagno,Linenumber,Drawingno,Tcopipeowner,Receiveddate,Signoffdate,Transmitdate,Comments,Accepteddate" +
  "&$expand=System($select=Systemno,Description,Descrus)";

export const tpparchiveAccessor: PageAccessor = {
  page: "tpparchive",
  model: "Tpparchive",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },

    {
      pathToValue: "Tagno",
      modifiedKey: "Tag No",
      unmodifiedKey: "Tagno"
    },
    {
      pathToValue: "Linenumber",
      modifiedKey: "Line Number",
      unmodifiedKey: "Linenumber"
    },
    {
      pathToValue: "Drawingno",
      modifiedKey: "Drawing No",
      unmodifiedKey: "Drawingno"
    },
    {
      pathToValue: "Tcopipeowner",
      modifiedKey: "TCOPIPEOWNER",
      unmodifiedKey: "Tcopipeowner"
    },
    {
      pathToValue: "Receiveddate",
      modifiedKey: "Received Date",
      unmodifiedKey: "Receiveddate",
      dataType: "date"
    },
    {
      pathToValue: "Signoffdate",
      modifiedKey: "Sign off Date",
      unmodifiedKey: "Signoffdate",
      dataType: "date"
    },
    {
      pathToValue: "Transmitdate",
      modifiedKey: "Transmittal Date",
      unmodifiedKey: "Transmitdate",
      dataType: "date"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Accepteddate",
      modifiedKey: "Accepted Date",
      unmodifiedKey: "Accepteddate",
      dataType: "date"
    }
  ]
};
