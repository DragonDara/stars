import { PageAccessor } from "./PageAccessors";

export let pmsdocdetailRequest: string =
  "$select=Id,Tagno,Checksheetno,DocNo,EngName,RusName,FilePath,Description,Pmsdoctype" +
  "&$expand=System($select=Unit,Systemno,Description),Flsdoctype($select=Doctype,Name,Code)";

export const pmsdocdetailAccessor: PageAccessor = {
  page: "pmsdocdetail",
  model: "Pmsdocdetail",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Tagno",
      modifiedKey: "Tag No",
      unmodifiedKey: "Tagno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Checksheetno",
      modifiedKey: "Checksheet No",
      unmodifiedKey: "Checksheetno"
    },
    {
      pathToValue: "DocNo",
      modifiedKey: "Document No",
      unmodifiedKey: "DocNo"
    },
    {
      pathToValue: "EngName",
      modifiedKey: "Document Name Eng",
      unmodifiedKey: "EngName"
    },
    {
      pathToValue: "RusName",
      modifiedKey: "Document Name Rus",
      unmodifiedKey: "RusName"
    },
    {
      pathToValue: "FilePath",
      modifiedKey: "File link",
      unmodifiedKey: "FilePath",
      dataType: "link"
    },
    {
      pathToValue: "Description",
      modifiedKey: "FLS Description",
      unmodifiedKey: "Description"
    },
    {
      pathToValue: "Pmsdoctype",
      modifiedKey: "FLS Doc Type",
      unmodifiedKey: "Pmsdoctype"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit",
      modifiedKey: "Unit No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Flsdoctype.Doctype",
      modifiedKey: "FLS Doc Type",
      unmodifiedKey: "Flsdoctype"
    },
    {
      pathToValue: "Flsdoctype.Name",
      modifiedKey: "FLS Doc Type Name",
      unmodifiedKey: "Flsdoctype"
    },
    {
      pathToValue: "Flsdoctype.Code",
      modifiedKey: "FLS Doc Type Code",
      unmodifiedKey: "Flsdoctype"
    }
  ]
};
