import { PageAccessors } from "./PageAccessors";

export class Page {
  displayedColumns: string[];
  accessors: Accessor[];
  dataSource: any[];
}

export class Accessor {
  pathToValue: string;
  modifiedKey: string;
  unmodifiedKey: string;
  dataType?: DataType;
}

export type DataType =
  | "boolean"
  | "date"
  | "link"
  | "primaryKey"
  | "switchLevel"
  | "hidden";

export type PageId =
  | "passports"
  | "engdocs"
  | "components"
  | "documentNumbers"
  | "docFileLinks"
  | "areas"
  | "units"
  | "systems"
  | "punchlist"
  | "vendordocs"
  | "commprocedures"
  | "passports"
  | "variations"
  | "tpparchive"
  | "commdossiers"
  | "fcrqvdarchive"
  | "fcrtpparchive"
  | "ibarchive"
  | "peerreview"
  | "pmsdocdetail"
  | "punchlistspost"
  | "qvdarchive"
  | "racert"
  | "racertsystem"
  | "racitation"
  | "scopedrwgrev"
  | "scopedrwgrevsyslink"
  | "completionstatus"
  | "areareportlink"
  | "dossierreview"
  | "sossystem"
  | "reportdoc"
  | "commdossierdoc"
  | "srcdoc"
  | "procdoc"
  | "leakdoc"
  | "commelpsp"
  | "commswdoc"
  | "shelldoc"
  | "asbuiltlink"
  | "componentdocnolink"
  | "fcrtpparchivelink"
  | "racitationfile";

export type ModelId =
  | "Engdoc"
  | "Component"
  | "Documentno"
  | "Systema"
  | "Area"
  | "Unit"
  | "Docfilelink"
  | "Punchlist"
  | "Vendordoc"
  | "Procedure"
  | "PassportNew"
  | "Variation"
  | "Tpparchive"
  | "Commdossierdoc"
  | "Commdossier"
  | "Fcrqvdarchive"
  | "Fcrtpparchive"
  | "IbArchive"
  | "Peer"
  | "Pmsdocdetail"
  | "Punchlistspost"
  | "Qvdarchive"
  | "Racert"
  | "Racertsystem"
  | "Racitation"
  | "Scopedrwgrev"
  | "Scopedrwgrevsyslink"
  | "Completionstatus"
  | "Areareportlink"
  | "Dossierreview"
  | "SosSystem"
  | "Reportdoc"
  | "Commdossierdoc"
  | "Srcdoc"
  | "Procdoc"
  | "Leakdoc"
  | "Commelpsp"
  | "Commswdoc"
  | "Shelldoc"
  | "Asbuiltlink"
  | "Componentdocnolink"
  | "Fcrtpparchivelink"
  | "Racitationfile";

export function PageChildren(page: PageId): string[] {
  const record = PAGE_CHILDREN.find(pc => pc.page == page);
  return record ? record.children : null;
}

export function PageFK(model: ModelId, page: PageId) {
  console.log(`Model = ${model} Page = ${page}`);
  const record = FOREIGN_KEYS.find(fk => fk.page == page && fk.model == model);
  return record ? record.foreignKey : null;
}

export function PagePK(page: PageId): Accessor {
  const pageAccessors = PageAccessors(page);
  if (pageAccessors) {
    const pagePK = pageAccessors.accessors.find(
      acc => acc.dataType && acc.dataType == "hidden"
    );
    return pagePK ? pagePK : null;
  }
  return null;
}

const FOREIGN_KEYS: { model: ModelId; page: PageId; foreignKey: string }[] = [
  //CHECKED FK
  // { page: "racert", children: ["racertsystem"] },
  { model: "Racertsystem", page: "racert", foreignKey: "Certid" },
  { model: "Racitation", page: "areas", foreignKey: "Areaid"},
  { model: "Peer", page: "areas", foreignKey: "Areaid"},
  // { page: "areas", children: ["units"] },
  { model: "Unit", page: "areas", foreignKey: "Areaid" },
  // {
  //   page: "documentNumbers",
  //   children: [
  //     "commdossiers",
  //     "docFileLinks",
  //     "engdocs",
  //     "passports",
  //     "variations",
  //     "vendordocs",
  //     "systems"
  //   ]
  // },
  { model: "Commdossierdoc", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "Docfilelink", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "Engdoc", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "PassportNew", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "Variation", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "Vendordoc", page: "documentNumbers", foreignKey: "Tcodocnoid" },
  { model: "Systema", page: "documentNumbers", foreignKey: "Ctnadocnoid" },
  { model: "Reportdoc", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "Srcdoc", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "Procdoc", page: "documentNumbers", foreignKey: "Docnoid" },
  { model: "Asbuiltlink", page: "documentNumbers", foreignKey: "Drawingid" },
  { model: "Commelpsp", page: "documentNumbers", foreignKey: "Docnoid"},
  { model: "Leakdoc", page: "documentNumbers", foreignKey: "Docnoid"},
  { model: "Commswdoc", page: "documentNumbers", foreignKey: "Docnoid"},
  { model: "Procedure", page: "documentNumbers", foreignKey: "Docnoid"},
  { model: "Shelldoc", page: "documentNumbers", foreignKey: "Docnoid"},
  // There are related tables in documentNumbers that are not added in FOREIGN_KEYS and PAGE_CHILDREN
  // Scopedewgsheet and Componentdocnolink



  // { page: "scopedrwgrev", children: ["scopedrwgrevsyslink"] },
  {
    model: "Scopedrwgrevsyslink",
    page: "scopedrwgrev",
    foreignKey: "Scopedrwgrevid"
  },

  // {
  //   page: "systems",
  //   children: [
  //     "areareportlink",
  //     "commdossiers",
  //     "components",
  //     "dossierreview",
  //     "engdocs",
  //     "fcrqvdarchive",
  //     "ibarchive",
  //     "punchlist",
  //     "qvdarchive",
  //     "scopedrwgrevsyslink",
  //     "sossystem",
  //     "fcrtpparchive",
  //     "racertsystem",
  //     "tpparchive",
  //     "vendordocs"
  //   ]
  // },
  { model: "Areareportlink", page: "systems", foreignKey: "Systemid" },
  { model: "Commdossier", page: "systems", foreignKey: "Systemid" },
  { model: "Component", page: "systems", foreignKey: "Systemid" },
  { model: "Dossierreview", page: "systems", foreignKey: "Systemid" },
  { model: "Engdoc", page: "systems", foreignKey: "Systemid" },
  { model: "Fcrqvdarchive", page: "systems", foreignKey: "Systemid" },
  { model: "IbArchive", page: "systems", foreignKey: "Systemid" },
  { model: "Punchlist", page: "systems", foreignKey: "Systemid" },
  { model: "Qvdarchive", page: "systems", foreignKey: "Systemid" },
  { model: "Scopedrwgrevsyslink", page: "systems", foreignKey: "Systemid" },
  { model: "SosSystem", page: "systems", foreignKey: "Systemid" },
  { model: "Fcrtpparchive", page: "systems", foreignKey: "Systemid" },
  { model: "Racertsystem", page: "systems", foreignKey: "Systemid" },
  { model: "Tpparchive", page: "systems", foreignKey: "Systemid" },
  { model: "Vendordoc", page: "systems", foreignKey: "Systemid" },
  { model: "Reportdoc", page: "systems", foreignKey: "Systemid" },
  { model: "Shelldoc", page:"systems", foreignKey: "Systemid"},
  { model: "Racert", page: "systems", foreignKey: ""},
  { model: "Leakdoc", page: "systems", foreignKey: "Systemid"},
  { model: "Commelpsp", page: "systems", foreignKey: "Systemid"},
  { model: "Commswdoc", page:"systems", foreignKey: "Systemid"},
  { model: "Srcdoc", page: "systems", foreignKey: "Systemid"},
  { model: "Procdoc", page: "systems", foreignKey: "Systemid"},
  { model: "Asbuiltlink", page: "systems", foreignKey: "Systemid"},
  { model: "Completionstatus", page: "systems", foreignKey: "Systemid"},
  { model: "Commdossierdoc", page: "systems", foreignKey: "Systemid"},

  // { page: "units", children: ["systems", "punchlistspost"] }
  { model: "Systema", page: "units", foreignKey: "Unitid" },
  { model: "Punchlistspost", page: "units", foreignKey: "Unitid" },

  // { page: "fcrtpparchive", children:["fcrtpparchivelink"]}
  { model: "Fcrtpparchivelink", page: "fcrtpparchive", foreignKey: "Tppid"},

  //{ page: "racitation", children:["racitationfile"]}
  { model: "Racitationfile", page:"racitation", foreignKey:"Citationid"}

];

const PAGE_CHILDREN: { page: PageId; children: PageId[] }[] = [
  { page: "areas", children: [
      "units",
      "racitation",
      "peerreview"
    ] 
  },
  {
    page: "documentNumbers",
    children: [
      "commdossiers",
      "docFileLinks",
      "engdocs",
      "passports",
      "variations",
      "vendordocs",
      "systems",
      "reportdoc",
      "srcdoc",
      "procdoc",
      "asbuiltlink",
      "commelpsp",
      "leakdoc",
      "commswdoc",
      "commprocedures",
      "shelldoc",
    ]
  },
  { page: "racert", children: ["racertsystem"] },

  { page: "scopedrwgrev", children: ["scopedrwgrevsyslink"] },
  {
    page: "systems",
    children: [
      "areareportlink",
      "commdossiers",
      "components",
      "dossierreview",
      "engdocs",
      "fcrqvdarchive",
      "ibarchive",
      "punchlist",
      "qvdarchive",
      "scopedrwgrevsyslink",
      "sossystem",
      "fcrtpparchive",
      "racertsystem",
      "tpparchive",
      "vendordocs",
      "reportdoc",
      "shelldoc",
      "commswdoc",
      "leakdoc",
      "commelpsp",
      "racert",
      "srcdoc",
      "procdoc",
      "asbuiltlink",
      "completionstatus",
      "commdossierdoc"
    ]
  },
  { page: "units", children: ["systems", "punchlistspost"] },
  { page: "fcrtpparchive", children:["fcrtpparchivelink"]},
  { page: "racitation", children:["racitationfile"]}
];
