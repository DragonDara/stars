import { PageAccessor } from "./PageAccessors";

export let completionstatusRequest: string =
  "$select=Id,Compnumbers,Qvddone,Qvdtotal,Tppdone,Tpptotal,Loopsdone,Loopstotal,Apldone,Apltotal,Bpldone,Bpltotal,Ereddone,Eredtotal," +
  "Vendorkadone,Vendorkatotal,Vreddone,Vredtotal,Sd,Commereddone,Commvreddone,Commchecklistsdone,Commcheckliststotal,Proccountdone," +
  "Proccount,VreddoneFpr,VredtotalFpr,EreddoneFpr,EredtotalFpr,Apldonecm,Apltotalcm,Bpldonecm,Bpltotalcm,Commcheckloaded,Radone,Ratotal" +
  "&$expand=System($select=Systemno,Description,Descrus,Systemid;" +
  "$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;" +
  "$expand=Area($select=Name,Namerus,Description,Descrus)))";

export const completionstatusAccessor: PageAccessor = {
  page: "completionstatus",
  model: "Completionstatus",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Unit.Area.Name",
      modifiedKey: "Area Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Namerus",
      modifiedKey: "Area Code Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Description",
      modifiedKey: "Area Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Descrus",
      modifiedKey: "Area Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitno",
      modifiedKey: "Unit Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnorus",
      modifiedKey: "Unit Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitname",
      modifiedKey: "Unit Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnamerus",
      modifiedKey: "Unit Name Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Compnumbers",
      modifiedKey: "Components",
      unmodifiedKey: "Compnumbers"
    },
    {
      pathToValue: "Qvddone",
      modifiedKey: "QVD Complete",
      unmodifiedKey: "Qvddone"
    },
    {
      pathToValue: "Qvdtotal",
      modifiedKey: "QVD Total",
      unmodifiedKey: "Qvdtotal"
    },
    {
      pathToValue: "Tppdone",
      modifiedKey: "TPP Complete",
      unmodifiedKey: "Tppdone"
    },
    {
      pathToValue: "Tpptotal",
      modifiedKey: "TPP Total",
      unmodifiedKey: "Tpptotal"
    },
    {
      pathToValue: "Loopsdone",
      modifiedKey: "Loops Complete",
      unmodifiedKey: "Loopsdone"
    },
    {
      pathToValue: "Loopstotal",
      modifiedKey: "Loops Total",
      unmodifiedKey: "Loopsdone"
    },
    {
      pathToValue: "Apldone",
      modifiedKey: "A_Construction PL Complete",
      unmodifiedKey: "Apldone"
    },
    {
      pathToValue: "Apltotal",
      modifiedKey: "A_Construction PL Total",
      unmodifiedKey: "Apltotal"
    },
    {
      pathToValue: "Bpldone",
      modifiedKey: "B_Construction PL Complete",
      unmodifiedKey: "Bpldone"
    },
    {
      pathToValue: "Bpltotal",
      modifiedKey: "B_Construction PL Total",
      unmodifiedKey: "Bpltotal"
    },
    {
      pathToValue: "Ereddone",
      modifiedKey: "Engineering Redline Complete",
      unmodifiedKey: "Ereddone"
    },
    {
      pathToValue: "Eredtotal",
      modifiedKey: "Engineering Redline Total",
      unmodifiedKey: "Eredtotal"
    },
    {
      pathToValue: "Vendorkadone",
      modifiedKey: "Vendor Key Action Documents Complete",
      unmodifiedKey: "Vendorkadone"
    },
    {
      pathToValue: "Vendorkatotal",
      modifiedKey: "Vendor Key Action Documents Total",
      unmodifiedKey: "Vendorkatotal"
    },
    {
      pathToValue: "Vreddone",
      modifiedKey: "Vendor Redline Complete",
      unmodifiedKey: "Vreddone"
    },
    {
      pathToValue: "Vredtotal",
      modifiedKey: "Vendor Redline Total",
      unmodifiedKey: "Vredtotal"
    },
    {
      pathToValue: "Sd",
      modifiedKey: "Scope Drawing Quantity",
      unmodifiedKey: "Sd"
    },
    {
      pathToValue: "Commereddone",
      modifiedKey: "Commissioning Engineering Redline Complete",
      unmodifiedKey: "Commereddone"
    },
    {
      pathToValue: "Commvreddone",
      modifiedKey: "Commissioning Vendor Redline Complet",
      unmodifiedKey: "Commvreddone"
    },
    {
      pathToValue: "Commchecklistsdone",
      modifiedKey: "Commissioning Checklists Complete",
      unmodifiedKey: "Commchecklistsdone"
    },
    {
      pathToValue: "Commcheckliststotal",
      modifiedKey: "Commissioning Checklists Total",
      unmodifiedKey: "Commcheckliststotal"
    },
    {
      pathToValue: "Proccountdone",
      modifiedKey: "Procedures Complete",
      unmodifiedKey: "Proccountdone"
    },
    {
      pathToValue: "Proccount",
      modifiedKey: "Procedures Total",
      unmodifiedKey: "Proccount"
    },
    {
      pathToValue: "VreddoneFpr",
      modifiedKey: "Vendor Redline Complete_FPR",
      unmodifiedKey: "VreddoneFpr"
    },
    {
      pathToValue: "VredtotalFpr",
      modifiedKey: "Vendor Redline Total_FPR",
      unmodifiedKey: "VredtotalFpr"
    },
    {
      pathToValue: "EreddoneFpr",
      modifiedKey: "Engineering Redline Complete_FPR",
      unmodifiedKey: "EreddoneFpr"
    },
    {
      pathToValue: "EredtotalFpr",
      modifiedKey: "Engineering Redline Total_FPR",
      unmodifiedKey: "EredtotalFpr"
    },
    {
      pathToValue: "Apldonecm",
      modifiedKey: "A_Commissioning Punchlist Complete",
      unmodifiedKey: "Apldonecm"
    },
    {
      pathToValue: "Apltotalcm",
      modifiedKey: "A_Commissioning Punchlist Total",
      unmodifiedKey: "Apltotalcm"
    },
    {
      pathToValue: "Bpldonecm",
      modifiedKey: "B_Commissioning Punchlist Complete",
      unmodifiedKey: "Bpldonecm"
    },
    {
      pathToValue: "Bpltotalcm",
      modifiedKey: "B_Commissioning Punchlist Total",
      unmodifiedKey: "Bpltotalcm"
    },
    {
      pathToValue: "Commcheckloaded",
      modifiedKey: "Commissioning Checklists Loaded",
      unmodifiedKey: "Commcheckloaded"
    },
    {
      pathToValue: "Radone",
      modifiedKey: "Regulatory Affairs Complete",
      unmodifiedKey: "Radone"
    },
    {
      pathToValue: "Ratotal",
      modifiedKey: "Regulatory Affairs Total",
      unmodifiedKey: "Ratotal"
    },
    {
      pathToValue: "System.Systemid",
      modifiedKey: "System Id",
      unmodifiedKey: "System"
    }
  ]
};
