import { shelldocRequest } from "./ShelldocAccessor";
import { commswdocRequest } from "./CommswdocAccessor";
import { commelpspRequest } from "./CommelpspAccessor";
import { leakdocRequest } from "./LeakdocAccessor";
import { procdocRequest } from "./ProcdocAccessor";
import { srcdocRequest } from "./SrcdocAccessor";
import { reportdocRequest } from "./ReportdocAccessor";
import { engDocsRequest } from "./EngDocsAccessor";
import { vendorDocsRequest } from "./VendorDocsAccessor";
import { componentsRequest } from "./ComponentsAccessor";
import { systemRequest } from "./SystemAccessor";
import { unitRequest } from "./UnitAccessor";
import { areaRequest } from "./AreaAccessor";
import { documentNoRequest } from "./DocumentNoAccessor";
import { docfilelinkRequest } from "./DocfilelinkAccessor";
import { PageId } from "./Page";
import { punchlistRequest } from "./PunchlistAccessors";
import { procedureRequest } from "./ProcedureAccessors";
import { passportRequest } from "./PassportAccessors";
import { variationRequest } from "./VariationAccessor";
import { tpparchiveRequest } from "./TpparchiveAccessor";
import { commdossierRequest } from "./CommdossierAccessor";
import { fcrqvdarchiveRequest } from "./FcrqvdarchiveAccessor";
import { fcrtpparchiveRequest } from "./FcrtpparchiveAccessor";
import { ibarchiveRequest } from "./IbArchiveAccessor";
import { peerreviewRequest } from "./PeerreviewAccessor";
import { pmsdocdetailRequest } from "./PmsdocdetailAccessor";
import { punchlistspostRequest } from "./PunchlistspostAccessor";
import { qvdarchiveRequest } from "./QvdarchiveAccessor";
import { racertRequest } from "./RacertAccessor";
import { racertsystemRequest } from "./RacertsystemAccessor";
import { racitationRequest } from "./RacitationAccessor";
import { scopedrwgrevRequest } from "./ScopedrwgrevAccessor";
import { scopedrwgrevsyslinkRequest } from "./ScopedrwgrevsyslinkAccessor";
import { completionstatusRequest } from "./CompletionstatusAccessor";
import { areareportlinkRequest } from "./AreareportlinkAccessor";
import { dossierreviewRequest } from "./DossierreviewAccessor";
import { sossystemRequest } from "./SossystemAccessor";
import { commdossierdocRequest } from "./CommdossierdocAccessor";
import { asbuiltlinkRequest } from "./AsbuiltlinkAccessor";
import { fcrtpparchivelinkRequest } from "./FcrtpparchivelinkAccessor";
import { racitationfileRequest } from "./RacitationfileAccessor";

export function PageRequest(page: PageId): string {
  console.log(page);
  const pageRequest = PAGE_REQUESTS.find(pr => pr.page == page);
  console.log(pageRequest);
  return request ? pageRequest.request : null;
}

const PAGE_REQUESTS = [
  request("areas", areaRequest),
  request("units", unitRequest),
  request("systems", systemRequest),
  request("docFileLinks", docfilelinkRequest),
  request("components", componentsRequest),
  request("documentNumbers", documentNoRequest),
  request("engdocs", engDocsRequest),
  request("vendordocs", vendorDocsRequest),
  request("punchlist", punchlistRequest),
  request("commprocedures", procedureRequest),
  request("passports", passportRequest),
  request("variations", variationRequest),
  request("tpparchive", tpparchiveRequest),
  request("commdossiers", commdossierRequest),
  request("fcrqvdarchive", fcrqvdarchiveRequest),
  request("fcrtpparchive", fcrtpparchiveRequest),
  request("ibarchive", ibarchiveRequest),
  request("peerreview", peerreviewRequest),
  request("pmsdocdetail", pmsdocdetailRequest),
  request("punchlistspost", punchlistspostRequest),
  request("qvdarchive", qvdarchiveRequest),
  request("racert", racertRequest),
  request("racertsystem", racertsystemRequest),
  request("racitation", racitationRequest),
  request("scopedrwgrev", scopedrwgrevRequest),
  request("scopedrwgrevsyslink", scopedrwgrevsyslinkRequest),
  request("completionstatus", completionstatusRequest),
  request("areareportlink", areareportlinkRequest),
  request("dossierreview", dossierreviewRequest),
  request("sossystem", sossystemRequest),
  request("reportdoc", reportdocRequest),
  request("commdossierdoc", commdossierdocRequest),
  request("srcdoc", srcdocRequest),
  request("procdoc", procdocRequest),
  request("leakdoc", leakdocRequest),
  request("commelpsp", commelpspRequest),
  request("commswdoc", commswdocRequest),
  request("shelldoc", shelldocRequest),
  request("asbuiltlink", asbuiltlinkRequest),
  request("fcrtpparchivelink", fcrtpparchivelinkRequest),
  request("racitationfile",racitationfileRequest )
];

function request(page: PageId, request: string) {
  return { page, request };
}
