import { PageAccessor } from "./PageAccessors";

export let componentsRequest: string =
  "$select=Id,Tagid,Parenttagid" +
  "&$expand=System($select=Systemno,Description,Descrus),Componentno($select=Tagno,Description,Engstatus,Pono,Remark,Servdesc;" +
  "$expand=Discipline($select=Discipline1,Description,Descriptionrus)),ParentComponent($select=Tagno)";

export const componentsAccessor: PageAccessor = {
  page: "components",
  model: "Component",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Componentno.Tagno",
      modifiedKey: "Tag No",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Description",
      modifiedKey: "Tag Description",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Servdesc",
      modifiedKey: "Tag Service Description",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Pono",
      modifiedKey: "PurchaseOrderNumber",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Engstatus",
      modifiedKey: "Engineering Status",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Discipline.Discipline1",
      modifiedKey: "Discipline",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Discipline.Description",
      modifiedKey: "Discipline Description",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Discipline.Descriptionrus",
      modifiedKey: "Discipline Description Rus",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "ParentComponent.Tagno",
      modifiedKey: "Parent Tag No",
      unmodifiedKey: "ParentComponent"
    },
    {
      pathToValue: "Parenttagid",
      modifiedKey: "Parent Tag Id",
      unmodifiedKey: "Parenttagid"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Remark",
      modifiedKey: "Remarks",
      unmodifiedKey: "System"
    }
  ]
};
