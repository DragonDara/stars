import { PageAccessor } from "./PageAccessors";

export let scopedrwgrevsyslinkRequest: string =
  "$expand=System($select=Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus))),Scopedrwgrev($select=Apddate,Filename,Id;$expand=Scopedrwgsheet($expand=Docno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename))),Apduser($select=Usercai,Fullname))";
export const scopedrwgrevsyslinkAccessor: PageAccessor = {
  page: "scopedrwgrevsyslink",
  model: "Scopedrwgrevsyslink",
  accessors: [
    {
      pathToValue: "Scopedrwgrev.Scopedrwgsheet.Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Scopedrwgrev",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Unit.Area.Name",
      modifiedKey: "Area Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Namerus",
      modifiedKey: "Area Code Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Description",
      modifiedKey: "Area Description",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Descrus",
      modifiedKey: "Area Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitno",
      modifiedKey: "Unit Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnorus",
      modifiedKey: "Unit Code Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitname",
      modifiedKey: "Unit Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnamerus",
      modifiedKey: "Unit Name Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Scopedrwgrev.Scopedrwgsheet.Docno.Title",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Scopedrwgrev"
    },
    {
      pathToValue: "Scopedrwgrev.Scopedrwgsheet.Docno.Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Scopedrwgrev"
    },
    {
      pathToValue: "Scopedrwgrev.Apddate",
      modifiedKey: "Approved Date",
      unmodifiedKey: "Scopedrwgrev",
      dataType: "date"
    },
    {
      pathToValue: "Scopedrwgrev.Apduser.Usercai",
      modifiedKey: "Approved User CAI",
      unmodifiedKey: "Scopedrwgrev"
    },
    {
      pathToValue: "Scopedrwgrev.Apduser.Fullname",
      modifiedKey: "Approved User Fullname",
      unmodifiedKey: "Scopedrwgrev"
    },
    {
      pathToValue: "FilenaScopedrwgrev.Filename",
      modifiedKey: "Scope Drawing Link",
      unmodifiedKey: "FilenaScopedrwgrev",
      dataType: "link"
    },
    {
      pathToValue: "Scopedrwgrev.Id",
      modifiedKey: "Scope Drawing Rev Id",
      unmodifiedKey: "Scopedrwgrev"
    }
  ]
};
