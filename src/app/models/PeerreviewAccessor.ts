import { PageAccessor } from "./PageAccessors";

export let peerreviewRequest: string =
  "$select=Id,Itemno,Subitemno,Subteam,Activity,Docforreview,Observation,Actiontoconsider,Status,Respnote,Category" +
  "&$expand=Area($select=Name,Namerus,Description,Descrus)";

export const peerreviewAccessor: PageAccessor = {
  page: "peerreview",
  model: "Peer",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Itemno",
      modifiedKey: "Item No",
      unmodifiedKey: "Itemno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Subitemno",
      modifiedKey: "Sub Item No",
      unmodifiedKey: "Subitemno"
    },
    {
      pathToValue: "Subteam",
      modifiedKey: "Sub Team",
      unmodifiedKey: "Subteam"
    },
    {
      pathToValue: "Activity",
      modifiedKey: "Activity",
      unmodifiedKey: "Activity"
    },
    {
      pathToValue: "Docforreview",
      modifiedKey: "Document for review",
      unmodifiedKey: "Docforreview"
    },
    {
      pathToValue: "Observation",
      modifiedKey: "Observation",
      unmodifiedKey: "Observation"
    },
    {
      pathToValue: "Actiontoconsider",
      modifiedKey: "Action to consider",
      unmodifiedKey: "Actiontoconsider"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Status",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "Respnote",
      modifiedKey: "Resp Note",
      unmodifiedKey: "Respnote"
    },
    {
      pathToValue: "Category",
      modifiedKey: "Category",
      unmodifiedKey: "Category"
    },
    {
      pathToValue: "Area.Name",
      modifiedKey: "Area Name Eng",
      unmodifiedKey: "Area"
    },
    {
      pathToValue: "Area.Namerus",
      modifiedKey: "Area Name Rus",
      unmodifiedKey: "Area"
    },
    {
      pathToValue: "Area.Description",
      modifiedKey: "Area Description Eng",
      unmodifiedKey: "Area"
    },
    {
      pathToValue: "Area.Descrus",
      modifiedKey: "Area Description Rus",
      unmodifiedKey: "Area"
    }
  ]
};
