import { PageAccessor } from "./PageAccessors";

export let commdossierRequest: string =
  "$select=id,Comments,Systemid" +
  "&$expand=Docno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename)),System($select=Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus)))";

export const commdossierAccessor: PageAccessor = {
  page: "commdossiers",
  model: "Commdossierdoc",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Name",
      modifiedKey: "Area Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Namerus",
      modifiedKey: "Area Name Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Description",
      modifiedKey: "Area Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Descrus",
      modifiedKey: "Area Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitno",
      modifiedKey: "Unit Number Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnorus",
      modifiedKey: "Unit Number Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitname",
      modifiedKey: "Unit Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnamerus",
      modifiedKey: "Unit Name Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Title",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Docfilelinks.Filename",
      modifiedKey: "Document Links",
      unmodifiedKey: "Docno",
      dataType: "link"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Docno.Docfilelinks.Filename",
      modifiedKey: "Document Links",
      unmodifiedKey: "Docno",
      dataType: "link"
    }
  ]
};
