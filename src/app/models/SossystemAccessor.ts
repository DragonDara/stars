import { PageAccessor } from "./PageAccessors";

export let sossystemRequest: string =
  "$select=Id,Sositemid,Comments,Commentsrus" +
  "&$expand=System($select=Systemno,Description,Descrus),Sositem($select=Sositemeng,Sositemrus,Description,Descrus;$expand=SosPeople($expand=User($select=Usercai,Fullname))),Signuser($select=Usercai,Fullname)";

export const sossystemAccessor: PageAccessor = {
  page: "sossystem",
  model: "SosSystem",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Sositemid",
      modifiedKey: "Sos Item Id",
      unmodifiedKey: "Sositemid"
    },
    {
      pathToValue: "Sositem.Sositemeng",
      modifiedKey: "Sos Item Eng",
      unmodifiedKey: "Sositem"
    },
    {
      pathToValue: "Sositem.Sositemrus",
      modifiedKey: "Sos Item Rus",
      unmodifiedKey: "Sositem"
    },
    {
      pathToValue: "Sositem.Description",
      modifiedKey: "Sos Item Description",
      unmodifiedKey: "Sositem"
    },
    {
      pathToValue: "Sositem.Descrus",
      modifiedKey: "Sos Item Description Rus",
      unmodifiedKey: "Sositem"
    },
    {
      pathToValue: "Sositem.SosPeople.User.Usercai",
      modifiedKey: "User CAI",
      unmodifiedKey: "Sositem"
    },
    {
      pathToValue: "Sositem.SosPeople.User.Fullname",
      modifiedKey: "User Fullname",
      unmodifiedKey: "Sositem"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "SOS Comments Eng",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Commentsrus",
      modifiedKey: "SOS Comments Rus",
      unmodifiedKey: "Commentsrus"
    },
    {
      pathToValue: "Signuser.Usercai",
      modifiedKey: "SOS Sign CAI",
      unmodifiedKey: "Sositem"
    },
    {
      pathToValue: "Siguser.Fullname",
      modifiedKey: "SOS Sign Fullname",
      unmodifiedKey: "Sositem"
    }
  ]
};
