import { PageAccessor } from "./PageAccessors";
export let racitationfileRequest: string =
  "$select=Citationfile,Statusfile" +
  "&$expand=Citation($select=Id)";

export const racitationfileAccessor: PageAccessor = {
  page: "racitationfile",
  model: "Racitationfile",
  accessors: [
    {
        pathToValue: "Citation.Citationid",
        modifiedKey: "Id",
        unmodifiedKey: "Citation",
    },
    {
        pathToValue: "Citationfile",
        modifiedKey: "Citationfile",
        unmodifiedKey: "Citationfile"
    },
    {
        pathToValue: "Statusfile",
        modifiedKey: "Statusfile",
        unmodifiedKey: "Statusfile"
    }
  ]
};