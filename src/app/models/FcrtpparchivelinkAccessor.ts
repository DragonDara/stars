import { PageAccessor } from "./PageAccessors";
export let fcrtpparchivelinkRequest: string =
  "$select=Tpplink" +
  "&$expand=Tpp($select=Id)";

export const fcrtpparchivelinkAccessor: PageAccessor = {
  page: "fcrtpparchivelink",
  model: "Fcrtpparchivelink",
  accessors: [
    {
        pathToValue: "Tpp.Tppid",
        modifiedKey: "Id",
        unmodifiedKey: "Tpp",
    },
    {
        pathToValue: "Tpplink",
        modifiedKey: "Tpplink",
        unmodifiedKey: "Tpplink"
    }
  ]
};