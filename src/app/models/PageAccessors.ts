import { shelldocAccessor } from "./ShelldocAccessor";
import { commswdocAccessor } from "./CommswdocAccessor";
import { commelpspAccessor } from "./CommelpspAccessor";
import { leakdocAccessor } from "./LeakdocAccessor";
import { procdocAccessor } from "./ProcdocAccessor";
import { srcdocAccessor } from "./SrcdocAccessor";
import { reportdocAccessor } from "./ReportdocAccessor";
import { racertsystemAccessor } from "./RacertsystemAccessor";
import { engDocsAccessor } from "./EngDocsAccessor";
import { vendorDocsAccessor } from "./VendorDocsAccessor";
import { Accessor, PageId, ModelId } from "./Page";
import { systemAccessor } from "./SystemAccessor";
import { unitAccessor } from "./UnitAccessor";
import { areaAccessor } from "./AreaAccessor";
import { documentNoAccessor } from "./DocumentNoAccessor";
import { docfilelinkAccessor } from "./DocfilelinkAccessor";
import { componentsAccessor } from "./ComponentsAccessor";
import { punchlistAccessor } from "./PunchlistAccessors";
import { procedureAccessor } from "./ProcedureAccessors";
import { passportAccessor } from "./PassportAccessors";
import { variationAccessor } from "./VariationAccessor";
import { tpparchiveAccessor } from "./TpparchiveAccessor";
import { commdossierAccessor } from "./CommdossierAccessor";
import { fcrqvdarchiveAccessor } from "./FcrqvdarchiveAccessor";
import { fcrtpparchiveAccessor } from "./FcrtpparchiveAccessor";
import { ibarchiveAccessor } from "./IbArchiveAccessor";
import { peerreviewAccessor } from "./PeerreviewAccessor";
import { pmsdocdetailAccessor } from "./PmsdocdetailAccessor";
import { punchlistspostAccessor } from "./PunchlistspostAccessor";
import { qvdarchiveAccessor } from "./QvdarchiveAccessor";
import { racertAccessor } from "./RacertAccessor";
import { racitationAccessor } from "./RacitationAccessor";
import { scopedrwgrevAccessor } from "./ScopedrwgrevAccessor";
import { scopedrwgrevsyslinkAccessor } from "./ScopedrwgrevsyslinkAccessor";
import { completionstatusAccessor } from "./CompletionstatusAccessor";
import { areareportlinkAccessor } from "./AreareportlinkAccessor";
import { dossierreviewAccessor } from "./DossierreviewAccessor";
import { sossystemAccessor } from "./SossystemAccessor";
import { commdossierdocAccessor } from "./CommdossierdocAccessor";
import { asbuiltlinkAccessor } from "./AsbuiltlinkAccessor";
import { fcrtpparchivelinkAccessor } from "./FcrtpparchivelinkAccessor";
import { racitationfileAccessor } from "./RacitationfileAccessor"

export class PageAccessor {
  page: PageId;
  model: ModelId;
  accessors: Accessor[];
}

export function PageAccessors(page: PageId): PageAccessor {
  return PAGE_ACCESSORS.find(pa => pa.page == page);
}

const PAGE_ACCESSORS: PageAccessor[] = [
  systemAccessor,
  unitAccessor,
  areaAccessor,
  documentNoAccessor,
  docfilelinkAccessor,
  componentsAccessor,
  engDocsAccessor,
  vendorDocsAccessor,
  punchlistAccessor,
  procedureAccessor,
  passportAccessor,
  variationAccessor,
  tpparchiveAccessor,
  commdossierAccessor,
  fcrqvdarchiveAccessor,
  fcrtpparchiveAccessor,
  ibarchiveAccessor,
  peerreviewAccessor,
  pmsdocdetailAccessor,
  punchlistspostAccessor,
  qvdarchiveAccessor,
  racertAccessor,
  racertsystemAccessor,
  racitationAccessor,
  scopedrwgrevAccessor,
  scopedrwgrevsyslinkAccessor,
  completionstatusAccessor,
  areareportlinkAccessor,
  dossierreviewAccessor,
  sossystemAccessor,
  reportdocAccessor,
  commdossierdocAccessor,
  srcdocAccessor,
  procdocAccessor,
  leakdocAccessor,
  commelpspAccessor,
  commswdocAccessor,
  shelldocAccessor,
  asbuiltlinkAccessor,
  fcrtpparchivelinkAccessor,
  racitationfileAccessor
];
