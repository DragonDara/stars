import { PageAccessor } from "./PageAccessors";

export let racitationRequest: string =
  "$select=Id,Tcno,Type,Agency,Itemno,Inspector,Issuedate,Coc,Comments,Raperson,Department,Clearpr,Contact,Statuspen,Letterdate,Status,So,Areamanager" +
  "&$expand=Area($select=Name,Description),Racitationfile($select=Citationfile,Statusfile)";

export const racitationAccessor: PageAccessor = {
  page: "racitation",
  model: "Racitation",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Tcno",
      modifiedKey: "Technical Commission",
      unmodifiedKey: "Tcno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Area.Name",
      modifiedKey: "Area Name",
      unmodifiedKey: "Area"
    },
    {
      pathToValue: "Area.Description",
      modifiedKey: "Area Description",
      unmodifiedKey: "Area"
    },
    {
      pathToValue: "Type",
      modifiedKey: "Type",
      unmodifiedKey: "Type"
    },
    {
      pathToValue: "Agency",
      modifiedKey: "Agency",
      unmodifiedKey: "Agency"
    },
    {
      pathToValue: "Itemno",
      modifiedKey: "Item Number",
      unmodifiedKey: "Itemno"
    },
    {
      pathToValue: "Inspector",
      modifiedKey: "Inspector",
      unmodifiedKey: "Inspector"
    },
    {
      pathToValue: "Issuedate",
      modifiedKey: "Issue Date",
      unmodifiedKey: "Issuedate",
      dataType: "date"
    },
    {
      pathToValue: "Coc",
      modifiedKey: "COC",
      unmodifiedKey: "Coc"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Department",
      modifiedKey: "Department",
      unmodifiedKey: "Department"
    },
    {
      pathToValue: "Clearpr",
      modifiedKey: "Clear Pr",
      unmodifiedKey: "Clearpr"
    },
    {
      pathToValue: "Contact",
      modifiedKey: "Contact",
      unmodifiedKey: "Contact"
    },
    {
      pathToValue: "Statuspen",
      modifiedKey: "Statuspen",
      unmodifiedKey: "Statuspen"
    },
    {
      pathToValue: "Letterdate",
      modifiedKey: "Letter Date",
      unmodifiedKey: "Letterdate",
      dataType: "date"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Status",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "So",
      modifiedKey: "So",
      unmodifiedKey: "So"
    },
    {
      pathToValue: "Areamanager",
      modifiedKey: "Areamanager",
      unmodifiedKey: "Areamanager"
    },
    {
      pathToValue: "Racitationfile.Citationfile",
      modifiedKey: "Citation Filelink",
      unmodifiedKey: "Racitationfile",
      dataType: "date"
    },
    {
      pathToValue: "Racitationfile.Statusfile",
      modifiedKey: "Status Filelink",
      unmodifiedKey: "Racitationfile",
      dataType: "date"
    }
  ]
};
