import { PageAccessor } from "./PageAccessors";

export let racertsystemRequest: string =
  "$select=Id" +
  "&$expand=System($select=Areano,Unitno,Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus))),Cert($select=Certnum,Description,Pono,Supplier,Obtained,Expiry,Elfilestatus;$expand=Docno($select=Docno;$expand=Docfilelinks($select=Filename)),Org($select=Organization,Description))";

export const racertsystemAccessor: PageAccessor = {
  page: "racertsystem",
  model: "Racertsystem",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Cert.Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Cert",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Areano",
      modifiedKey: "Area No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unitno",
      modifiedKey: "Unit No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Name",
      modifiedKey: "Area Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Namerus",
      modifiedKey: "Area Code Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Description",
      modifiedKey: "Area Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Descrus",
      modifiedKey: "Area Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitno",
      modifiedKey: "Unit Code Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnorus",
      modifiedKey: "Unit Code Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitname",
      modifiedKey: "Unit Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitnamerus",
      modifiedKey: "Unit Name Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Cert.Org.Organization",
      modifiedKey: "Cert Type",
      unmodifiedKey: "Cert"
    },
    {
      pathToValue: "Cert.Org.Description",
      modifiedKey: "Cert Description",
      unmodifiedKey: "Cert"
    },
    {
      pathToValue: "Cert.Certnum",
      modifiedKey: "Certification Number",
      unmodifiedKey: "Cert"
    },
    {
      pathToValue: "Cert.Description",
      modifiedKey: "Certification Description",
      unmodifiedKey: "Cert"
    },
    {
      pathToValue: "Cert.Pono",
      modifiedKey: "Purchase Order No",
      unmodifiedKey: "Cert"
    },
    {
      pathToValue: "Cert.Supplier",
      modifiedKey: "Supplier",
      unmodifiedKey: "Cert"
    },
    {
      pathToValue: "Cert.Obtained",
      modifiedKey: "Obtained Date",
      unmodifiedKey: "Cert",
      dataType: "date"
    },
    {
      pathToValue: "Cert.Expiry",
      modifiedKey: "Expiration Date",
      unmodifiedKey: "Cert",
      dataType: "date"
    },
    {
      pathToValue: "Cert.Elfilestatus",
      modifiedKey: "Electronic File Status",
      unmodifiedKey: "Cert"
    },
    {
      pathToValue: "Cert.Docno.Docfilelinks.Filename",
      modifiedKey: "Document File",
      unmodifiedKey: "Cert",
      dataType:"link"
    }
  ]
};
