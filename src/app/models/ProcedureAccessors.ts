import { PageAccessor } from "./PageAccessors";
export let procedureRequest: string =
  "$select=Id,Statusid,Commgroupid,Execstatusid" +
  "&$expand=Docno($select=Id,Docno,Doctype,Title,Titlerus;$expand=Docfilelinks($select=Filename)),Proctype($select=Tcoproctype,Description)";

export const procedureAccessor: PageAccessor = {
  page: "commprocedures",
  model: "Procedure",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Docno.Title",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Doctype",
      modifiedKey: "Doc type",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Proctype.Tcoproctype",
      modifiedKey: "Procedure Type Code",
      unmodifiedKey: "Proctype"
    },
    {
      pathToValue: "Proctype.Description",
      modifiedKey: "Procedure Type Description",
      unmodifiedKey: "Proctype"
    },
    {
      pathToValue: "Statusid",
      modifiedKey: "Procedure Status Code",
      unmodifiedKey: "Statusid"
    },
    {
      pathToValue: "Commgroupid",
      modifiedKey: "Commissioning Group Id",
      unmodifiedKey: "Commgroupid"
    },
    {
      pathToValue: "Execstatusid",
      modifiedKey: "Procedure Exec Status Code",
      unmodifiedKey: "Execstatusid"
    },
    {
      pathToValue: "Docno.Docfilelinks.Filename",
      modifiedKey: "Doc Document Link",
      unmodifiedKey: "Docno"
    }
  ]
};
