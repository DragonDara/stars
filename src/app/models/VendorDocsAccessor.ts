import { PageAccessor } from "./PageAccessors";

export let vendorDocsRequest: string =
  "$select=Id" +
  "&$expand=Tcodocno($select=Docno,Clientdocno,Title,Titlerus,Palastrev,Issuestatus,Pono,Pkgitem,Vendor,Fis,Disc,Hostatus;" +
  "$expand=Doctype1($select=Doctype,Title,Titlerus,Timing,Priority,Kd,Keydoc,Ad,Actiondoc,Abreq,Abreqdoc))";

export const vendorDocsAccessor: PageAccessor = {
  page: "vendordocs",
  model: "Vendordoc",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Tcodocno.Docno",
      modifiedKey: "Vendor Document Number",
      unmodifiedKey: "Tcodocno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Tcodocno.Clientdocno",
      modifiedKey: "TCO Document Number",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Title",
      modifiedKey: "Title",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Palastrev",
      modifiedKey: "Palastrev",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Issuestatus",
      modifiedKey: "Issue status",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Pono",
      modifiedKey: "Po No",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Pkgitem",
      modifiedKey: "Equipment Package Item",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Vendor",
      modifiedKey: "Vendor",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Fis",
      modifiedKey: "FIS",
      unmodifiedKey: "Tcodocno"
    },

    {
      pathToValue: "Tcodocno.Disc",
      modifiedKey: "Discipline",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Doctype1.Doctype",
      modifiedKey: "Vendor Doc Type Code",
      unmodifiedKey: "Tcodocno"
    },

    {
      pathToValue: "Tcodocno.Doctype1.Title",
      modifiedKey: "Vendor Doc Type Title",
      unmodifiedKey: "Tcodocno"
    },

    {
      pathToValue: "Tcodocno.Doctype1.Titlerus",
      modifiedKey: "Vendor Doc Type Title Rus",
      unmodifiedKey: "Tcodocno"
    },

    {
      pathToValue: "Tcodocno.Doctype1.Timing",
      modifiedKey: "Timing",
      unmodifiedKey: "Tcodocno"
    },

    {
      pathToValue: "Tcodocno.Doctype1.Priority",
      modifiedKey: "Priority",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Doctype1.Kd",
      modifiedKey: "Key Document Flag",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Doctype1.Keydoc",
      modifiedKey: "Is Vendor Key Doc",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Doctype1.Ad",
      modifiedKey: "Action Document Flag",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Doctype1.Actiondoc",
      modifiedKey: "Is Vendor Action Doc",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Doctype1.Abreq",
      modifiedKey: "As Built Required Flag",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Doctype1.Abreqdoc",
      modifiedKey: "As Built Required Text",
      unmodifiedKey: "Tcodocno"
    },
    {
      pathToValue: "Tcodocno.Hostatus",
      modifiedKey: "Handover Status",
      unmodifiedKey: "Tcodocno"
    }
  ]
};
