import { PageAccessor } from "./PageAccessors";

export let documentNoRequest: string =
  "$select=Id,Docno,Title,Titlerus,Palastrev,Fis,Doctype,Hostatus,Keydoc,Actiondoc,Abreq,Issuestatus,Clientdocno,Disc,Pono,Pkgitem,Doctypeid,Vendor";

export const documentNoAccessor: PageAccessor = {
  page: "documentNumbers",
  model: "Documentno",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Title",
      modifiedKey: "Title",
      unmodifiedKey: "Title"
    },
    {
      pathToValue: "Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Titlerus"
    },
    {
      pathToValue: "Palastrev",
      modifiedKey: "Palastrev",
      unmodifiedKey: "Palastrev"
    },
    {
      pathToValue: "Doctype",
      modifiedKey: "Document Type",
      unmodifiedKey: "Doctype"
    },
    {
      pathToValue: "Hostatus",
      modifiedKey: "Handover Status",
      unmodifiedKey: "Hostatus"
    },
    {
      pathToValue: "Keydoc",
      modifiedKey: "Key Document",
      unmodifiedKey: "Keydoc"
    },
    {
      pathToValue: "Actiondoc",
      modifiedKey: "Action Document",
      unmodifiedKey: "Actiondoc"
    },
    {
      pathToValue: "Issuestatus",
      modifiedKey: "Issue status",
      unmodifiedKey: "Issuestatus"
    },
    {
      pathToValue: "Clientdocno",
      modifiedKey: "Client Document No",
      unmodifiedKey: "Clientdocno"
    },
    {
      pathToValue: "Disc",
      modifiedKey: "Discipline",
      unmodifiedKey: "Disc"
    },
    {
      pathToValue: "Pono",
      modifiedKey: "Po No",
      unmodifiedKey: "Pono"
    },
    {
      pathToValue: "Pkgitem",
      modifiedKey: "PKG item",
      unmodifiedKey: "Pkgitem"
    },
    {
      pathToValue: "Doctypeid",
      modifiedKey: "Document Type Id",
      unmodifiedKey: "Doctypeid"
    },
    {
      pathToValue: "Vendor",
      modifiedKey: "Vendor",
      unmodifiedKey: "Vendor"
    }
  ]
};
