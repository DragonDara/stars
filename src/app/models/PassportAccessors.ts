import { PageAccessor } from "./PageAccessors";
export let passportRequest: string =
  "$select=Equipment,Registrdate,Comments,Permittooperate" +
  "&$expand=Cpdocno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename))" +
  ",Docno($select=Docno;$expand=Docfilelinks($select=Filename))";
//ERR_INCOMPLETE_CHUNKED_ENCODING
// ",Componentno($select=Tagno,Description,Servdesc,Pono)";

export const passportAccessor: PageAccessor = {
  page: "passports",
  model: "PassportNew",
  accessors: [
    {
      pathToValue: "Equipment",
      modifiedKey: "Equipment Class",
      unmodifiedKey: "Equipment"
    },
    {
      pathToValue: "Registrdate",
      modifiedKey: "Register Date",
      unmodifiedKey: "Registrdate",
      dataType: "date"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Permittooperate",
      modifiedKey: "Permit to operate",
      unmodifiedKey: "Permittooperate"
    },
    {
      pathToValue: "Componentno.Tagno",
      modifiedKey: "Tag No",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Description",
      modifiedKey: "Component Description",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Servdesc",
      modifiedKey: "Component Serv Description",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Componentno.Pono",
      modifiedKey: "Po No",
      unmodifiedKey: "Componentno"
    },
    {
      pathToValue: "Cpdocno.Docno",
      modifiedKey: "Cp Document No",
      unmodifiedKey: "Cpdocno"
    },
    {
      pathToValue: "Cpdocno.Title",
      modifiedKey: "Cp Document Title",
      unmodifiedKey: "Cpdocno"
    },
    {
      pathToValue: "Cpdocno.Titlerus",
      modifiedKey: "Cp Document Title Rus",
      unmodifiedKey: "Cpdocno"
    },
    {
      pathToValue: "Cpdocno.Docfilelinks.Filename",
      modifiedKey: "Cp Document Filelink",
      unmodifiedKey: "Cpdocno"
    },
    {
      pathToValue: "Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Docfilelinks.Filename",
      modifiedKey: "File link",
      unmodifiedKey: "Docno"
    }
  ]
};
