import { PageAccessor } from "./PageAccessors";

export let unitRequest: string =
  "" +
  "$select=Unitid,Unitno,Unitname,Unitnorus,Unitnamerus,Closed,Commdate,Virtual,AreaId" +
  "&$expand=Area($select=Name,Description)";

export const unitAccessor: PageAccessor = {
  page: "units",
  model: "Unit",
  accessors: [
    {
      pathToValue: "Unitid",
      modifiedKey: "Unit Id",
      unmodifiedKey: "Unitid",
      dataType: "hidden"
    },
    {
      pathToValue: "Unitno",
      modifiedKey: "Unit No",
      unmodifiedKey: "Unitno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Unitname",
      modifiedKey: "Unit Name",
      unmodifiedKey: "Unitname"
    },
    {
      pathToValue: "Areaid",
      modifiedKey: "Area Id",
      unmodifiedKey: "Areaid"
    },
    {
      pathToValue: "Closed",
      modifiedKey: "Closed",
      unmodifiedKey: "Closed",
      dataType: "boolean"
    },
    {
      pathToValue: "Virtual",
      modifiedKey: "Virtual",
      unmodifiedKey: "Virtual",
      dataType: "boolean"
    },
    {
      pathToValue: "Unitnorus",
      modifiedKey: "Unit No Rus",
      unmodifiedKey: "Unitnorus"
    },
    {
      pathToValue: "Unitnamerus",
      modifiedKey: "Unit Name Rus",
      unmodifiedKey: "Unitnamerus"
    },
    {
      pathToValue: "Commdate",
      modifiedKey: "Unit Commissioning Date",
      unmodifiedKey: "Commdate",
      dataType: "date"
    },
    {
      pathToValue: "Area.Name",
      modifiedKey: "Area Name",
      unmodifiedKey: "Area"
    },
    {
      pathToValue: "Area.Description",
      modifiedKey: "Area Description",
      unmodifiedKey: "Area"
    }
  ]
};
