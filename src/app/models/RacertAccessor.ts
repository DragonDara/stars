import { PageAccessor } from "./PageAccessors";

export let racertRequest: string =
  "$select=Id,Certnum,Description,Pono,Supplier,Obtained,Expiry,Elfilestatus" +
  "&$expand=Org($select=Organization,Description),Docno($select=Docno;$expand=Docfilelinks($select=Filename))";

export const racertAccessor: PageAccessor = {
  page: "racert",
  model: "Racert",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Certnum",
      modifiedKey: "Number",
      unmodifiedKey: "Certnum",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Org.Organization",
      modifiedKey: "Type",
      unmodifiedKey: "Org"
    },
    {
      pathToValue: "Org.Description",
      modifiedKey: "Description",
      unmodifiedKey: "Org"
    },
    {
      pathToValue: "Description",
      modifiedKey: "Description",
      unmodifiedKey: "Description"
    },
    {
      pathToValue: "Pono",
      modifiedKey: "Purchase Order No",
      unmodifiedKey: "Pono"
    },
    {
      pathToValue: "Supplier",
      modifiedKey: "Supplier",
      unmodifiedKey: "Supplier"
    },
    {
      pathToValue: "Obtained",
      modifiedKey: "Obtained Date",
      unmodifiedKey: "Obtained",
      dataType: "date"
    },
    {
      pathToValue: "Expiry",
      modifiedKey: "Expiration Date",
      unmodifiedKey: "Expiry",
      dataType: "date"
    },
    {
      pathToValue: "Elfilestatus",
      modifiedKey: "Electronic File Status",
      unmodifiedKey: "Elfilestatus"
    },
    {
      pathToValue: "Elfilestatus",
      modifiedKey: "Electronic File Status",
      unmodifiedKey: "Elfilestatus"
    },
    {
      pathToValue: "Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Docfilelinks.Filename",
      modifiedKey: "Document File",
      unmodifiedKey: "Docno"
    }
  ]
};
