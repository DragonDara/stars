import { PageAccessor } from "./PageAccessors";

export let ibarchiveRequest: string =
  "$select=Id,Clientdocno,Pfdno,Title,Booklocation,Status,Comments,Systemid" +
  "&$expand=Contract($select=Company),System($select=Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitname;$expand=Area($select=Name,Description)))";

export const ibarchiveAccessor: PageAccessor = {
  page: "ibarchive",
  model: "IbArchive",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "primaryKey"
    },
    {
      pathToValue: "System.Systemno",
      modifiedKey: "System No",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Description",
      modifiedKey: "System Description Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Descrus",
      modifiedKey: "System Description Rus",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Name",
      modifiedKey: "Area Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Area.Description",
      modifiedKey: "Area Description Eng",
      unmodifiedKey: "System"
    },

    {
      pathToValue: "System.Unit.Unitno",
      modifiedKey: "Unit Number Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "System.Unit.Unitname",
      modifiedKey: "Unit Name Eng",
      unmodifiedKey: "System"
    },
    {
      pathToValue: "Clientdocno",
      modifiedKey: "Client Document No",
      unmodifiedKey: "Clientdocno"
    },
    {
      pathToValue: "Pfdno",
      modifiedKey: "Pfd No",
      unmodifiedKey: "Pfdno"
    },
    {
      pathToValue: "Title",
      modifiedKey: "Title",
      unmodifiedKey: "Title"
    },
    {
      pathToValue: "Booklocation",
      modifiedKey: "Book location",
      unmodifiedKey: "Booklocation"
    },
    {
      pathToValue: "Status",
      modifiedKey: "Status",
      unmodifiedKey: "Status"
    },
    {
      pathToValue: "Comments",
      modifiedKey: "Comments",
      unmodifiedKey: "Comments"
    },
    {
      pathToValue: "Contract.Company",
      modifiedKey: "Contractor",
      unmodifiedKey: "Contract"
    }
  ]
};
