import { PageAccessor } from "./PageAccessors";

export let engDocsRequest: string =
  "$select=Id" +
  "&$expand=Docno($select=Docno,Title,Titlerus,Palastrev,Fis,Doctype,Hostatus,Keydoc,Actiondoc,Abreq,Issuestatus,Clientdocno,Disc,Pono,Pkgitem,Doctypeid;" +
  "$expand=DoctypeNavigation($select=Doctype,Title,Abreqdoc,Timing,Abreq,Compdoc,Priority,Cdtype))";

export const engDocsAccessor: PageAccessor = {
  page: "engdocs",
  model: "Engdoc",
  accessors: [
    {
      pathToValue: "Id",
      modifiedKey: "Id",
      unmodifiedKey: "Id",
      dataType: "hidden"
    },
    {
      pathToValue: "Docno.Docno",
      modifiedKey: "Document No",
      unmodifiedKey: "Docno",
      dataType: "primaryKey"
    },
    {
      pathToValue: "Docno.Title",
      modifiedKey: "Title Eng",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Titlerus",
      modifiedKey: "Title Rus",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Palastrev",
      modifiedKey: "Revision",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Fis",
      modifiedKey: "Final Issue Status",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Doctype",
      modifiedKey: "Doc type",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Hostatus",
      modifiedKey: "Handover Status",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Keydoc",
      modifiedKey: "Key Doc",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Actiondoc",
      modifiedKey: "Action Doc",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Abreq",
      modifiedKey: "AB Req",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Issuestatus",
      modifiedKey: "Issue status",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Clientdocno",
      modifiedKey: "Client Doc No",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Disc",
      modifiedKey: "Discipline",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Pono",
      modifiedKey: "Po No",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Pkgitem",
      modifiedKey: "MOC Package",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Doctypeid",
      modifiedKey: "Doc type id",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.Vendor",
      modifiedKey: "Vendor",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Doctype",
      modifiedKey: "Doc type Code",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Title",
      modifiedKey: "Doc type Title",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Abreqdoc",
      modifiedKey: "Doc type AB Required",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Timing",
      modifiedKey: "Timing",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Abreq",
      modifiedKey: "Doc type AB Required Flag",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Compdoc",
      modifiedKey: "Completions Document",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Priority",
      modifiedKey: "Priority",
      unmodifiedKey: "Docno"
    },
    {
      pathToValue: "Docno.DoctypeNavigation.Cdtype",
      modifiedKey: "CD Type",
      unmodifiedKey: "Docno"
    }
  ]
};
