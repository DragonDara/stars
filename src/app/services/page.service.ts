import { environment } from "src/environments/environment";
import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
@Injectable({ providedIn: "root" })
export class PageService {
  constructor(private http: HttpClient) {}

  private url: string = environment.apiUrl;
  public Entries(request: string): Observable<any[]> {
    console.log("PageService");
    console.log(`${this.url}/${request}`);
    return this.http.get<any[]>(`${this.url}/${request}`);
  }

  public Count(request: string): Observable<number> {
    return this.http.get<number>(`${this.url}/count/${request}`);
  }
}
