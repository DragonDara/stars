import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IEffect } from "../components/page-table/page-table.component";

@Injectable({ providedIn: "root" })
export class EffectService {
  public readonly announcements = new BehaviorSubject<IEffect>(null);
  private value: IEffect = null;

  constructor() {}

  public Announce(effect: IEffect) {
    this.value = effect;
    this.announcements.next(effect);
  }

  public get lastValue() {
    return this.value;
  }
}
