export class ModuleConfig {
  publishName: string;
  routeName: string;
  tableName: string;
  config: string[];
  tableColumns: TableColumn[];
  relatedModules?: string[];
  relatedColumns?: string[];
  keyColumn?: string;
  
  constructor(publishName: string, routeName: string, tableName: string, config: string[], tableColumns: TableColumn[], relatedModules?: string[], relatedColumns?: string[], keyColumn?: string) {
    this.publishName = publishName;
    this.routeName = routeName;
    this.tableName = tableName;
    this.config = config;
    this.tableColumns = tableColumns;
    this.relatedModules = relatedModules;
    this.relatedColumns = relatedColumns;
    this.keyColumn = keyColumn;
  }

  public GetQueryString(
    addFilters?: string
  ): string {
    var cfg = [...this.config];

    if (addFilters) {
      const filterIdx = cfg.findIndex(c => c.substr(0, 7) == "$filter");

      if (filterIdx == -1) cfg.push("$filter=" + addFilters);
      else cfg[filterIdx] += "&" + addFilters;
    }

    return `${this.tableName}?${cfg.join("&")}`; //Components?$select=...&$filter=...
  }
}

export class TableColumn {
  publicTitle: string;
  originalTitle: string;
}

export class DynamicPageConfig {
  private static configs: ModuleConfig[] = [
    new ModuleConfig(
      "Systems",
      "systems",
      "Systema",
      [
        "$select=Systemid,Systemno,Description,Closed,Fwdate,Virtual,Ct,Mc,Descrus,Tcdate,Rct,Rmc,Compstatus,Compvercomments,Vkaapplicable,Veredapplicable,Commdosstatus,Ctconfsent,Mcconfsent,Unitno,Areano,Unitdesc",
        "$expand=Unit($select=Unitno),Ctdocno($select=Docno),Mcdocno($select=Docno),Aoccomdocno($select=Docno),Aocmcdocno($select=Docno)"
      ],
      [
        { publicTitle: "System No", originalTitle: "Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "Description" },
        { publicTitle: "System Description Rus", originalTitle: "Descrus" },
        { publicTitle: "System Closed", originalTitle: "Closed" },
        { publicTitle: "Final Walkdown Date", originalTitle: "Fwdate" },
        { publicTitle: "UnitCodeEng", originalTitle: "Unit.Unitno" },
        { publicTitle: "Virtual", originalTitle: "Virtual" },
        { publicTitle: "CT Acceptance Date", originalTitle: "Ct" },
        { publicTitle: "Mechanical Completion Acceptance Date", originalTitle: "Mc" },
        { publicTitle: "Mechanical Completion Confirmation Sent", originalTitle: "Mcconfsent" },
        { publicTitle: "Mechanical Completio Doc Number", originalTitle: "Mcdocno.Docno" },
        { publicTitle: "Technical Commission Date", originalTitle: "Tcdate" },
        { publicTitle: "Ready For CT Date", originalTitle: "Rct" },
        { publicTitle: "Post Date of Ready Mechanical Completion Letter", originalTitle: "Rmc" },
        { publicTitle: "Custody Transfer Doc Number", originalTitle: "Ctdocno.Docno" },
        { publicTitle: "System Completion Status Code", originalTitle: "Compstatus" },
        { publicTitle: "Completion Version Comments", originalTitle: "Compvercomments" },
        { publicTitle: "Vendor Key Action Applicable", originalTitle: "Vkaapplicable" },
        { publicTitle: "Venndor Engineering Redline Applicable", originalTitle: "Veredapplicable" },
        { publicTitle: "Commissioning Dossier Status", originalTitle: "Commdosstatus" },
        { publicTitle: "Custody Transfer Confirmation Sent", originalTitle: "Ctconfsent" },
        { publicTitle: "Aoccomdocnoid", originalTitle: "Aoccomdocno.Docno" },
        { publicTitle: "Aocmcdocnoid", originalTitle: "Aocmcdocno.Docno" },
        { publicTitle: "Unit Code Number", originalTitle: "Unitno" },
        { publicTitle: "Area Code Number", originalTitle: "Areano" },
        { publicTitle: "Unit Description", originalTitle: "Unitdesc" }
      ],
      [
        "components",
        "punchlists",
        // "Asbuiltlink",
        // "Checkfileslink",
        // "Checklist",
        // "Commdossierdoc",
        // "Commdossier",
        // "Commelpsp",
        // "Commswdoc",
        // "Docsad",
        // "Dossierreview",
        // "Engdoc",
        // "Form141",
        // "Fcrqvdarchive",
        // "IbArchive",
        // "IbTppArchive",
        // "Leakdoc"
      ],
      [
        "Systemid",
        "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid",
        // "Systemid"
      ],
      "Systemid"
    ),
    new ModuleConfig(
      "Punchlists",
      "punchlists",
      "Punchlist",
      [
        "$select=Plnumber,Pldescription,Pldescrus,Actiongroup,Category,Raisedby,Clearedby,Cleareddate,Commentseng,Commentsrus,Eng,Mat,Reopencomments,Status,Verifiedby,Verifieddate",
        "$expand=Discipline($select=Description),System($select=Systemno)"
      ],
      [
        { publicTitle: "Punchlist Item Number", originalTitle: "Plnumber" },
        { publicTitle: "Description Eng", originalTitle: "Pldescription" },
        { publicTitle: "Description Rus", originalTitle: "Pldescrus" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "Discipline", originalTitle: "Discipline.Description" },
        { publicTitle: "Action Group", originalTitle: "Actiongroup" },
        { publicTitle: "Category Code", originalTitle: "Category" },
        { publicTitle: "Raised By", originalTitle: "Raisedby" },
        { publicTitle: "Cleared By", originalTitle: "Clearedby" },
        { publicTitle: "Cleared Date", originalTitle: "Cleareddate" },
        { publicTitle: "Comments Eng", originalTitle: "Commentseng" },
        { publicTitle: "Commets Rus", originalTitle: "Commentsrus" },
        { publicTitle: "Engineering Required", originalTitle: "Eng" },
        { publicTitle: "Material Required", originalTitle: "Mat" },
        { publicTitle: "Re-Opened Comments", originalTitle: "Reopencomments" },
        { publicTitle: "Status", originalTitle: "Status" },
        { publicTitle: "Verified By", originalTitle: "Verifiedby" },
        { publicTitle: "Verified Date", originalTitle: "Verifieddate" }
      ]
    ),
    new ModuleConfig(
      "Units",
      "units",
      "Unit",
      [
        "$select=Unitid,Unitno,Unitname,Unitnorus,Unitnamerus,Closed,Commdate,Virtual,AreaId",
        "$filter=Closed eq 0",
        "$expand=Area($select=Name,Description)"
      ],
      [
        { publicTitle: "Unit Id", originalTitle: "Unitid" },
        { publicTitle: "Unit No", originalTitle: "Unitno" },
        { publicTitle: "Unit Name", originalTitle: "Unitname" },
        { publicTitle: "Area Id", originalTitle: "Areaid" },
        { publicTitle: "Closed", originalTitle: "Closed" },
        { publicTitle: "Virtual", originalTitle: "Virtual" },
        { publicTitle: "Unit No Rus", originalTitle: "Unitnorus" },
        { publicTitle: "Unit Name Rus", originalTitle: "Unitnamerus" },
        { publicTitle: "Unit Commissioning Date", originalTitle: "Commdate" },
        { publicTitle: "Area Name", originalTitle: "Area.Name" },
        { publicTitle: "Area Description", originalTitle: "Area.Description" }
      ]
    ),
    new ModuleConfig(
      "Components",
      "components",
      "Component",
      [
        "$select=Tagid,Parenttagid",
        "$expand=System($select=Systemno,Description,Descrus),Componentno($select=Tagno,Description,Engstatus,Pono,Remark,Servdesc;$expand=Discipline($select=Discipline1,Description,Descriptionrus)),ParentComponent($select=Tagno)"
      ],
      [
        { publicTitle: "Tag No", originalTitle: "Componentno.Tagno" },
        { publicTitle: "Tag Description", originalTitle: "Componentno.Description" },
        { publicTitle: "Tag Service Description", originalTitle: "Componentno.Servdesc" },
        { publicTitle: "PurchaseOrderNumber", originalTitle: "Componentno.Pono" },
        { publicTitle: "Engineering Status", originalTitle: "Componentno.Engstatus" },
        { publicTitle: "Discipline", originalTitle: "Componentno.Discipline.Discipline1" },
        { publicTitle: "Discipline Description", originalTitle: "Componentno.Discipline.Description" },
        { publicTitle: "Discipline Description Rus", originalTitle: "Componentno.Discipline.Descriptionrus" },
        { publicTitle: "Parent Tag No", originalTitle: "ParentComponent.Tagno" },
        { publicTitle: "Parent Tag Id", originalTitle: "Parenttagid" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Remarks", originalTitle: "Componentno.Remark" }
      ]
    ),
    new ModuleConfig(
      "Global Documents",
      "documents",
      "Documentno",
      [
        "$select=Docno,Title,Titlerus,Palastrev,Fis,Doctype,Hostatus,Keydoc,Actiondoc,Abreq,Issuestatus,Clientdocno,Disc,Pono,Pkgitem,Doctypeid,Vendor",
      ],
      [
        { publicTitle: "Document No", originalTitle: "Docno" },
        { publicTitle: "Title", originalTitle: "Title" },
        { publicTitle: "Title Rus", originalTitle: "Titlerus" },
        { publicTitle: "Palastrev", originalTitle: "Palastrev" },
        { publicTitle: "Document Type", originalTitle: "Doctype" },
        { publicTitle: "Handover Status", originalTitle: "Hostatus" },
        { publicTitle: "Key Document", originalTitle: "Keydoc" },
        { publicTitle: "Action Document", originalTitle: "Actiondoc" },
        { publicTitle: "AB Req", originalTitle: "Abreq" },
        { publicTitle: "Issue status", originalTitle: "Issuestatus" },
        { publicTitle: "Client Document No", originalTitle: "Clientdocno" },
        { publicTitle: "Discipline", originalTitle: "Disc" },
        { publicTitle: "Po No", originalTitle: "Pono" },
        { publicTitle: "PKG item", originalTitle: "Pkgitem" },
        { publicTitle: "Document Type Id", originalTitle: "Doctypeid" },
        { publicTitle: "Vendor", originalTitle: "Vendor" },

      ]
    ),
    new ModuleConfig(
      "Areas",
      "areas",
      "Area",
      [
        "$select=Id,Description,Descrus,Name,NameRus,Responsible"
      ],
      [
        { publicTitle: "Name Eng", originalTitle: "Name" },
        { publicTitle: "Name Rus", originalTitle: "Namerus" },
        { publicTitle: "Description Eng", originalTitle: "Description" },
        { publicTitle: "Description Rus", originalTitle: "Descrus" },
        { publicTitle: "Responsible", originalTitle: "Responsible" }
      ]
    ),
    new ModuleConfig(
      "As-built links",
      "asbuiltlinks",
      "Asbuiltlink",
      [
        "$select=Systemid,Drawingid,Redlconst,Redlcomm,Redlcommfn,Recievedconst,Recievedcomm,Comments,Sheetno"
      ],
      [
        { publicTitle: "System Id", originalTitle: "Systemid" },
        { publicTitle: "Drawing Id", originalTitle: "Drawingid" },
        { publicTitle: "Sheetno", originalTitle: "Sheetno" },
        { publicTitle: "Construction Redline Status Code", originalTitle: "Redlconst" },
        { publicTitle: "Construction Redline Received", originalTitle: "Recievedconst" },
        { publicTitle: "Commissioning Redline Status Code", originalTitle: "Redlcomm" },
        { publicTitle: "Commissioning Redline File Name", originalTitle: "Redlcommfn" },
        { publicTitle: "Commissioning Redline Received", originalTitle: "Recievedcomm" },
        { publicTitle: "Comments", originalTitle: "Comments" },
      ]
    ),
    new ModuleConfig(
      "Punchlist",
      "punchlists",
      "Punchlist",
      [
        "$select=Systemid,Plnumber,Raisedby,Category,Pldescription,Disciplineid,Contractor,Mat,Eng,Status,Cleareddate,Clearedby,Verifieddate,Verifiedby,Createddate,Actiongroup,Pldescrus,Addedby,Comments,Id,Managerdate,Managercomments,Managerid,Updatedate,Updateuserid,Reopencomments,Aocid,Aoccomments,Aocdate,Rejectionreason,Rejectiondate,Rejectionuserid,Applicabledocno,Commentseng,Commentsrus"
      ],
      [
        { publicTitle: "Id", originalTitle: "Id" },
        { publicTitle: "Systemid", originalTitle: "Systemid" },
        { publicTitle: "Plnumber", originalTitle: "Plnumber" },
        { publicTitle: "Raisedby", originalTitle: "Raisedby" },
        { publicTitle: "Category", originalTitle: "Category" },
        { publicTitle: "Pldescription", originalTitle: "Pldescription" },
        { publicTitle: "Disciplineid", originalTitle: "Disciplineid" },
        { publicTitle: "Contractor", originalTitle: "Contractor" },
        { publicTitle: "Mat", originalTitle: "Mat" },
        { publicTitle: "Eng", originalTitle: "Eng" },
        { publicTitle: "Status", originalTitle: "Status" },
        { publicTitle: "Cleareddate", originalTitle: "Cleareddate" },
        { publicTitle: "Clearedby", originalTitle: "Clearedby" },
        { publicTitle: "Verifieddate", originalTitle: "Verifieddate" },
        { publicTitle: "Verifiedby", originalTitle: "Verifiedby" },
        { publicTitle: "Createddate", originalTitle: "Createddate" },
        { publicTitle: "Actiongroup", originalTitle: "Actiongroup" },
        { publicTitle: "Pldescrus", originalTitle: "Pldescrus" },
        { publicTitle: "Addedby", originalTitle: "Addedby" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "Managerdate", originalTitle: "Managerdate" },
        { publicTitle: "Managercomments", originalTitle: "Managercomments" },
        { publicTitle: "Managerid", originalTitle: "Managerid" },
        { publicTitle: "Updatedate", originalTitle: "Updatedate" },
        { publicTitle: "Updateuserid", originalTitle: "Updateuserid" },
        { publicTitle: "Reopencomments", originalTitle: "Reopencomments" },
        { publicTitle: "Aocid", originalTitle: "Aocid" },
        { publicTitle: "Aoccomments", originalTitle: "Aoccomments" },
        { publicTitle: "Aocdate", originalTitle: "Aocdate" },
        { publicTitle: "Rejectionreason", originalTitle: "Rejectionreason" },
        { publicTitle: "Rejectiondate", originalTitle: "Rejectiondate" },
        { publicTitle: "Rejectionuserid", originalTitle: "Rejectionuserid" },
        { publicTitle: "Applicabledocno", originalTitle: "Applicabledocno" },
        { publicTitle: "Commentseng", originalTitle: "Commentseng" },
        { publicTitle: "Commentsrus", originalTitle: "Commentsrus" }
      ]
    ),
    new ModuleConfig(
      "Engineering documents",
      "engdocs",
      "Engdoc",
      [
        "$select=Id",
        "$expand=Docno($select=Docno,Title,Titlerus,Palastrev,Fis,Doctype,Hostatus,Keydoc,Actiondoc,Abreq,Issuestatus,Clientdocno,Disc,Pono,Pkgitem,Doctypeid;$expand=DoctypeNavigation($select=Doctype,Title,Abreqdoc,Timing,Abreq,Compdoc,Priority,Cdtype))"
      ],
      [
        { publicTitle: "Id", originalTitle: "Id" },
        { publicTitle: "Document No", originalTitle: "Docno.Docno" },
        { publicTitle: "Title Eng", originalTitle: "Docno.Title" },
        { publicTitle: "Title Rus", originalTitle: "Docno.Titlerus" },
        { publicTitle: "Revision", originalTitle: "Docno.Palastrev" },
        { publicTitle: "Final Issue Status", originalTitle: "Docno.Fis" },
        { publicTitle: "Doc type", originalTitle: "Docno.Doctype" },
        { publicTitle: "Handover Status", originalTitle: "Docno.Hostatus" },
        { publicTitle: "Key Doc", originalTitle: "Docno.Keydoc" },
        { publicTitle: "Action Doc", originalTitle: "Docno.Actiondoc" },
        { publicTitle: "AB Req", originalTitle: "Docno.Abreq" },
        { publicTitle: "Issue status", originalTitle: "Docno.Issuestatus" },
        { publicTitle: "Client Doc No", originalTitle: "Docno.Clientdocno" },
        { publicTitle: "Discipline", originalTitle: "Docno.Disc" },
        { publicTitle: "Po No", originalTitle: "Docno.Pono" },
        { publicTitle: "MOC Package", originalTitle: "Docno.Pkgitem" },
        { publicTitle: "Doc type id", originalTitle: "Docno.Doctypeid" },
        { publicTitle: "Vendor", originalTitle: "Docno.Vendor" },
        { publicTitle: "Doc type Code", originalTitle: "Docno.DoctypeNavigation.Doctype" },
        { publicTitle: "Doc type Title", originalTitle: "Docno.DoctypeNavigation.Title" },
        { publicTitle: "Doc type AB Required", originalTitle: "Docno.DoctypeNavigation.Abreqdoc" },
        { publicTitle: "Timing", originalTitle: "Docno.DoctypeNavigation.Timing" },
        { publicTitle: "Doc type AB Required Flag", originalTitle: "Docno.DoctypeNavigation.Abreq" },
        { publicTitle: "Completions Document", originalTitle: "Docno.DoctypeNavigation.Compdoc" },
        { publicTitle: "Priority", originalTitle: "Docno.DoctypeNavigation.Priority" },
        { publicTitle: "CD Type", originalTitle: "Docno.DoctypeNavigation.Cdtype" },
      ]
    ),
    new ModuleConfig(
      "Vendor Documents",
      "vendordocs", 
      "Documentno",
      [
        '$select=Id,Docno,Clientdocno,Title,Titlerus,Palastrev,Issuestatus,Pono,Pkgitem,Vendor,Fis,Disc,Hostatus',
        '$expand=Doctype1($select=Doctype,Title,Titlerus,Timing,Priority,Kd,Keydoc,Ad,Actiondoc,Abreq,Abreqdoc)'
      ],
      [
        { publicTitle: "ID", originalTitle: "Id" },
        { publicTitle: "Vendor Document Number", originalTitle: "Docno" },
        { publicTitle: "TCO Document Number", originalTitle: "Clientdocno" },
        { publicTitle: "Document Title Eng", originalTitle: "Title" },
        { publicTitle: "Document Title Rus", originalTitle: "Titlerus" },
        { publicTitle: "Palastrev", originalTitle: "Palastrev" },
        { publicTitle: "Issuestatus", originalTitle: "Issuestatus" },
        { publicTitle: "Purchase Order Number", originalTitle: "Pono" },
        { publicTitle: "Equipment Package Item", originalTitle: "Pkgitem" },
        { publicTitle: "Vendor", originalTitle: "Vendor" },
        { publicTitle: "Final Issue Status", originalTitle: "Fis" },
        { publicTitle: "Discipline", originalTitle: "Disc" },
        { publicTitle: "Vendor Doc Type Code", originalTitle: "Doctype1.Doctype" },
        { publicTitle: "Vendor Doc Type Title Eng", originalTitle: "Doctype1.Title" },
        { publicTitle: "Vendor Doc Type Title Rus", originalTitle: "Doctype1.Titlerus" },
        { publicTitle: "Timing", originalTitle: "Doctype1.Timing" },
        { publicTitle: "Priority", originalTitle: "Doctype1.Priority" },
        { publicTitle: "Key Document Flag", originalTitle: "Doctype1.Kd" },
        { publicTitle: "Is Vendor Key Doc", originalTitle: "Doctype1.Keydoc" },
        { publicTitle: "Action Document Flag", originalTitle: "Doctype1.Ad" },
        { publicTitle: "Is Vendor Action Doc", originalTitle: "Doctype1.Actiondoc" },
        { publicTitle: "As Built Required Flag", originalTitle: "Doctype1.Abreq" },
        { publicTitle: "As Built Required Text", originalTitle: "Doctype1.Abreqdoc" },
        { publicTitle: "Handover Status", originalTitle: "Hostatus" },
        
      ]
    ),
    new ModuleConfig(
      "Document Filelinks",
      "docfilelinks", 
      "Docfilelink",
      [
        "$select=Id,Docnoid,Filename,Rev,Description,BackdraftStatus,Filetype,Updateuserid,Updatedate,Transferredtotidedate",
        "$expand=Transferredtotideuser($select=Usercai,Fullname)"
      ],
      [
        { publicTitle: "Id", originalTitle: "Id" },
        { publicTitle: "Document Id", originalTitle: "Docnoid" },
        { publicTitle: "File", originalTitle: "Filename" },
        { publicTitle: "Rev", originalTitle: "Rev" },
        { publicTitle: "Description", originalTitle: "Description" },
        { publicTitle: "Update Date", originalTitle: "Updatedate" },
        { publicTitle: "Backdraft Status", originalTitle: "BackdraftStatus" },
        { publicTitle: "File Type", originalTitle: "Filetype" },
        { publicTitle: "Update user id", originalTitle: "Updateuserid" },
        { publicTitle: "Transferred to Tide Date", originalTitle: "Transferredtotidedate" },
        { publicTitle: "Transferred to Tide Cai", originalTitle: "Transferredtotideuser.Usercai" },
        { publicTitle: "Transferred to Tide Fullname", originalTitle: "Transferredtotideuser.Fullname" }
      ]
    ),
    new ModuleConfig(
      "Valve Work Shops",
      "valveworkshops",
      "Valveworkshop",
      [
        "$select=Jobno,Valveno,Serialno,Manufacturer,Model,Classification,Certdate,Nextcertdate,Docno,Filename,Transferredtotide,Booklocation",
        "$expand=Discipline($select=Discipline1,Description,Descriptionrus)"  
      ],
      [
        { publicTitle: "DZNumber", originalTitle: "Jobno" },
        { publicTitle: "Valve Number", originalTitle: "Valveno" },
        { publicTitle: "Serial Number", originalTitle: "Serialno" },
        { publicTitle: "Manufacturer", originalTitle: "Manufacturer" },
        { publicTitle: "Model", originalTitle: "Model" },
        { publicTitle: "Classification", originalTitle: "Classification" },
        { publicTitle: "Certification Date", originalTitle: "Certdate" },
        { publicTitle: "Next Certification Date", originalTitle: "Nextcertdate" },
        { publicTitle: "Discipline", originalTitle: "Discipline.Discipline1" },
        { publicTitle: "Discipline Description Eng", originalTitle: "Discipline.Description" },
        { publicTitle: "Discipline Description Rus", originalTitle: "Discipline.Descriptionrus" },
        { publicTitle: "Document Number", originalTitle: "Docno" },
        { publicTitle: "File Link", originalTitle: "Filename" },
        { publicTitle: "Transferred to Tide", originalTitle: "Transferredtotide" },
        { publicTitle: "Booklocation", originalTitle: "Booklocation" }
      ]
    ),
    new ModuleConfig(
      "Variations",
      "variations",
      "Variation",
      [
        "$select=No,Comments,Status",
        "$expand=Docno($select=Docno,Title,TitleRus;$expand=Docfilelinks($select=Filename))"  
      ],
      [
        { publicTitle: "Variation No", originalTitle: "No" },
        { publicTitle: "Variation Comments", originalTitle: "Comments" },
        { publicTitle: "Document No", originalTitle: "Docno.Docno" },
        { publicTitle: "Document Title Eng", originalTitle: "Docno.Title" },
        { publicTitle: "Document Title Rus", originalTitle: "Docno.TitleRus" },
        { publicTitle: "Variation Status", originalTitle: "Status" },
        { publicTitle: "Document Links", originalTitle: "Docno.Docfilelinks.Filename" }
      ]
    ),
    new ModuleConfig(
      "Users",
      "users",
      "User",
      [
        "$select=Usercai,Fullname,Userid",
      ],
      [
        { publicTitle: "User id", originalTitle: "Userid" },
        { publicTitle: "CAI", originalTitle: "Usercai" },
        { publicTitle: "Full Name", originalTitle: "Fullname" },
      ]
    ),
    new ModuleConfig(
      "TPP Archive",
      "tpparchive",
      "Tpparchive",
      [
        "$select=Tagno,Linenumber,Drawingno,Tcopipeowner,Receiveddate,Signoffdate,Transmitdate,Comments,Accepteddate",
        "$expand=System($select=Systemno,Description,Descrus)"  
      ],
      [
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Tag No", originalTitle: "Tagno" },
        { publicTitle: "Line Number", originalTitle: "Linenumber" },
        { publicTitle: "Drawing No", originalTitle: "Drawingno" },
        { publicTitle: "TCOPIPEOWNER", originalTitle: "Tcopipeowner" },
        { publicTitle: "Received Date", originalTitle: "Receiveddate" },
        { publicTitle: "Sign off Date", originalTitle: "Signoffdate" },
        { publicTitle: "Transmittal Date", originalTitle: "Transmitdate" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "Accepted Date", originalTitle: "Accepteddate" }
      ]
    ),
    new ModuleConfig(
      "Commissioning Dossiers",
      "commdossiers",
      "Commdossierdoc",
      [
        "$select=Comments,Systemid",
        "$expand=Docno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename)),System($select=Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus)))"
      ],
      [
        { publicTitle: "Area Name Eng", originalTitle: "System.Unit.Area.Name" },
        { publicTitle: "Area Name Rus", originalTitle: "System.Unit.Area.Namerus" },
        { publicTitle: "Area Description Eng", originalTitle: "System.Unit.Area.Description" },
        { publicTitle: "Area Description Rus", originalTitle: "System.Unit.Area.Descrus" },
        { publicTitle: "Unit Number Eng", originalTitle: "System.Unit.Unitno" },
        { publicTitle: "Unit Number Rus", originalTitle: "System.Unit.Unitnorus" },
        { publicTitle: "Unit Name Eng", originalTitle: "System.Unit.Unitname" },
        { publicTitle: "Unit Name Rus", originalTitle: "System.Unit.Unitnamerus" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Document No", originalTitle: "Docno.Docno" },
        { publicTitle: "Document Title Eng", originalTitle: "Docno.Title" },
        { publicTitle: "Document Title Rus", originalTitle: "Docno.Titlerus" },
        { publicTitle: "File Link", originalTitle: "Docno.Docfilelinks.Filename" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "System Id", originalTitle: "Systemid" },
      ]
    ),
    new ModuleConfig(
      "Commissioning Procedures",
      "commprocedures",
      "Procedure",
      [
        "$select=Statusid,Commgroupid,Execstatusid",
        "$expand=Docno($select=Id,Docno,Doctype,Title,Titlerus;$expand=Docfilelinks($select=Filename)),Proctype($select=Tcoproctype,Description)"
      ],
      [
        { publicTitle: "Document Id", originalTitle: "Docno.Id" },
        { publicTitle: "Procedure Type Code", originalTitle: "Proctype.Tcoproctype" },
        { publicTitle: "Procedure Type Description", originalTitle: "Proctype.Description" },
        { publicTitle: "Document Title Eng", originalTitle: "Docno.Title" },
        { publicTitle: "Document Title Rus", originalTitle: "Docno.Titlerus" },
        { publicTitle: "Procedure Status Code", originalTitle: "Statusid" },
        { publicTitle: "Commissioning Group Id", originalTitle: "Commgroupid" },
        { publicTitle: "Procedure Exec Status Code", originalTitle: "Execstatusid" },
        { publicTitle: "Document No", originalTitle: "Docno.Docno" },
        { publicTitle: "Document Type", originalTitle: "Docno.Doctype" },
        { publicTitle: "Document Link", originalTitle: "Docno.Docfilelinks.Filename" },
        
      ]
    ),
    new ModuleConfig(
      "FCR QVD Archive",
      "fcrqvdarchive",
      "Fcrqvdarchive",
      [
        "$select=Tagno,Checksheet,Sheetdesc,Docno,Filename,Tabno,Notreq,Systemid",
        "$expand=System($select=Systemno,Description,Descrus),Disc($select=Discipline1,Description,DescriptionRus)"
      ],
      [
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Discipline", originalTitle: "Disc.Discipline1" },
        { publicTitle: "Discipline Description Eng", originalTitle: "Disc.Description" },
        { publicTitle: "Discipline Description Rus", originalTitle: "Disc.Descriptionrus" },
        { publicTitle: "Tag No", originalTitle: "Tagno" },
        { publicTitle: "Checksheet", originalTitle: "Checksheet" },
        { publicTitle: "Sheetdesc", originalTitle: "Sheetdesc" },
        { publicTitle: "Document No", originalTitle: "Docno" },
        { publicTitle: "File Link", originalTitle: "Filename" },
        { publicTitle: "Tab No", originalTitle: "Tabno" },
        { publicTitle: "Not Required", originalTitle: "Notreq" },
        { publicTitle: "System Id", originalTitle: "Systemid" }
      ]
    ),
    new ModuleConfig(
      "FCR TPP Archive",
      "fcrtpparchive",
      "Fcrtpparchive",
      [
        "$select=Id,Tagno,Linenumber,Tcopipeowner,Receiveddate,Signoffdate,Comments,Accepteddate,Systemid",
        "$expand=System($select=Systemno,Description,Descrus)"
      ],
      [
        { publicTitle: "FCR TPP Archive Id", originalTitle: "Id" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Tag No", originalTitle: "Tagno" },
        { publicTitle: "Line Number", originalTitle: "Linenumber" },
        { publicTitle: "TCOPIPEOWNER", originalTitle: "Tcopipeowner" },
        { publicTitle: "Received Date", originalTitle: "Receiveddate" },
        { publicTitle: "Sign off Date", originalTitle: "Signoffdate" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "Accepteddate", originalTitle: "Accepteddate" },
        { publicTitle: "System Id", originalTitle: "Systemid" }
      ]
    ),
    new ModuleConfig(
      "IB Archive",
      "ibarchive",
      "IbArchive",
      [
        "$select=Clientdocno,Pfdno,Title,Booklocation,Status,Comments,Systemid",
        "$expand=Contract($select=Company),System($select=Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitname;$expand=Area($select=Name,Description)))"
      ],
      [
        { publicTitle: "Client Document No", originalTitle: "Clientdocno" },
        { publicTitle: "Pfd No", originalTitle: "Pfdno" },
        { publicTitle: "Title", originalTitle: "Title" },
        { publicTitle: "Booklocation", originalTitle: "Booklocation" },
        { publicTitle: "Status", originalTitle: "Status" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "Contractor", originalTitle: "Contract.Company" },
        { publicTitle: "Area Name", originalTitle: "System.Unit.Area.Name" },
        { publicTitle: "Area Description", originalTitle: "System.Unit.Area.Description" },
        { publicTitle: "Unit", originalTitle: "System.Unit.Unitno" },
        { publicTitle: "Unit Name", originalTitle: "System.Unit.Unitname" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Systemid", originalTitle: "Systemid" },
      ]
    ),
    new ModuleConfig(
      "IB TPP Archive",
      "ibtppArchive",
      "IbTppArchive",
      [
        "$select=Htno,Location,Contractno,Comments,Status,Systemid",
        "$expand=System($select=Systemno,Description,Descrus)"
      ],
      [
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Hydro Test Number", originalTitle: "Htno" },
        { publicTitle: "Location", originalTitle: "Location" },
        { publicTitle: "Contract No", originalTitle: "Contractno" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "Status", originalTitle: "Status" },
        { publicTitle: "System Id", originalTitle: "Systemid" },
      ]
    ),
    new ModuleConfig(
      "Passports",
      "passports",
      "PassportNew",
      [
        "$select=Equipment,Registrdate,Comments,Permittooperate",
        "$expand=Componentno($select=Tagno,Description,Servdesc,Pono),Cpdocno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename)),Docno($select=Docno;$expand=Docfilelinks($select=Filename))"
      ],
      [
        { publicTitle: "Tag No", originalTitle: "Componentno.Tagno" },
        { publicTitle: "Equipment Class", originalTitle: "Equipment" },
        { publicTitle: "Component Description", originalTitle: "Componentno.Description" },
        { publicTitle: "Component Serv Description", originalTitle: "Componentno.Servdesc" },
        { publicTitle: "Po No", originalTitle: "Componentno.Pono" },
        { publicTitle: "Cp Document No", originalTitle: "Cpdocno.Docno" },
        { publicTitle: "Cp Document Title Eng", originalTitle: "Cpdocno.Title" },
        { publicTitle: "Cp Document Title Rus", originalTitle: "Cpdocno.Titlerus" },
        { publicTitle: "Register Date", originalTitle: "Registrdate" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "Permit to operate", originalTitle: "Permittooperate" },
        { publicTitle: "Cp Document Filelink", originalTitle: "Cpdocno.Docfilelinks.Filename" },
        { publicTitle: "Document No", originalTitle: "Docno.Docno" },
        { publicTitle: "Filelinks", originalTitle: "Docno.Docfilelinks.Filename" }
      ]
    ),
    new ModuleConfig(
      "Peer Review",
      "peerreview",
      "Peer",
      [
        "$select=Itemno,Subitemno,Subteam,Activity,Docforreview,Observation,Actiontoconsider,Status,Respnote,Category",
        "$expand=Area($select=Name,Namerus,Description,Descrus)"
      ],
      [
        { publicTitle: "Area Name Eng", originalTitle: "Area.Name" },
        { publicTitle: "Area Name Rus", originalTitle: "Area.Namerus" },
        { publicTitle: "Area Description Eng", originalTitle: "Area.Description" },
        { publicTitle: "Area Description Rus", originalTitle: "Area.Descrus" },
        { publicTitle: "Item No", originalTitle: "Itemno" },
        { publicTitle: "Sub Item No", originalTitle: "Subitemno" },
        { publicTitle: "Sub Team", originalTitle: "Subteam" },
        { publicTitle: "Activity", originalTitle: "Activity" },
        { publicTitle: "Document for review", originalTitle: "Docforreview" },
        { publicTitle: "Observation", originalTitle: "Observation" },
        { publicTitle: "Action to consider", originalTitle: "Actiontoconsider" },
        { publicTitle: "Status", originalTitle: "Status" },
        { publicTitle: "Resp Note", originalTitle: "Respnote" },
        { publicTitle: "Category", originalTitle: "Category" }
      ]
    ),
    new ModuleConfig(
      "PMS Document Details",
      "pmsdocdetails",
      "Pmsdocdetail",
      [
        "$select=Tagno,Checksheetno,DocNo,EngName,RusName,FilePath,Description,Pmsdoctype",
        "$expand=System($select=Unit,Systemno,Description),Flsdoctype($select=Doctype,Name,Code)"
      ],
      [
        { publicTitle: "Unit", originalTitle: "System.Unit" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description", originalTitle: "System.Description" },
        { publicTitle: "Tag No", originalTitle: "Tagno" },
        { publicTitle: "Checksheet No", originalTitle: "Checksheetno" },
        { publicTitle: "Document No", originalTitle: "DocNo" },
        { publicTitle: "Document Name Eng", originalTitle: "EngName" },
        { publicTitle: "Document Name Rus", originalTitle: "RusName" },
        { publicTitle: "Filelink", originalTitle: "FilePath" },
        { publicTitle: "PMS Document Description", originalTitle: "Description" },
        { publicTitle: "PMS Doctype Description", originalTitle: "Pmsdoctype" },
        { publicTitle: "Fast Load Shedding Doctype", originalTitle: "Flsdoctype.Doctype" },
        { publicTitle: "Fast Load Shedding Doctype Name", originalTitle: "Flsdoctype.Name" },
        { publicTitle: "Fast Load Shedding Doctype Code", originalTitle: "Flsdoctype.Code" },
      ]
    ),
    new ModuleConfig(
      "Punchlist Post",
      "punchlistspost",
      "Punchlistspost",
      [
        "$select=Plnumber,Category,Pldescription,Pldescrus,Contractor,Status,Actiongroup,Createddate,Verifieddate,Cleareddate",
        "$expand=Unit($select=Unitno,Unitname;$expand=Area($select=Name,Description,Descrus)),Raisedbyuser($select=Usercai,Fullname),Verifiedbyuser($select=Usercai,Fullname)"
      ],
      [
        { publicTitle: "Area Name", originalTitle: "Unit.Area.Name" },
        { publicTitle: "Area Description Eng", originalTitle: "Unit.Area.Description" },
        { publicTitle: "Area Description Rus", originalTitle: "Unit.Area.Descrus" },
        { publicTitle: "Unit No", originalTitle: "Unit.Unitno" },
        { publicTitle: "Unit Name", originalTitle: "Unit.Unitname" },
        { publicTitle: "Punchlist Number", originalTitle: "Plnumber" },
        { publicTitle: "Category", originalTitle: "Category" },
        { publicTitle: "Punchlist Description Eng", originalTitle: "Pldescription" },
        { publicTitle: "Punchlist Description Rus", originalTitle: "Pldescrus" },
        { publicTitle: "Contractor", originalTitle: "Contractor" },
        { publicTitle: "Status", originalTitle: "Status" },
        { publicTitle: "Action Group", originalTitle: "Actiongroup" },
        { publicTitle: "Created Date", originalTitle: "Createddate" },
        { publicTitle: "Raised by CAI", originalTitle: "Raisedbyuser.Usercai" },
        { publicTitle: "Raised by", originalTitle: "Raisedbyuser.Fullname" },
        { publicTitle: "Verified Date", originalTitle: "Verifieddate" },
        { publicTitle: "Verified by CAI", originalTitle: "Verifiedbyuser.Usercai" },
        { publicTitle: "Verified by", originalTitle: "Verifiedbyuser.Fullname" },
        { publicTitle: "Cleared Date", originalTitle: "Cleareddate" }
      ]
    ),
    new ModuleConfig(
      "QVD Archive",
      "qvdarchive",
      "Qvdarchive",
      [
        "$select=Tagno,Checksheet,Sheetdesc,Docno,Filename,Tabno,Notreq,Comments,Systemid",
        "$expand=System($select=Systemno,Description,Descrus),Discipline($select=Discipline1,Description,Descriptionrus),VerifiedUser($select=Usercai,Fullname)"
      ],
      [
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Discipline", originalTitle: "Discipline.Discipline1" },
        { publicTitle: "Discipline Description Eng", originalTitle: "Discipline.Description" },
        { publicTitle: "Discipline Description Rus", originalTitle: "Discipline.Descriptionrus" },
        { publicTitle: "Tag No", originalTitle: "Tagno" },
        { publicTitle: "Checksheet", originalTitle: "Checksheet" },
        { publicTitle: "Sheetdesc", originalTitle: "Sheetdesc" },
        { publicTitle: "Document No", originalTitle: "Docno" },
        { publicTitle: "File Link", originalTitle: "Filename" },
        { publicTitle: "Tab No", originalTitle: "Tabno" },
        { publicTitle: "Not Required Code", originalTitle: "Notreq" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "System Id", originalTitle: "Systemid" },
        { publicTitle: "Usercai", originalTitle: "VerifiedUser.Usercai" },
        { publicTitle: "Fullname", originalTitle: "VerifiedUser.Fullname" }
      ]
    ),
    new ModuleConfig(
      "RA Certifications",
      "racerts",
      "Racert",
      [
        "$select=Certnum,Description,Pono,Supplier,Obtained,Expiry,Elfilestatus",
        "$expand=Org($select=Organization,Description),Docno($select=Docno;$expand=Docfilelinks($select=Filename))"
      ],
      [
        { publicTitle: "Certificate Type", originalTitle: "Org.Organization" },
        { publicTitle: "Certificate Description", originalTitle: "Org.Description" },
        { publicTitle: "Certification Number", originalTitle: "Certnum" },
        { publicTitle: "Certification Description", originalTitle: "Description" },
        { publicTitle: "Purchase Order No", originalTitle: "Pono" },
        { publicTitle: "Supplier", originalTitle: "Supplier" },
        { publicTitle: "Obtained Date", originalTitle: "Obtained" },
        { publicTitle: "Expiration Date", originalTitle: "Expiry" },
        { publicTitle: "Electronic File Status", originalTitle: "Elfilestatus" },
        { publicTitle: "Document No", originalTitle: "Docno.Docno" },
        { publicTitle: "Document File", originalTitle: "Docno.Docfilelinks.Filename" }
      ]
    ),
    new ModuleConfig(
      "RA Certificates by System",
      "racertsbysystem",
      "Racertsystem",
      [
        "$expand=System($select=Areano,Unitno,Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus))),Cert($select=Certnum,Description,Pono,Supplier,Obtained,Expiry,Elfilestatus;$expand=Docno($select=Docno;$expand=Docfilelinks($select=Filename)),Org($select=Organization,Description))"
      ],
      [
        { publicTitle: "Area No", originalTitle: "System.Areano" },
        { publicTitle: "Unit No", originalTitle: "System.Unitno" },
        { publicTitle: "Area Code Eng", originalTitle: "System.Unit.Area.Name" },
        { publicTitle: "Area Code Rus", originalTitle: "System.Unit.Area.Namerus" },
        { publicTitle: "Area Description Eng", originalTitle: "System.Unit.Area.Description" },
        { publicTitle: "Area Description Rus", originalTitle: "System.Unit.Area.Descrus" },
        { publicTitle: "Unit Code Eng", originalTitle: "System.Unit.Unitno" },
        { publicTitle: "Unit Code Rus", originalTitle: "System.Unit.Unitnorus" },
        { publicTitle: "Unit Name Eng", originalTitle: "System.Unit.Unitname" },
        { publicTitle: "Unit Name Rus", originalTitle: "System.Unit.Unitnamerus" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "Cert Type", originalTitle: "Cert.Org.Organization" },
        { publicTitle: "Cert Description", originalTitle: "Cert.Org.Description" },
        { publicTitle: "Certification Number", originalTitle: "Cert.Certnum" },
        { publicTitle: "Certification Description", originalTitle: "Cert.Description" },
        { publicTitle: "Purchase Order No", originalTitle: "Cert.Pono" },
        { publicTitle: "Supplier", originalTitle: "Cert.Supplier" },
        { publicTitle: "Obtained Date", originalTitle: "Cert.Obtained" },
        { publicTitle: "Expiration Date", originalTitle: "Cert.Expiry" },
        { publicTitle: "Electronic File Status", originalTitle: "Cert.Elfilestatus" },
        { publicTitle: "Document No", originalTitle: "Cert.Docno.Docno" },
        { publicTitle: "Document File", originalTitle: "Cert.Docno.Docfilelinks.Filename" },
      ]
    ),
    new ModuleConfig(
      "RA Citations",
      "racitations",
      "Racitation",
      [
        "$select=Tcno,Type,Agency,Itemno,Inspector,Issuedate,Coc,Comments,Raperson,Department,Clearpr,Contact,Statuspen,Letterdate,Status,So,Areamanager",
        "$expand=Area($select=Name,Description),Racitationfile($select=Citationfile,Statusfile)"
      ],
      [
        { publicTitle: "Technical Commission", originalTitle: "Tcno" },
        { publicTitle: "Area Name", originalTitle: "Area.Name" },
        { publicTitle: "Area Description", originalTitle: "Area.Description" },
        { publicTitle: "Type", originalTitle: "Type" },
        { publicTitle: "Agency", originalTitle: "Agency" },
        { publicTitle: "Item Number", originalTitle: "Itemno" },
        { publicTitle: "Inspector", originalTitle: "Inspector" },
        { publicTitle: "Issue Date", originalTitle: "Issuedate" },
        { publicTitle: "COC", originalTitle: "Coc" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "Raperson", originalTitle: "Raperson" },
        { publicTitle: "Department", originalTitle: "Department" },
        { publicTitle: "Clear Pr", originalTitle: "Clearpr" },
        { publicTitle: "Contact", originalTitle: "Contact" },
        { publicTitle: "Statuspen", originalTitle: "Statuspen" },
        { publicTitle: "Letter Date", originalTitle: "Letterdate" },
        { publicTitle: "Status", originalTitle: "Status" },
        { publicTitle: "So", originalTitle: "So" },
        { publicTitle: "Areamanager", originalTitle: "Areamanager" },
        { publicTitle: "Citation Filelink", originalTitle: "Racitationfile.Citationfile" },
        { publicTitle: "Status Filelink", originalTitle: "Racitationfile.Statusfile" }        
      ]
    ),
    new ModuleConfig(
      "Scope Drawings",
      "scopedrwgs",
      "Scopedrwgrev",
      [
        "$select=Apddate,Filename,Id",
        "$expand=Scopedrwgsheet($expand=Docno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename))),Apduser($select=Usercai,Fullname)"
      ],
      [
        { publicTitle: "Document No", originalTitle: "Scopedrwgsheet.Docno.Docno" },
        { publicTitle: "Title Eng", originalTitle: "Scopedrwgsheet.Docno.Title" },
        { publicTitle: "Title Rus", originalTitle: "Scopedrwgsheet.Docno.Titlerus" },
        { publicTitle: "Approved Date", originalTitle: "Apddate" },
        { publicTitle: "Approved User CAI", originalTitle: "Apduser.Usercai" },
        { publicTitle: "Approved User Fullname", originalTitle: "Apduser.Fullname" },
        { publicTitle: "Scope Drawing Link", originalTitle: "Filename" },
        { publicTitle: "Drawing link", originalTitle: "Scopedrwgsheet.Docno.Docfilelinks.Filename" },
        { publicTitle: "Scope Drawing Rev Id", originalTitle: "Id" } 
      ]
    ),
    new ModuleConfig(
      "Scope Drawings by System",
      "scopedrwgbysystem",
      "Scopedrwgrevsyslink",
      [
        "$expand=System($select=Systemno,Description,Descrus;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus))),Scopedrwgrev($select=Apddate,Filename,Id;$expand=Scopedrwgsheet($expand=Docno($select=Docno,Title,Titlerus;$expand=Docfilelinks($select=Filename))),Apduser($select=Usercai,Fullname))"
      ],
      [
        { publicTitle: "Area Code Eng", originalTitle: "System.Unit.Area.Name" },
        { publicTitle: "Area Code Rus", originalTitle: "System.Unit.Area.Namerus" },
        { publicTitle: "Area Description Eng", originalTitle: "System.Unit.Area.Description" },
        { publicTitle: "Area Description Rus", originalTitle: "System.Unit.Area.Descrus" },
        { publicTitle: "Unit Code Eng", originalTitle: "System.Unit.Unitno" },
        { publicTitle: "Unit Code Rus", originalTitle: "System.Unit.Unitnorus" },
        { publicTitle: "Unit Name Eng", originalTitle: "System.Unit.Unitname" },
        { publicTitle: "Unit Name Rus", originalTitle: "System.Unit.Unitnamerus" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },

        { publicTitle: "Document No", originalTitle: "Scopedrwgrev.Scopedrwgsheet.Docno.Docno" },
        { publicTitle: "Title Eng", originalTitle: "Scopedrwgrev.Scopedrwgsheet.Docno.Title" },
        { publicTitle: "Title Rus", originalTitle: "Scopedrwgrev.Scopedrwgsheet.Docno.Titlerus" },
        { publicTitle: "Approved Date", originalTitle: "Scopedrwgrev.Apddate" },
        { publicTitle: "Approved User CAI", originalTitle: "Scopedrwgrev.Apduser.Usercai" },
        { publicTitle: "Approved User Fullname", originalTitle: "Scopedrwgrev.Apduser.Fullname" },
        { publicTitle: "Scope Drawing Link", originalTitle: "Scopedrwgrev.Filename" },
        { publicTitle: "Drawing link", originalTitle: "Scopedrwgrev.Scopedrwgsheet.Docno.Docfilelinks.Filename" },
        { publicTitle: "Scope Drawing Rev Id", originalTitle: "Scopedrwgrev.Id" } 
      ]
    ),
    new ModuleConfig(
      "System Readiness Report",
      "systemreadinessreport",
      "Completionstatus",
      [
        "$select=Compnumbers,Qvddone,Qvdtotal,Tppdone,Tpptotal,Loopsdone,Loopstotal,Apldone,Apltotal,Bpldone,Bpltotal,Ereddone,Eredtotal,"+
        "Vendorkadone,Vendorkatotal,Vreddone,Vredtotal,Sd,Commereddone,Commvreddone,Commchecklistsdone,Commcheckliststotal,Proccountdone,"+
        "Proccount,VreddoneFpr,VredtotalFpr,EreddoneFpr,EredtotalFpr,Apldonecm,Apltotalcm,Bpldonecm,Bpltotalcm,Commcheckloaded,Radone,Ratotal,Id",
        "$expand=System($select=Systemno,Description,Descrus,Systemid;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus)))"
      ],
      [
        { publicTitle: "Area Code Eng", originalTitle: "System.Unit.Area.Name" },
        { publicTitle: "Area Code Rus", originalTitle: "System.Unit.Area.Namerus" },
        { publicTitle: "Area Description Eng", originalTitle: "System.Unit.Area.Description" },
        { publicTitle: "Area Description Rus", originalTitle: "System.Unit.Area.Descrus" },
        { publicTitle: "Unit Code Eng", originalTitle: "System.Unit.Unitno" },
        { publicTitle: "Unit Code Rus", originalTitle: "System.Unit.Unitnorus" },
        { publicTitle: "Unit Name Eng", originalTitle: "System.Unit.Unitname" },
        { publicTitle: "Unit Name Rus", originalTitle: "System.Unit.Unitnamerus" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },

        { publicTitle: "Components", originalTitle: "Compnumbers" },
        { publicTitle: "QVD Complete", originalTitle: "Qvddone" },
        { publicTitle: "QVD Total", originalTitle: "Qvdtotal" },
        { publicTitle: "TPP Complete", originalTitle: "Tppdone" },
        { publicTitle: "TPP Total", originalTitle: "Tpptotal" },
        { publicTitle: "Loops Complete", originalTitle: "Loopsdone" },
        { publicTitle: "Loops Total", originalTitle: "Loopstotal" },
        { publicTitle: "A_Construction PL Complete", originalTitle: "Apldone" },
        { publicTitle: "A_Construction PL Total", originalTitle: "Apltotal" },
        { publicTitle: "B_Construction PL Complete", originalTitle: "Bpldone" },
        { publicTitle: "B_Construction PL Total", originalTitle: "Bpltotal" },
        { publicTitle: "Engineering Redline Complete", originalTitle: "Ereddone" },
        { publicTitle: "Engineering Redline Total", originalTitle: "Eredtotal" },
        { publicTitle: "Vendor Key Action Documents Complete", originalTitle: "Vendorkadone" },
        { publicTitle: "Vendor Key Action Documents Total", originalTitle: "Vendorkatotal" },
        { publicTitle: "Vendor Redline Complete", originalTitle: "Vreddone" },
        { publicTitle: "Vendor Redline Total", originalTitle: "Vredtotal" },
        { publicTitle: "Scope Drawing Quantity", originalTitle: "Sd" },
        { publicTitle: "Commissioning Engineering Redline Complete", originalTitle: "Commereddone" },
        { publicTitle: "Commissioning Vendor Redline Complete", originalTitle: "Commvreddone" },
        { publicTitle: "Commissioning Checklists Complete", originalTitle: "Commchecklistsdone" },
        { publicTitle: "Commissioning Checklists Total", originalTitle: "Commcheckliststotal" },
        { publicTitle: "Procedures Complete", originalTitle: "Proccountdone" },
        { publicTitle: "Procedures Total", originalTitle: "Proccount" },
        { publicTitle: "Vendor Redline Complete_FPR", originalTitle: "VreddoneFpr" },
        { publicTitle: "Vendor Redline Total_FPR", originalTitle: "VredtotalFpr" },
        { publicTitle: "Engineering Redline Complete_FPR", originalTitle: "EreddoneFpr" },
        { publicTitle: "Engineering Redline Total_FPR", originalTitle: "EredtotalFpr" },
        { publicTitle: "A_Commissioning Punchlist Complete", originalTitle: "Apldonecm" },
        { publicTitle: "A_Commissioning Punchlist Total", originalTitle: "Apltotalcm" },
        { publicTitle: "B_Commissioning Punchlist Complete", originalTitle: "Bpldonecm" },
        { publicTitle: "B_Commissioning Punchlist Total", originalTitle: "Bpltotalcm" },
        { publicTitle: "Commissioning Checklists Loaded", originalTitle: "Commcheckloaded" },
        { publicTitle: "Regulatory Affairs Complete", originalTitle: "Radone" },
        { publicTitle: "Regulatory Affairs Total", originalTitle: "Ratotal" },
        { publicTitle: "System Id", originalTitle: "System.Systemid" },
      ]
    ),
    new ModuleConfig(
      "System Priority",
      "systempriority",
      "Areareportlink",
      [
        "$select=Priorityno,Subpriorityno",
        "$expand=Areareport($select=Name,Namerus,Description,Descrus),System($select=Systemno,Description,Descrus,Systemid)"
      ],
      [
        { publicTitle: "System Priority", originalTitle: "Priorityno" },
        { publicTitle: "System Sub Priority", originalTitle: "Subpriorityno" },
        { publicTitle: "Area Report Name Eng", originalTitle: "Areareport.Name" },
        { publicTitle: "Area Report Name Rus", originalTitle: "Areareport.Namerus" },
        { publicTitle: "Area Report Description Eng", originalTitle: "Areareport.Description" },
        { publicTitle: "Area Report Description Rus", originalTitle: "Areareport.Descrus" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },
        { publicTitle: "System Id", originalTitle: "System.Systemid" }
      ]
    ),
    new ModuleConfig(
      "System Dossier Reviews",
      "systemdossierreview",
      "Dossierreview",
      [
        "$select=Status,Receiveddate,Reviewdate,Accepteddate,Transmittaldate,Location,Comments,Systemid",
        "$expand=System($select=Systemno,Description,Descrus,Systemid;$expand=Unit($select=Unitno,Unitnorus,Unitname,Unitnamerus;$expand=Area($select=Name,Namerus,Description,Descrus))),Contractoruser($select=Usercai,Fullname),Reviewuser($select=Fullname,Usercai),Pfduser($select=Usercai,Fullname)"
      ],
      [
        { publicTitle: "Area Code Eng", originalTitle: "System.Unit.Area.Name" },
        { publicTitle: "Area Code Rus", originalTitle: "System.Unit.Area.Namerus" },
        { publicTitle: "Area Description Eng", originalTitle: "System.Unit.Area.Description" },
        { publicTitle: "Area Description Rus", originalTitle: "System.Unit.Area.Descrus" },
        { publicTitle: "Unit Code Eng", originalTitle: "System.Unit.Unitno" },
        { publicTitle: "Unit Code Rus", originalTitle: "System.Unit.Unitnorus" },
        { publicTitle: "Unit Name Eng", originalTitle: "System.Unit.Unitname" },
        { publicTitle: "Unit Name Rus", originalTitle: "System.Unit.Unitnamerus" },
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },

        { publicTitle: "Dossier Review Status Code", originalTitle: "Status" },
        { publicTitle: "Received Date", originalTitle: "Receiveddate" },
        { publicTitle: "Reviewd Date", originalTitle: "Reviewdate" },
        { publicTitle: "Accepted Date", originalTitle: "Accepteddate" },
        { publicTitle: "Transmittal Date", originalTitle: "Transmittaldate" },

        { publicTitle: "Contractor User CAI", originalTitle: "Contractoruser.Usercai" },
        { publicTitle: "Contractor User Fullname", originalTitle: "Contractoruser.Fullname" },
        { publicTitle: "Review User CAI", originalTitle: "Reviewuser.Usercai" },
        { publicTitle: "Review User Fullname", originalTitle: "Reviewuser.Fullname" },
        { publicTitle: "PFD User CAI", originalTitle: "Pfduser.Usercai" },
        { publicTitle: "PFD User Fullname", originalTitle: "Pfduser.Fullname" },
        { publicTitle: "Location", originalTitle: "Location" },
        { publicTitle: "Comments", originalTitle: "Comments" },
        { publicTitle: "System Id", originalTitle: "Systemid" },
      ]
    ),
    new ModuleConfig(
      "SOS Items System",
      "sositemssystem",
      "SosSystem",
      [
        "$select=Sositemid,Comments,Commentsrus",
        "$expand=System($select=Systemno,Description,Descrus),Sositem($select=Sositemeng,Sositemrus,Description,Descrus;$expand=SosPeople($expand=User($select=Usercai,Fullname))),Signuser($select=Usercai,Fullname)"
      ],
      [
        { publicTitle: "System No", originalTitle: "System.Systemno" },
        { publicTitle: "Sos Item Id", originalTitle: "Sositemid" },
        { publicTitle: "Sos Item Eng", originalTitle: "Sositem.Sositemeng" },
        { publicTitle: "Sos Item Rus", originalTitle: "Sositem.Sositemrus" },
        { publicTitle: "Sos Item Description Eng", originalTitle: "Sositem.Description" },
        { publicTitle: "Sos Item Description Rus", originalTitle: "Sositem.Descrus" },
        { publicTitle: "User CAI", originalTitle: "Sositem.SosPeople.User.Usercai" },
        { publicTitle: "User Fullname", originalTitle: "Sositem.SosPeople.User.Fullname" },
        { publicTitle: "System Description Eng", originalTitle: "System.Description" },
        { publicTitle: "System Description Rus", originalTitle: "System.Descrus" },

        { publicTitle: "SOS Comments Eng", originalTitle: "Comments" },
        { publicTitle: "SOS Comments Rus", originalTitle: "Commentsrus" },
        { publicTitle: "SOS Sign CAI", originalTitle: "Signuser.Usercai" },
        { publicTitle: "SOS Sign Fullname", originalTitle: "Siguser.Fullname" },
      ]
    ),
    // {
    //   publishName: "Checkfileslink",
    //   routeName: "checkfileslinks",
    //   tableName: "Checkfileslink",
    //   config: ["$select=Id,Checklistid,Tagid,Systemid,Filename"],
    //   tableColumns: [
    //     { publicTitle: "Id", originalTitle: "Id" },
    //     { publicTitle: "Checklistid", originalTitle: "Checklistid" },
    //     { publicTitle: "Tagid", originalTitle: "Tagid" },
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Filename", originalTitle: "Filename" }
    //   ]
    // },
    // {
    //   publishName: "Checklists",
    //   routeName: "checklists",
    //   tableName: "Checklist",
    //   config: [
    //     "$select=Systemid,Tagno,Checksheet,Datecompleted,Itemstatus,Nameinsp,Discipline,Engstatus,Createddate,Createdweek,Asheetcomplete,Servdesc,Sheetdesc,Filename,TppFlag,Starsstatus,Updateuserid,Updatedate,Comments,Remediation,Id"
    //   ],
    //   tableColumns: [
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Tagno", originalTitle: "Tagno" },
    //     { publicTitle: "Checksheet", originalTitle: "Checksheet" },
    //     { publicTitle: "Datecompleted", originalTitle: "Datecompleted" },
    //     { publicTitle: "Itemstatus", originalTitle: "Itemstatus" },
    //     { publicTitle: "Nameinsp", originalTitle: "Nameinsp" },
    //     { publicTitle: "Discipline", originalTitle: "Discipline" },
    //     { publicTitle: "Engstatus", originalTitle: "Engstatus" },
    //     { publicTitle: "Createddate", originalTitle: "Createddate" },
    //     { publicTitle: "Createdweek", originalTitle: "Createdweek" },
    //     { publicTitle: "Asheetcomplete", originalTitle: "Asheetcomplete" },
    //     { publicTitle: "Servdesc", originalTitle: "Servdesc" },
    //     { publicTitle: "Sheetdesc", originalTitle: "Sheetdesc" },
    //     { publicTitle: "Filename", originalTitle: "Filename" },
    //     { publicTitle: "TppFlag", originalTitle: "ITppFlagd" },
    //     { publicTitle: "Starsstatus", originalTitle: "Starsstatus" },
    //     { publicTitle: "Updateuserid", originalTitle: "Updateuserid" },
    //     { publicTitle: "Updatedate", originalTitle: "Updatedate" },
    //     { publicTitle: "Comments", originalTitle: "Comments" },
    //     { publicTitle: "Remediation", originalTitle: "Remediation" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   publishName: "Commissioning Dossier Doc",
    //   routeName: "commdossierdoc",
    //   tableName: "Commdossierdoc",
    //   config: ["$select=Docnoid,Systemid,Commgroupid,Comments,Id"],
    //   tableColumns: [
    //     { publicTitle: "Docnoid", originalTitle: "Docnoid" },
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Commgroupid", originalTitle: "Commgroupid" },
    //     { publicTitle: "Comments", originalTitle: "Comments" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   tableName: "Commelpsp",
    //   config: ["$select=Systemid,Docnoid,Tagno,Id"],
    //   tableColumns: [
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Docnoid", originalTitle: "Docnoid" },
    //     { publicTitle: "Tagno", originalTitle: "Tagno" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   tableName: "Commswdoc",
    //   config: ["$select=Systemid,Docnoid,Tagno,Id"],
    //   tableColumns: [
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Docnoid", originalTitle: "Docnoid" },
    //     { publicTitle: "Tagno", originalTitle: "Tagno" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   tableName: "Completionstatus",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Componentdocnolink",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Componentno",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Discipline",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Docsad",
    //   config: [
    //     "$select=Docno,Systemid,Direction,Id,Updatedate,Updateuserid,Remark,Abreq,Docnoid,Unitid,Areaid,Updflag"
    //   ],
    //   tableColumns: [
    //     { publicTitle: "Docno", originalTitle: "Docno" },
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Direction", originalTitle: "Direction" },
    //     { publicTitle: "Id", originalTitle: "Id" },
    //     { publicTitle: "Updatedate", originalTitle: "Updatedate" },
    //     { publicTitle: "Updateuserid", originalTitle: "Updateuserid" },
    //     { publicTitle: "Remark", originalTitle: "Remark" },
    //     { publicTitle: "Abreq", originalTitle: "Abreq" },
    //     { publicTitle: "Docnoid", originalTitle: "Docnoid" },
    //     { publicTitle: "Unitid", originalTitle: "Unitid" },
    //     { publicTitle: "Areaid", originalTitle: "Areaid" },
    //     { publicTitle: "Updflag", originalTitle: "Updflag" }
    //   ]
    // },
    // {
    //   tableName: "Dossierreview",
    //   config: [
    //     "$select=Systemid,Reviewdate,Contractorid,Pfduserid,Status,Accepteddate,Transmittaldate,Comments,Updatedate,Updateuserid,Receiveddate,Closed,Reviewuserid,Location,Id"
    //   ],
    //   tableColumns: [
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Reviewdate", originalTitle: "Reviewdate" },
    //     { publicTitle: "Contractorid", originalTitle: "Contractorid" },
    //     { publicTitle: "Pfduserid", originalTitle: "Pfduserid" },
    //     { publicTitle: "Status", originalTitle: "Status" },
    //     { publicTitle: "Accepteddate", originalTitle: "Accepteddate" },
    //     { publicTitle: "Transmittaldate", originalTitle: "Transmittaldate" },
    //     { publicTitle: "Comments", originalTitle: "Comments" },
    //     { publicTitle: "Updatedate", originalTitle: "Updatedate" },
    //     { publicTitle: "Updateuserid", originalTitle: "Updateuserid" },
    //     { publicTitle: "Receiveddate", originalTitle: "Receiveddate" },
    //     { publicTitle: "Closed", originalTitle: "Closed" },
    //     { publicTitle: "Reviewuserid", originalTitle: "Reviewuserid" },
    //     { publicTitle: "Location", originalTitle: "Location" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   tableName: "Fcrtpparchivelink",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Flsdoctype",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   publishName: "Form141",
    //   routeName: "Form141",
    //   tableName: "Form141",
    //   config: [
    //     "$select=Systemid,Itemid,Reqbconst,Reqbstartup,Reqastartup,Tcodepperson,Tcodeppersonrus,Personname,Signdate,Updatedate,Updateuserid,Newitemname,Newitemnamerus,Signuserid,Tcopositionid,Doauserid,Id"
    //   ],
    //   tableColumns: [
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Itemid", originalTitle: "Itemid" },
    //     { publicTitle: "Reqbconst", originalTitle: "Reqbconst" },
    //     { publicTitle: "Reqbstartup", originalTitle: "Reqbstartup" },
    //     { publicTitle: "Reqastartup", originalTitle: "Reqastartup" },
    //     { publicTitle: "Tcodepperson", originalTitle: "Tcodepperson" },
    //     { publicTitle: "Tcodeppersonrus", originalTitle: "Tcodeppersonrus" },
    //     { publicTitle: "Personname", originalTitle: "Personname" },
    //     { publicTitle: "Signdate", originalTitle: "Signdate" },
    //     { publicTitle: "Updatedate", originalTitle: "Updatedate" },
    //     { publicTitle: "Updateuserid", originalTitle: "Updateuserid" },
    //     { publicTitle: "Newitemname", originalTitle: "Newitemname" },
    //     { publicTitle: "Newitemnamerus", originalTitle: "Newitemnamerus" },
    //     { publicTitle: "Signuserid", originalTitle: "Signuserid" },
    //     { publicTitle: "Tcopositionid", originalTitle: "Tcopositionid" },
    //     { publicTitle: "Doauserid", originalTitle: "Doauserid" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   publishName: "Leakdoc",
    //   routeName: "Leakdoc",
    //   tableName: "Leakdoc",
    //   config: ["$select=Systemid,Docnoid,Id"],
    //   tableColumns: [
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Docnoid", originalTitle: "Docnoid" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   tableName: "Peerdocnolink",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Pfdcontractor",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Phase5docnolink",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Pmssystem",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   publishName: "Procdoc",
    //   routeName: "Procdoc",
    //   tableName: "Procdoc",
    //   config: ["$select=Systemid,Docnoid,Id"],
    //   tableColumns: [
    //     { publicTitle: "Systemid", originalTitle: "Systemid" },
    //     { publicTitle: "Docnoid", originalTitle: "Docnoid" },
    //     { publicTitle: "Id", originalTitle: "Id" }
    //   ]
    // },
    // {
    //   publishName: "Proctype",
    //   routeName: "Proctype",
    //   tableName: "Proctype",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Racitationfile",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Reportdoc",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Scopedrwgrev",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Scopedrwgrevsyslink",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Shelldoc",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Sositem",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "SosPeople",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "SosSystem",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Srcdoc",
    //   config: [],
    //   tableColumns: []
    // },
    // {
    //   tableName: "Tpparchivelink",
    //   config: [],
    //   tableColumns: []
    // }
  ];

  public static FindModule(moduleRoute: string): ModuleConfig {
    const index = this.configs.findIndex(c => c.routeName.toLowerCase() == moduleRoute.toLowerCase());
    return index > -1 ? this.configs[index] : null;
  }
}