import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { DynamicPageConfig, ModuleConfig } from "./dynamic-page.config";
import { DynamicService } from "../../services/dynamic.service";
import { PageEvent } from "@angular/material/paginator";
import { trigger, transition, useAnimation } from "@angular/animations";

import * as XLSX from "xlsx";
import {
  FadeInAnimation,
  FadeOutAnimation
} from "src/app/shared/animations/dynamic-module-animation";
import { MatTableDataSource } from "@angular/material/table";
import { FormControl } from "@angular/forms";

import f from "odata-filter-builder";

@Component({
  selector: "dynamic-page",
  templateUrl: "./dynamic-page.component.html",
  styleUrls: ["./dynamic-page.component.scss"],
  animations: [
    trigger("fadeInOut", [
      transition(":enter", useAnimation(FadeInAnimation)),
      transition(":leave", useAnimation(FadeOutAnimation))
    ])
  ]
})
export class DynamicPageComponent implements OnInit, OnDestroy {
  module: ModuleConfig;
  focusedRow: number = -1;
  dataSource = new MatTableDataSource();

  @ViewChild("mainTable", { static: false }) table: ElementRef;

  totalItems: number = 100;
  isPending: boolean;
  currentPageIndex: number = 0;
  currentPageSize: number = 100;
  currentModule: string;

  filters = {};
  filterSubs = {};
  filterValues = {};
  filtersCreated = true;
  timeout = null;

  currentSortColumn: string;
  currentSortOrder: string = "asc";

  constructor(private route: ActivatedRoute, private service: DynamicService) {}

  ngOnInit() {
    this.routeSub = this.route.paramMap.subscribe(param => {
      const moduleName = param.get("module");
      this.module = DynamicPageConfig.FindModule(moduleName);
      if (!this.module) return;

      this.currentSortColumn = this.module.tableColumns[0].originalTitle;
      this.createFilters();

      if (this.module) {
        this.service.setCurrentModule(this.module);
        this.currentPageIndex = 0;
        this.renderTable();
      }
    });

    // this.dynamicCallSub = this.service.dynamicCall.subscribe((functionName: string) => {
    //   switch (functionName) {
    //     case "execute": {
    //       break;
    //     }
    //   }
    // });
  }

  get originalColumnTitles(): string[] {
    return this.module.tableColumns.map(e => e.originalTitle);
  }

  get filterTitles(): string[] {
    return this.module.tableColumns.map(e => "f_" + e.originalTitle);
  }

  private createFilters() {
    for (const column of this.module.tableColumns) {
      this.filters[column.originalTitle] = new FormControl("");

      this.filterSubs[column.originalTitle] = (this.filters[
        column.originalTitle
      ] as FormControl).valueChanges.subscribe(value => {
        this.filterValues[column.originalTitle] = value;
        if (value == "") delete this.filterValues[column.originalTitle];
      });
    }
    this.filtersCreated = true;
  }

  generateFilterString() {
    let odataFilter = f();
    for (const key in this.filterValues) {
      const filterValue = this.filterValues[key].toLowerCase();

      odataFilter.contains(
        x => x.toLower(`cast(${key.replace(/\./g, "/")}, 'Edm.String')`),
        filterValue
      );
    }
    return odataFilter.toString();
  }

  pageChange(event: PageEvent) {
    this.currentPageIndex = event.pageIndex;
    this.currentPageSize = event.pageSize;

    if (this.module) this.renderTable();
  }

  private renderTable() {
    this.isPending = true;

    let moduleName = this.module.tableName;

    // if (this.currentModule != moduleName)
    //   this.countSub = this.service
    //     .Count(
    //       DynamicPageConfig.GetQueryString(
    //         moduleName,
    //         this.generateFilterString()
    //       )
    //     )
    //     .subscribe(count => (this.totalItems = count));

    const queryString =
      this.module.GetQueryString(this.generateFilterString()) +
      `&$top=${this.currentPageSize}` +
      `&$skip=${this.currentPageIndex * this.currentPageSize}` +
      `&$orderby=${this.currentSortColumn.replace(/\./g, "/")} ${
        this.currentSortOrder
      }`;

    this.moduleSub = this.service.Entries(queryString).subscribe(
      response => {
        this.currentModule = moduleName;

        for (let i = 0; i < response.length; i++) {
          response[i]["Identifier"] =
            i + 1 + this.currentPageIndex * this.currentPageSize;
        }
        this.dataSource.data = response;
        console.log(response);
      },
      err => {
        this.isPending = false;
        console.error(err);
      },
      () => {
        setTimeout(() => (this.isPending = false), 300);
      }
    );
  }

  ExportToExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(
      this.table.nativeElement
    );
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    /* save to file */
    let today = new Date().toLocaleDateString();
    let filename = `${this.module.publishName} - ${today}.xlsx`;
    XLSX.writeFile(wb, filename);
  }

  routeSub: Subscription;
  countSub: Subscription;
  moduleSub: Subscription;
  dynamicCallSub: Subscription;
  ngOnDestroy() {
    if (this.routeSub) this.routeSub.unsubscribe();
    if (this.moduleSub) this.moduleSub.unsubscribe();
    if (this.dynamicCallSub) this.dynamicCallSub.unsubscribe();

    for (let col in this.filterSubs) {
      this.filterSubs[col].unsubscribe();
    }
  }

  IsFocusedRow(e: any): boolean {
    if (!e) return false;
    return e == this.focusedRow;
  }

  onKeyUp() {
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this.renderTable();
    }, 1500);
  }

  onFocusedRowChanged(e: any): void {
    if (e.target.tagName == "TD") {
      let dataId = e.target.parentElement.getAttribute("data-id");
      if (dataId == this.focusedRow) this.focusedRow = -1;
      else this.focusedRow = e.target.parentElement.getAttribute("data-id");
    }
  }

  getSortIcon() {
    if (this.currentSortOrder == "asc") return "arrow_drop_down";

    return "arrow_drop_up";
  }

  onHeaderClick(moduleName: string) {
    if (this.currentSortColumn == moduleName && this.currentSortOrder == "asc")
      this.currentSortOrder = "desc";
    else this.currentSortOrder = "asc";

    this.currentSortColumn = moduleName;
    this.renderTable();
  }
}
