import { Component, OnInit, Input, ChangeDetectorRef, ViewChild } from "@angular/core";
import { DynamicService } from "src/app/modules/dynamic-table/services/dynamic.service";
import { Subscription } from "rxjs";
import {
  ModuleConfig,
  DynamicPageConfig,
  TableColumn
} from "../dynamic-page/dynamic-page.config";
import { PageEvent } from "@angular/material/paginator";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { FormControl } from '@angular/forms';

import f from 'odata-filter-builder';
import { ResizeEvent } from 'angular-resizable-element';

@Component({
  selector: "app-relation-tabs",
  templateUrl: "./dynamic-relation-tabs.component.html",
  styleUrls: ["./dynamic-relation-tabs.component.scss"]
})
export class DynamicRelationTabsComponent implements OnInit {
  @Input() mainModule: ModuleConfig;
  @Input() keyId: any;

  dataSource = new MatTableDataSource();
  tabPanelIndex: number;
  tabModules: ModuleConfig[];

  totalItems: number;
  isPending: boolean;
  currentPageIndex: number = 0;
  currentPageSize: number = 100;

  filters = {};
  filterSubs = {};
  filterValues = {};
  filtersCreated = true;
  timeout = null;

  public style: object = {};

  constructor(private service: DynamicService) {
    this.tabPanelIndex = -1;
  }

  get relationColumns(): TableColumn[] {
    return this.tabModules[this.tabPanelIndex].tableColumns;
  }

  get originalColumnTitles(): string[] {
    return this.relationColumns.map(e => e.originalTitle);
  }

  get filterTitles(): string[] {
    return this.relationColumns.map(e => 'f_' + e.originalTitle);
  }

  ngOnInit() {
    this.tabModules = [];

    let ind = 0;
    for (let relatedName of this.mainModule.relatedModules) {
      const relatedModule = DynamicPageConfig.FindModule(relatedName);
      this.tabModules.push(relatedModule);
      ind += 1;
    }
  }

  onResizeEnd(event: ResizeEvent): void {
    
    this.style = {
      height: `${event.rectangle.height-63}px` // :(
    };

    console.log('Element was resized', event);
  }

  tabChange(event: MatTabChangeEvent) {
    this.currentPageIndex = 0;
    this.tabPanelIndex = event.index - 1;
    if (this.tabPanelIndex != -1) {
      this.clearFilters();
      this.createFilters();
      this.renderTabContent();
    }
  }

  clearFilters() {
    this.filters = {};
    this.filterValues = {};

    for (let col in this.filterSubs) {
      this.filterSubs[col].unsubscribe();
    }
    this.filterSubs = {};
  }

  createFilters() {
    const currentModule = this.tabModules[this.tabPanelIndex];

    for (const column of currentModule.tableColumns) {
      this.filters[column.originalTitle] = new FormControl('');

      this.filterSubs[column.originalTitle] = (this.filters[column.originalTitle] as FormControl).valueChanges.subscribe(
        value => {
          this.filterValues[column.originalTitle] = value;
          if (value == '')
            delete this.filterValues[column.originalTitle];
        }
      )
    }
    this.filtersCreated = true;
  }

  generateFilterString() {
    let odataFilter = f();
    for (const col in this.filterValues) {
      const filterValue = this.filterValues[col].toLowerCase();

      odataFilter.contains(x => x.toLower(`cast(${col.replace(/\./g, '/')}, 'Edm.String')`), filterValue);
    }
    return odataFilter.toString();
  }

  checkData(): boolean {
    return this.dataSource.data.length == 0;
  }

  ngOnChanges() {
    if (this.tabPanelIndex != -1)
      this.renderTabContent();
  }

  pageChange(event: PageEvent) {
    this.currentPageIndex = event.pageIndex;
    this.currentPageSize = event.pageSize;
  }

  private renderTabContent() {

    const tabIndex = this.tabPanelIndex;
    if (tabIndex == -1) return;

    this.isPending = true;

    let filter = `${this.mainModule.relatedColumns[tabIndex]} eq ${this.keyId}`;
    const formFilters = this.generateFilterString();
    
    if (formFilters)
      filter += ' and ' + formFilters;
    
    const pagedQuery = this.tabModules[tabIndex].GetQueryString(filter)
      + `&$top=${this.currentPageSize}`
      + `&$skip=${this.currentPageIndex * this.currentPageSize}`;

    this.moduleSub = this.service.Entries(pagedQuery).subscribe(
      response => {
        this.dataSource.data = response;
      },
      err => (this.isPending = false),
      () => (this.isPending = false)
    );
  }

  onKeyUp() {
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this.renderTabContent();
    }, 1500);
  }

  closeTab() {
    this.tabPanelIndex = -1;
    this.style = {
      height: "auto"
    }
  }

  isAnyTabSelected(): boolean {
    return this.tabPanelIndex >= 0;
  }

  moduleSub: Subscription;
  ngOnDestroy() {
    if (this.moduleSub) this.moduleSub.unsubscribe();
    this.clearFilters();
  }
}
