import { Injectable, Output, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { ActivatedRoute } from "@angular/router";
import { ModuleConfig } from "../components/dynamic-page/dynamic-page.config";
import { environment } from "src/environments/environment";

@Injectable({ providedIn: "root" })
export class DynamicService {
  private url: string = environment.apiUrl;

  currentModule: ModuleConfig;
  @Output() getCurrentModule: EventEmitter<ModuleConfig> = new EventEmitter();
  @Output() dynamicCall: EventEmitter<string> = new EventEmitter();

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  public Entries(query: string): Observable<any[]> {
    console.log("DynamicServiceEntries");
    console.log(`${this.url}/${query}`);
    return this.http.get<any[]>(`${this.url}/${query}`).pipe(
      map(response => {
        return response;
      })
    );
  }

  public Count(query: string): Observable<number> {
    console.log("DynamicServiceCount");
    console.log(`${this.url}/${query}&$count=true`);
    return this.http.get<number>(`${this.url}/${query}&$count=true`);
  }

  public setCurrentModule(module: ModuleConfig): void {
    this.currentModule = module;
    this.getCurrentModule.emit(module);
  }
}
