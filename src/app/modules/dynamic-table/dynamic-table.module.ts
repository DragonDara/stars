import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DynamicRelationTabsComponent } from "./components/dynamic-relation-tabs/dynamic-relation-tabs.component";
import { HttpClientModule } from "@angular/common/http";
import { DynamicService } from "./services/dynamic.service";
import { RouterModule } from "@angular/router";
import { DynamicPageComponent } from "./components/dynamic-page/dynamic-page.component";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatTabsModule } from "@angular/material/tabs";
import { MatTableModule } from "@angular/material/table";
import { MatIconModule } from "@angular/material/icon";
import { GetValuePipe } from "../../pipes/get-value.pipe";
import { CleanDataPipe } from "../../pipes/clean-data.pipe";
import {
  MatInputModule,
  MatButtonModule,
  MatTooltipModule
} from "@angular/material";
import { ReactiveFormsModule } from "@angular/forms";
import { ResizableModule } from "angular-resizable-element";
import { MsalGuard } from "@azure/msal-angular";

@NgModule({
  declarations: [DynamicPageComponent, DynamicRelationTabsComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatTabsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatTooltipModule,
    MatIconModule,
    MatInputModule,
    ResizableModule,
    RouterModule.forChild([
      {
        path: "archive/:module",
        component: DynamicPageComponent,
        canActivate: [MsalGuard]
      }
    ])
  ],
  providers: [DynamicService, GetValuePipe, CleanDataPipe]
})
export class DynamicTableModule {}
