import { PipeTransform, Pipe } from "@angular/core";

@Pipe({ name: "getValue" })
export class GetValuePipe implements PipeTransform {
  transform(obj: any, key: string): any {
    key = key.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
    key = key.replace(/^\./, ""); // strip a leading dot
    var a = key.split(".");
    while (a.length) {
      if (Array.isArray(obj)) {
        return obj.map(o => this.transform(o, a.join(".")));
      }

      var n = a.shift();
      if (n in obj) {
        obj = obj[n];
      } else {
        return;
      }
    }
    return obj;
  }
}
