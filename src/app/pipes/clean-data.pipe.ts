import { PipeTransform, Pipe } from "@angular/core";
import { formatDate } from "@angular/common";

@Pipe({ name: "cleanData" })
export class CleanDataPipe implements PipeTransform {
  transform(obj: any): any {
    return tryFormatDate(obj);
  }
}

export function tryFormatDate(obj: any): any {
  if (typeof obj == "string" && !isNaN(Date.parse(obj)))
    return formatDate(obj, "dd.MM.yyyy", "en");

  return obj;
}

export function tryFormatBoolean(obj: any): string {
  switch (+obj) {
    case 0:
      return "Not";
    case 1:
      return "Yes";
    case -1:
      return "Not Applicable";
    default:
      return obj;
  }
}
